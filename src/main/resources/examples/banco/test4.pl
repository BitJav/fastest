#!/usr/bin/perl
use feature qw(say);

# Hash donde se guardarán las cuentas
%cajasImp = {};

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = ('finalizacionExitosa' => 'OK',
			 'numeroClienteEnUso' => 'Número de cliente en uso',
			 'noPoseeSaldoSuficiente' => 'No posee saldo suficiente',
			 'saldoNoNulo' => 'El saldo de la cuenta no es nulo');

#Chequea si existe o no una una cuenta
# Argumento: número de cuenta
sub isAlreadyAnAccount {
	if(exists($cajasImp->{$_[0]})) {
		return 1;
	} else {
		return 0;
	}
}

# Crea un cliente en el banco con la cuenta c
# Argumento: número de cuenta
sub createClient {
	$accountNumber = $_[0];
	if(isAlreadyAnAccount($accountNumber)) {
		say($mensajes{'numeroClienteEnUso'})
	} else {
		$cajasImp->{$accountNumber} = 0;
	}
}

# Realiza el deposito de m en la cuenta c
# Primer argumento: monto a depositar
# Segundo argumento: número de cuenta
sub depositar {
	$amount = $_[0];
	$accountNumber = $_[1];
	
	if(isAlreadyAnAccount($accountNumber)) {
		if ($amount > 0) {
			$cajasImp->{$accountNumber} += $amount;
			say($mensajes{'finalizacionExitosa'})
		} else {
			say("Monto incorrecto")
		} 
	} else {
		say("Cuenta inexistente")	
	}
}

# Realiza una extracción del monto m de la cuenta c
# Primer argumento: monto a depositar
# Segundo argumento: número de cuenta
sub extraer {
	$amount = $_[0];
	$accountNumber = $_[1];
	
	if(isAlreadyAnAccount($accountNumber)) {
		if ($amount > 0) {
			if($amount <= $cajasImp->{$accountNumber}) {
				$cajasImp->{$accountNumber} -= $amount;
				say($mensajes{'finalizacionExitosa'})
			} else {
				say($mensajes{'noPoseeSaldoSuficiente'})
			}
		} else {
			say("Monto incorrecto")
		} 
	} else {
		say("Cuenta inexistente")	
	}
}


if(isAlreadyAnAccount('3')) {
	say("si 3")
} else {
	say("no 3")
}

createClient('3');

if(isAlreadyAnAccount('3')) {
	say("si 3")
} else {
	say("no 3")
}


depositar('4', 100);
depositar('3', 200);

say("La cuenta nº 3 tiene \$". $cajasImp->{'3'});
	
	