# Defino el enumerado para tipos de domicilio
use constant {
    houseI    => 'houseI',
    buildingI => 'buildingI'
};

# Defino el enumerado para los géneros
use constant {
    maleI => 'maleI',
    femaleI => 'femaleI'
};

package Citizen;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
  my ($self, $name, $age, $gender, $progenitors) = @_;
  $self->{"name"} = $name;
  $self->{"age"} = $age;
  $self->{"gender"} = $gender;
  $self->{"progenitors"} = $progenitors;
}

sub get {
  my $self = shift;
  my $arg = shift;
  return $self->{$arg};
}

package Address;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
  my ($self, $streetName, $number, $type) = @_;
  $self->{"streetName"} = $streetName;
  $self->{"number"} = $number;
  $self->{"type"} = $type;
}

sub get {
  my $self = shift;
  my $arg = shift;
  return $self->{$arg};
}

package DniAndAddress;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
  my ($self, $dni, $address) = @_;
  $self->{"dni"} = $dni;
  $self->{"address"} = $address;
}

sub get {
  my $self = shift;
  my $arg = shift;
  return $self->{$arg};
}

package main;

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = (
  'personAlreadyRegistered' => 'La persona ya se encuentra registrada',
  'inexistingPerson' => 'La persona no se encuentra registrada',
  'personAlreadyHaveDwelling' => 'La persona ya tiene domicilio',
  'childAlreadyRegistered' => 'El hija/o ya se encuentra registrada/o',
  'notRegisteredChild' => 'Hijo no se encuentra registrado'
);

# Arrays donde se guardará la información del registro civil
@populationImp = ();
@dwellingsImp = ();
@childrenImp = ();

# Chequea si ya fue agregado una persona
# a un determinado contenedor
# Argumento 1: contenedor donde buscar
# Argumento 2: DNI de persona buscada
sub personExists {
  @container = @{$_[0]};
  $citizenDni = $_[1];

  $length = @container;
  for $i (0..$length-1) {
    if(ref($container[$i]) eq 'HASH') {
      if($container[$i]->{"dni"} eq $citizenDni) {
        return 1;
      }
    } else {
      if($container[$i]->get("dni") eq $citizenDni) {
        return 1;
      }
    }
  }
  return 0;
}

sub childInArray {
  @children = @{$_[0]};
  $child = $_[1];

  $childrenSize = @children;
  for $i (0..$childrenSize-1) {
    if($children[$i] eq $child) {
      return 1;
    }
  }
  return 0;
}

sub getPersonArrayIndex {
  @container = @{$_[0]};
  $citizenDni = $_[1];

  $length = @container;
  for $i (0..$length-1) {
    if(ref($container[$i]) eq 'HASH') {
      if($container[$i]->{"dni"} eq $citizenDni) {
        return $i;
      }
    } else {
      if($container[$i]->get("dni") eq $citizenDni) {
        return $i;
      }
    }
  }
  return -1;
}


sub citizenRegistration {
  $dni = $_[0];
  $citizen = $_[1];
  $address = $_[2];
  @children = @{$_[3]};
  
  if(personExists(\@populationImp, $dni)) {
    say($mensajes{'personAlreadyRegistered'});
  } else {
    $lastIndex = @populationImp;

    $citizenAux = Citizen->new();
    $name = $citizen->get("name");
    $age = $citizen->get("age");
    $gender = $citizen->get("gender");
    $progenitors = $citizen->get("progenitors");
    $citizenAux->set($name,$age,$gender,$progenitors);

    $populationImp[$lastIndex] = {"dni" => $dni, "citizen" => $citizenAux};

    
    $lastIndex = @dwellingsImp;

    $addressAux = Address->new();
    $streetName = $address->get("streetName");
    $number = $address->get("number");
    $type = $address->get("type");
    $addressAux->set($streetName,$number,$type);

    $dwellingsImp[$lastIndex] = DniAndAddress->new();
    $dwellingsImp[$lastIndex]->set($dni, $addressAux);

    $lastIndex = @childrenImp;

    $childrenImp[lastIndex] = {"dni" => $dni, "dnis" => \@children};
  }
}

sub changePersonDwelling {
  $dni = $_[0];
  $address = $_[1];
  
  if(personExists(\@dwellingsImp, $dni)) {
    $dweIndex = getPersonArrayIndex(\@dwellingsImp, $dni);
    $addressAux = $dwellingsImp[$dweIndex]->get("address");

    $streetName = $address->get("streetName");
    $number = $address->get("number");
    $type = $address->get("type");
    $addressAux->set($streetName,$number,$type);

    $dwellingsImp[$dweIndex]->set($dni, $addressAux);
  } else {
    say($mensajes{'inexistingPerson'});
  }
}

sub addChild {
  $dad = $_[0];
  $child = $_[1];

  if(personExists(\@populationImp, $dad)) {
    if(personExists(\@populationImp, $child)) {
      $chiIndex = getPersonArrayIndex(\@childrenImp, $dad);
      @children = @{$childrenImp[$chiIndex]->{"dnis"}};
      if(childInArray(\@children, $child)) {
        say($mensajes{'childAlreadyRegistered'});
      } else {
        push(@{$childrenImp[$chiIndex]->{"dnis"}}, $child);
      }
    } else {
      say($mensajes{'notRegisteredChild'});
    }
  } else {
    say($mensajes{'inexistingPerson'});
  }
}

sub citizenDeath {
  $citizenDni = $_[0];

  if(personExists(\@populationImp, $citizenDni)) {
    $popIndex = getPersonArrayIndex(\@populationImp, $citizenDni);
    $dweIndex = getPersonArrayIndex(\@dwellingsImp, $citizenDni);
    $chiIndex = getPersonArrayIndex(\@childrenImp, $citizenDni);
    delete $populationImp[popIndex];
    delete $dwellingsImp[dweIndex];
    delete $childrenImp[chiIndex];
    $progenitors = $populationImp[$popIndex]->{"progenitors"};
    $dni1 = $progenitors->{"dni1"};
    $dni2 = $progenitors->{"dni2"};

    if (personExists(\@childrenImp, $dni1)) {
      $dni1Index = getPersonArrayIndex(\@childrenImp, $dni1);
      @dni1Children = @{$childrenImp[$dni1Index]->{"dnis"}};
      if(childInArray(\@dni1Children, $citizenDni)) {
        $dniIndex = getPersonArrayIndex(\@dni1Children, $citizenDni);
        delete ${$childrenImp[$dni1Index]->{"dnis"}}[$dniIndex];
      }
    }
    if (personExists(\@childrenImp, $dni2)) {
      $dni2Index = getPersonArrayIndex(\@childrenImp, $dni2);
      @dni2Children = @{$childrenImp[$dni2Index]->{"dnis"}};
      if(childInArray(\@dni2Children, $citizenDni)) {
        $dniIndex = getPersonArrayIndex(\@dni2Children, $citizenDni);
        delete ${$childrenImp[$dni2Index]->{"dnis"}}[$dniIndex];
      }
    }
  } else {
    say($mensajes{'inexistingPerson'});
  }
}