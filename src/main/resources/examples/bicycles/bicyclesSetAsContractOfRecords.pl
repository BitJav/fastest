# Defino el enumerado para tamaños de bicicletas
use constant {
    s18I	=> 's18I',
    s20I	=> 's20I',
    s24I	=> 's24I',
    s26I	=> 's26I',
    s28I	=> 's28I',
    s29I	=> 's29I'
};

# Defino el enumerado para colores
use constant {
    yellowI	=> 'yellowI',
    redI	=> 'redI',
    blueI	=> 'blueI'
};

package Set;

sub new {
    my ($class,$args) = @_;
    @emptyArray = ();
    my $self = bless {"elem" => \@emptyArray}, $class;
}

sub push {
	my ($self, $pair) = @_;
	@elem = @{$self->{"elem"}};
	$elemArraySize = @elem;
	$self->{"elem"}[$elemArraySize] = $pair;
}

sub pop {
	my $self = shift;
	my $arg = shift;
	@elem = @{$self->{"elem"}};
	$elemArraySize = @elem;
	if($elemArraySize == 0) {
		die;
	}
	$index = $elemArraySize-1;
	$result = $self->{"elem"}[$index];
	delete $self->{"elem"}[$index];
	return $result;
}

package Bike;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $size, $color, $manufacturer) = @_;
	$self->{"size"} = $size;
	$self->{"color"} = $color;
	$self->{"manufacturer"} = $manufacturer;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package Manufacturer;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $name, $phone, $address) = @_;
	$self->{"name"} = $name;
	$self->{"phone"} = $phone;
	$self->{"address"} = $address;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package main;

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = (
	'bikeAlreadyAdded' => 'La bicicleta ya fue agregada previamente',
	'bikeNotFound' => 'La bicicleta no existe'
);

# Objeto donde se guardarán las bicicletas
$bikes = Set->new();

# Entero utilizado para saber cual es el índice donde
# debe guardarse el siguiente elemento de bikes
$lastIndex = 0;

# Chequea si ya fue agregado un ID de bicilceta
# Argumento: ID de bicilceta
sub bikeExists {
	$bikesSize = $bikes->{"elem"};
	for $i (0..$bikesSize-1) {
		if($bikes->{"elem"}[$i]->{"fst"} eq $_[0]) {
			return 1;
		}
	}
	return 0;
}

# Retorna  el índice del array donde se encuentra guardado
# el hash que tiene el número de bicicleta que se pasa como
# argumento como clave. Devuelve -1 si no existe
# Argumento: número de bicicleta
sub getAccountArrayIndex {
	$bikesSize = $bikes->{"elem"};
	for $i (0..$bikesSize-1) {
		if($bikes->{"elem"}[$i]->{"fst"} == $_[0]) {
			return $i;
		}
	}
	return -1;
}

# Agrega una bicicleta
# Primer argumento: ID de bicicleta
# Segundo argumento: Objeto que contiene la información de la bicilceta
sub addBike {
	$bikeId = $_[0];
	$bike = $_[1];
	if(bikeExists($bikeId)) {
		say($mensajes{'bikeAlreadyAdded'})
	} else {
		$lastIndex = $bikes->{"elem"};
		$bikes[$lastIndex]->{"fst"} = $bikeId;

		$bikeAux = Bike->new();
		$size = $bike->get("size");
		$color = $bike->get("color");
		$manufacturerAux = $bike->get("manufacturer");
		$name = $manufacturerAux->get("name");
		$phone = $manufacturerAux->get("phone");
		$address = $manufacturerAux->get("address");
		$manufacturer = Manufacturer->new();
		$manufacturer->set($name,$phone,$address);
		$bikeAux->set($size,$color,$manufacturer);

		$bikes->{"elem"}[$lastIndex]->{"snd"} = $bikeAux;
		$lastIndex++;
	}
}

sub removeBike {
	$bikeId = $_[0];

	if(bikeExists($bikeId)) {
		$arrayIndex = getAccountArrayIndex($bikeId);
		delete $bikes->{"elem"}[$arrayIndex];
	} else {
		say($mensajes{'bikeNotFound'})
	}
}

sub getBikeManufacturer {
	$bikeId = $_[0];

	if(bikeExists($bikeId)) {
		$arrayIndex = getAccountArrayIndex($bikeId);
		return $bikes->{"elem"}[$arrayIndex]->get("manufacturer");
	} else {
		say($mensajes{'bikeNotFound'})
	}
}
