#!/usr/bin/perl
use feature qw(say);
use YAML::XS;
use Data::Dumper;

sub __fastest_dump {
  open F, '>', 'state.yml';
  print F Dumper ( @_ );
  close F;
}

package main;

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = (
	'alreadyExistElements' => 'Ya existen elementos actualmente',
	'sonElementAlreadyRelatedAsDad' => 'El elemento hijo existe como padre de un elemento',
	'sonElementAlreadyRelatedAsChild' => 'El elemento hijo existe como hijo de un elemento',
	'dadElementDoesntExist' => 'El elemento padre no existe',
	'dadElementAlreadyHaveTwoRelatedElements' => 'El elemento padre ya tiene 2 elementos asociados',
	'elementNotIncludedInRelations' => 'El elemento no se encuentra entre las relaciones'
);

@elementsImp = ();
@relationsImp = ();

sub isAnElement {
	$element = $_[0];
	$elementsCount = getElementsCount();
	for $i (0..$elementsCount-1) {
		if($elementsImp[$i] eq $element) {
			return 1;
		}
	}
	return 0;
}

sub getElementIndexFromElements {
	$element = $_[0];
	$elementsCount = getElementsCount();
	for $i (0..$elementsCount-1) {
		if($elementsImp[$i] eq $element) {
			return $i;
		}
	}
	return -1;
}

sub getRightElementIndexFromRelation {
	$element = $_[0];
	$relationsSize = @relationsImp;
	for $i (0..$relationsSize-1) {
		if($relationsImp[$i]->{"right"} eq $element) {
			return $i;
		}
	}
	return -1;
}

sub getRightRelations {
	$element = $_[0];
	@results = ();
	$relationsSize = @relationsImp;

	for $i (0..$relationsSize-1) {
		if($relationsImp[$i]->{"left"} eq $element) {
			push(@results, $relationsImp[$i]->{"right"});
		}
	}
	return $results;
}

sub isALeftRelationElement {
	$element = $_[0];
	$relationsSize = @relationsImp;
	for $i (0..$relationsSize-1) {
		if($relationsImp[$i]->{"left"} eq $element) {
			return 1;
		}
	}
	return 0;
}

sub isARightRelationElement {
	$element = $_[0];
	$relationsSize = @relationsImp;
	for $i (0..$relationsSize-1) {
		if($relationsImp[$i]->{"right"} eq $element) {
			return 1;
		}
	}
	return 0;
}

sub removeRightElementFromRelations {
	$element = $_[0];
	$relationsSize = @relationsImp;
	@indexes = ();
	for $i (0..$relationsSize-1) {
		if($relationsImp[$i]->{"right"} eq $element) {
			push(@indexes, $i);
		}
	}
	$size = @indexes;
	for $j (0..$size-1) {
		delete $relationsImp[$j];
	}
}

sub getElementsCount {
	$size = @elementsImp;
	return $size;
}


sub addInitialElement {
	$element = $_[0];
	$elementsCount = getElementsCount();
	if($elementsCount > 0) {
		say($mensajes{'alreadyExistElements'})
	} else {
		push(@elementsImp, $element);
	}
}

sub addRelationToElement {
	$dadElement = $_[0];
	$sonElement = $_[1];
	if(isAnElement($dadElement)) {
		@rightRelations = @{getRightRelations($dadElement)};
		$rightRelationsSize = @rightRelations;
		if ($rightRelationsSize > 2) {
			say($mensajes{'dadElementAlreadyHaveTwoRelatedElements'});
		} else {
			if (isALeftRelationElement($sonElement)) {
				say($mensajes{'sonElementAlreadyRelatedAsDad'});	
			} else {
				if (isARightRelationElement($sonElement)) {
					say($mensajes{'sonElementAlreadyRelatedAsChild'});
				} else {
					if(isAnElement($sonElement) != 1) {
						push(@elementsImp, $sonElement);
					}
					$pair = {"left" => $dadElement, "right" => $sonElement};
					push(@relationsImp, $pair);
				}
			}
		}
	} else {
		say($mensajes{'dadElementDoesntExist'})
	}
}

sub removeElement {
	$element = $_[0];
	if (isALeftRelationElement($element) || isARightRelationElement($element)) {
		if(isALeftRelationElement($element)) {
			removeRightElementFromRelations($element);
		}
		if(isARightRelationElement($element)) {
			$index = getRightElementIndexFromRelation($element);
			delete $relationsImp[$index];
		}
		if(isAnElement($element)) {
			$index = getElementIndexFromElements($element);
			delete $elementsImp[$index];
		}
	} else {
		say($mensajes{'elementNotIncludedInRelations'});
	}
}