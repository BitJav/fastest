package compserver.abstraction.types.spectypes;

/**
 * Represents a node in the AST generated with the abstraction laws that contains
 * the information of type of a relation
 */
public class SpecNodeRel implements SpecNodeBinRelation{

	public SpecNodeRel(){
	}
	public SpecNodeRel(SpecNode domType, SpecNode ranType){
		this.domType = domType;
		this.ranType = ranType;
	}
	@Override
	public void setSpecID(String id){
		this.id = id;
	}
	@Override
	public String getSpecID(){
		return id;
	}
	@Override
	public SpecNode getDomType(){
		return domType;
	}
	@Override
	public void setDomType(SpecNode domType){
		this.domType = domType;
	}
	@Override
	public SpecNode getRanType(){
		return ranType;
	}
	@Override
	public void setRanType(SpecNode ranType){
		this.ranType = ranType;
	}
	private String id;
	private SpecNode domType;
	private SpecNode ranType;
}
