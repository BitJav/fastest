package compserver.abstraction.types.impltypes;

/**
 * Represents a node in the AST generated with the abstraction laws that contains
 * the information of screen without a particular configuration
 */
public class ImplNodeScreenPlane implements ImplNodeScreen{

	public ImplNodeScreenPlane(){
		this.screenType = "plane";
	}
	@Override
	public void setImplID(String id){
		this.id = id;
	}
	@Override
	public String getImplID(){
		return id;
	}
	@Override
	public void setScreenType(String screenType){
		this.screenType = screenType;
	}
	@Override
	public String getScreenType(){
		return screenType;
	}
	@Override
	public void setScreenPath(String path){
		this.path = path;
	}
	@Override
	public String getScreenPath(){
		return path;
	}
	@Override
	public void setEnumeration(ImplNodeEnumeration enumeration){
		this.enumeration = enumeration;
	}
	@Override
	public ImplNodeEnumeration getEnumeration(){
		return enumeration;
	}
	private String screenType;
	private String id;
	private String path;
	private ImplNodeEnumeration enumeration;
}