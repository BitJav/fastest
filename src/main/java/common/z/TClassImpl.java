package common.z;

import czt.SpecUtils;
import czt.visitors.CZTCloner;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.util.Visitor;
import net.sourceforge.czt.z.ast.*;

import java.util.List;

/**
 * An implementation of the interface TClass.
 * @author Pablo Rodriguez Monetti
 */
public class TClassImpl implements TClass {

    private AxPara myAxPara;
    private String schName;
    private List<PreExpr> inclPreds;

    public TClassImpl(AxPara axPara, String schName)
            throws IllegalArgumentException {

        if (!isTClass(axPara)) {
            throw new IllegalArgumentException();
        } else {
            setMyAxPara(axPara);
            this.schName = schName;
            SpecUtils.setAxParaName(axPara, schName);
        }
    }
    public TClassImpl(AxPara axPara, String schName, List<PreExpr> inclPreds)
            throws IllegalArgumentException {

        if (!isTClass(axPara)) {
            throw new IllegalArgumentException();
        } else {
            this.inclPreds = inclPreds;
            setMyAxPara(axPara);
            this.schName = schName;
            SpecUtils.setAxParaName(axPara, schName);
        }
    }

    static boolean isTClass(AxPara axPara) {
        if (!OpSchemeImpl.isOpScheme(axPara)) {
            return true;
        }
        return true;
        //return false;
    }

    @Override
	public void setMyAxPara(AxPara axPara) {
        myAxPara = axPara;
    }

    @Override
	public AxPara getMyAxPara() {
        return myAxPara;
    }

    @Override
	public void setSchName(String schName) {
        this.schName = schName;
    }

    @Override
	public String getSchName() {
        return schName;
    }

    @Override
	public ZNameList getName() {
        return myAxPara.getName();
    }

    @Override
	public Box getBox() {
        return myAxPara.getBox();
    }

    @Override
	public NameList getNameList() {
        return myAxPara.getNameList();
    }

    @Override
	public SchText getSchText() {
        return myAxPara.getSchText();
    }

    @Override
	public ZNameList getZNameList() {
        return myAxPara.getZNameList();
    }

    @Override
	public ZSchText getZSchText() {
        return myAxPara.getZSchText();
    }

    @Override
	public void setBox(Box box) {
        myAxPara.setBox(box);
    }

    @Override
	public void setNameList(NameList nameList) {
        myAxPara.setNameList(nameList);
    }

    @Override
	public void setSchText(SchText schText) {
        myAxPara.setSchText(schText);
    }

    @Override
	public <R> R accept(Visitor<R> v) {
        return myAxPara.accept(v);
    }

    @Override
	public Term create(Object[] args) {
        return myAxPara.create(args);
    }

    @Override
	public <T> T getAnn(Class<T> c) {
        return myAxPara.getAnn(c);
    }

    @Override
	public List getAnns() {
        return myAxPara.getAnns();
    }

    @Override
	public Object[] getChildren() {
        return myAxPara.getChildren();
    }

    @Override
    public AxPara clone() {
        return new TClassImpl((AxPara) myAxPara.accept(new CZTCloner()), schName);
    }


    @Override
	public List<PreExpr> getInclPreds() {
        // TODO Auto-generated method stub
        return inclPreds;
    }

    @Override
	public void setInclPreds(List<PreExpr> inclPreds) {
        this.inclPreds = inclPreds;
    }
}
