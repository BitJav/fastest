package client.blogic.testing.atcal;

import java.util.LinkedHashMap;
import java.util.Map;

import bsh.This;
import client.blogic.testing.atcal.exceptions.ContractShouldHaveOnlyOneMemberException;
import client.blogic.testing.atcal.z.ast.ZExpr;

/**
 * Contract types can be classified as:
 * <ul>
 * <li><strong>Unary:</strong> when the contract have only one setter argument</li>
 * <li><strong>Normal:</strong> when the contract have two or more setter arguments</li>
 * </ul>
 * Unary ones can be used to refine sets o lists
 * 
 * Created by cristian on 4/21/15.
 */
public class ContractType extends ATCALType {
	private final String                           module;
	private final String                           constructor;
	private final LinkedHashMap<String, ATCALType> constArgs;
	private final String                           setter;
	private final LinkedHashMap<String, ATCALType> setterArgs;
	private final String                           getter;
	private final LinkedHashMap<String, ATCALType> getterArgs;

	public ContractType(String constructor,
	                    LinkedHashMap<String, ATCALType> constArgs,
	                    String setter,
	                    LinkedHashMap<String, ATCALType> setterArgs,
	                    String getter,
	                    LinkedHashMap<String, ATCALType> getterArgs) {
		this.module      = null;
		this.constructor = constructor;
		this.constArgs   = constArgs;
		this.setter      = setter;
		this.setterArgs  = setterArgs;
		this.getter      = getter;
		this.getterArgs  = getterArgs;
	}

	public ContractType(String module,
	                    String constructor,
	                    LinkedHashMap<String, ATCALType> constArgs,
	                    String setter,
	                    LinkedHashMap<String, ATCALType> setterArgs,
	                    String getter,
	                    LinkedHashMap<String, ATCALType> getterArgs) {
		this.module      = module.replaceAll("\"", "");  // remove string characters
		this.constructor = constructor;
		this.constArgs   = constArgs;
		this.setter      = setter;
		this.setterArgs  = setterArgs;
		this.getter      = getter;
		this.getterArgs  = getterArgs;
	}

	public String getModule() {
		return module;
	}

	public String getConstructor() {
		return constructor;
	}

	public Map<String, ATCALType> getConstArgs() {
		return constArgs;
	}

	public String getSetter() {
		return setter;
	}

	public Map<String, ATCALType> getSetterArgs() {
		return setterArgs;
	}

	public String getGetter() {
		return getter;
	}

	public Map<String, ATCALType> getGetterArgs() {
		return getterArgs;
	}

	/**
	 * Tells if this contract type is a unary one
	 * 
	 * @return True if this contract type is a unary one
	 */
	public boolean hasUniqueMember() {
		return setterArgs.keySet().size() == 1;
	}

	/**
	 * This method should only be called when this contract type is refining a set or list.
	 * In such case, this type has only one member.
	 * 
	 * @return The unique member's type this contract holds
	 */
	public ATCALType getUniqueMemberType() {
		ATCALType memberType     = null;
		int       memberQuantity = setterArgs.keySet().size();
		if (memberQuantity != 1) {
			throw new ContractShouldHaveOnlyOneMemberException(constructor);
		}
		for (String memberName : setterArgs.keySet()) {
			memberType = setterArgs.get(memberName);
		}
		return memberType;
	}

	/**
	 * This method should only be called when this contract type is refining a set or list.
	 * In such case, this type has only one member.
	 * 
	 * @return The unique member's name this contract holds
	 */
	public String getUniqueMemberName() {
		String member         = "ERROR";
		int    memberQuantity = setterArgs.keySet().size();
		if (memberQuantity != 1) {
			throw new ContractShouldHaveOnlyOneMemberException(constructor);
		}
		for (String memberName : setterArgs.keySet()) {
			member = memberName;
		}
		return member;
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		return false;
	}

	@Override
	public String typeCategory() {
		return "Contract";
	}

	@Override
	public boolean contains(String member) {
		return setterArgs.containsKey(member);
	}

	@Override
	public String toString() {
		return "ContractType{\n" + "\t" +
		       module +
		       "\n" +
		       "\tconstructor=" +
		       constructor +
		       "( " +
		       constArgs +
		       " )\n" +
		       "\t, setter=" +
		       setter +
		       "( " +
		       setterArgs +
		       " )\n" +
		       "\t, getter=" +
		       getter +
		       "( " +
		       getterArgs +
		       " )" +
		       "\n}";
	}

	@Override
	public int hashCode() {
		final int prime  = 31;
		int       result = 1;
		result = prime * result + ((constArgs == null) ? 0 : constArgs.hashCode());
		result = prime * result + ((constructor == null) ? 0 : constructor.hashCode());
		result = prime * result + ((getter == null) ? 0 : getter.hashCode());
		result = prime * result + ((getterArgs == null) ? 0 : getterArgs.hashCode());
		result = prime * result + ((module == null) ? 0 : module.hashCode());
		result = prime * result + ((setter == null) ? 0 : setter.hashCode());
		result = prime * result + ((setterArgs == null) ? 0 : setterArgs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractType other = (ContractType) obj;
		if (constArgs == null) {
			if (other.constArgs != null)
				return false;
		} else if (!constArgs.equals(other.constArgs))
			return false;
		if (constructor == null) {
			if (other.constructor != null)
				return false;
		} else if (!constructor.equals(other.constructor))
			return false;
		if (getter == null) {
			if (other.getter != null)
				return false;
		} else if (!getter.equals(other.getter))
			return false;
		if (getterArgs == null) {
			if (other.getterArgs != null)
				return false;
		} else if (!getterArgs.equals(other.getterArgs))
			return false;
		if (module == null) {
			if (other.module != null)
				return false;
		} else if (!module.equals(other.module))
			return false;
		if (setter == null) {
			if (other.setter != null)
				return false;
		} else if (!setter.equals(other.setter))
			return false;
		if (setterArgs == null) {
			if (other.setterArgs != null)
				return false;
		} else if (!setterArgs.equals(other.setterArgs))
			return false;
		return true;
	}
}
