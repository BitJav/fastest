package client.blogic.testing.atcal.visitors;

import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.apl.expressions.APLExprList;
import client.blogic.testing.atcal.apl.expressions.CallExpr;
import client.blogic.testing.atcal.apl.expressions.ConsExpr;
import client.blogic.testing.atcal.apl.expressions.LongExpr;
import client.blogic.testing.atcal.apl.expressions.RecordExpr;
import client.blogic.testing.atcal.apl.expressions.ReferenceExpr;
import client.blogic.testing.atcal.apl.expressions.StringExpr;
import client.blogic.testing.atcal.apl.expressions.APLArray.APLArrayIndex;

public interface APLExprVisitor<T> {

	public T visit(APLArray aplArray);

	public T visit(APLArrayIndex aplArrayIndex);

	public T visit(APLExprList aplExprList);

	public T visit(APLVar aplVar);

	public T visit(CallExpr callExpr); // <------------- VER

	public T visit(ConsExpr constantExpr);

	public T visit(LongExpr longExpr);

	public T visit(RecordExpr recordExpr);

	public T visit(StringExpr stringExpr);

	public T visit(ReferenceExpr stringExpr);
}
