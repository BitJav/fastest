/**
 * 
 */
package client.blogic.testing.atcal.visitors;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.ArrayType;
import client.blogic.testing.atcal.IntType;
import client.blogic.testing.atcal.RecordType;
import client.blogic.testing.atcal.StringType;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.apl.expressions.APLExprList;
import client.blogic.testing.atcal.apl.expressions.CallExpr;
import client.blogic.testing.atcal.apl.expressions.ConsExpr;
import client.blogic.testing.atcal.apl.expressions.LongExpr;
import client.blogic.testing.atcal.apl.expressions.RecordExpr;
import client.blogic.testing.atcal.apl.expressions.ReferenceExpr;
import client.blogic.testing.atcal.apl.expressions.StringExpr;
import client.blogic.testing.atcal.apl.expressions.APLArray.APLArrayIndex;
import client.blogic.testing.atcal.apl.APLVar;

/**
 * @author Javier Bonet on 18/10/2019
 * 
 */
public class TypeSolverVisitor implements APLExprVisitor<ATCALType> {
	
	@Override
	public ATCALType visit(APLArray aplArray) {
		// Returns the an array type with size 0 because I suppose it wouldn't be used
		return new ArrayType(aplArray.getType(), 0);
	}

	@Override
	public ATCALType visit(APLArrayIndex aplArrayIndex) {
		return aplArrayIndex.getParent().accept(this);
	}

	@Override
	public ATCALType visit(APLExprList aplExprList) {
		List<APLExpr> exprList = aplExprList.getAplExprList();
		return new ArrayType(aplExprList.getType(), exprList.size());
	}

	@Override
	public ATCALType visit(APLVar aplVar) {
		return aplVar.getType();
	}

	@Override
	public ATCALType visit(CallExpr callExpr) {
		throw new RuntimeException("Type solver called over a function call, this shouldn't happened");
	}

	@Override
	public ATCALType visit(ConsExpr constantExpr) {
		// I consider constant expressions as strings
		return new StringType();
	}

	@Override
	public ATCALType visit(LongExpr longExpr) {
		// I consider that long values to be IntType
		return IntType.getInstance();
	}

	@Override
	public ATCALType visit(RecordExpr recordExpr) {
		LinkedHashMap<String, APLExpr> membersMap = recordExpr.getRecordMap();
		LinkedHashMap<String, ATCALType> typesMap = new LinkedHashMap<>();
		for (Entry<String, APLExpr> entry : membersMap.entrySet()) {
			String memberName = entry.getKey();
			ATCALType memberType = entry.getValue().accept(this);
			typesMap.put(memberName, memberType);
		}
		return new RecordType(typesMap);
	}

	@Override
	public ATCALType visit(StringExpr stringExpr) {
		return new StringType();
	}
	
	@Override
	public ATCALType visit(ReferenceExpr referenceExpr) {
		// I consider that reference values to be IntType
		return IntType.getInstance();
	}

}
