package client.blogic.testing.atcal.exceptions;

public class InnerArrayTypeMustBeStringException extends CheckedException {
	private static final long serialVersionUID = 2535292959610923350L;
	
	private static final String TO_REFINE_STRING_AS       = "To refine a string as ";
	private static final String INNER_TYPE_MUST_BE_STRING = ", the inner type must be string";

	public InnerArrayTypeMustBeStringException(String arrayVarName) {
		super(TO_REFINE_STRING_AS + arrayVarName + INNER_TYPE_MUST_BE_STRING);
	}
	
	public InnerArrayTypeMustBeStringException(String arrayVarName, Throwable err) {
		super(TO_REFINE_STRING_AS + arrayVarName + INNER_TYPE_MUST_BE_STRING, err);
	}
}
