package client.blogic.testing.atcal.exceptions;

public class SpecificationVariableCantBeRefinedToTypeException extends CheckedException {
	private static final long serialVersionUID = 5062549811092599898L;

	private static final String CANT_BE_REFINED_TO = " can't be refined to ";

	public SpecificationVariableCantBeRefinedToTypeException(String specificationVar, String destinationRefinementType) {
		super(specificationVar + CANT_BE_REFINED_TO + destinationRefinementType);
	}

	public SpecificationVariableCantBeRefinedToTypeException(String specificationVar,
	                                                         String destinationRefinementType,
	                                                         Throwable err) {
		super(specificationVar + CANT_BE_REFINED_TO + destinationRefinementType, err);
	}
}
