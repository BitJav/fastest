package client.blogic.testing.atcal.exceptions;

public class WithClauseNotAllowedException extends CheckedException {
	private static final long serialVersionUID = 772047520374695741L;

	private static final String WITH_CLAUSE_NOT_ALLOWED = "With clauses shouldn't be used when refining to ";

	public WithClauseNotAllowedException(String destinationRefinementType) {
		super(WITH_CLAUSE_NOT_ALLOWED + destinationRefinementType);
	}

	public WithClauseNotAllowedException(String destinationRefinementType, Throwable err) {
		super(WITH_CLAUSE_NOT_ALLOWED + destinationRefinementType, err);
	}
}