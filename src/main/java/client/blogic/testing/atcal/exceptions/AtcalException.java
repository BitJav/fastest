package client.blogic.testing.atcal.exceptions;

public class AtcalException extends RuntimeException {
	private static final long serialVersionUID = -6543744474100864059L;

	public AtcalException(String errorMessage) {
		super(errorMessage);
	}
	
	public AtcalException(String errorMessage, Throwable err) {
		super(errorMessage + "\n\t" + err.getMessage(), err);
	}
}
