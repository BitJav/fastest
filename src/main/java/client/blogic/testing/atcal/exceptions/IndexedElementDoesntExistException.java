package client.blogic.testing.atcal.exceptions;

public class IndexedElementDoesntExistException extends CheckedException {
	private static final long serialVersionUID = 5015387630807208495L;

	private static final String THE_ELEMENT_AT = "The element at ";
	private static final String IN             = " in ";
	private static final String DOESNT_EXIST   = " doesn't exist";

	public IndexedElementDoesntExistException(String expr, int index) {
		super(THE_ELEMENT_AT + index + IN + expr + DOESNT_EXIST);
	}

	public IndexedElementDoesntExistException(String expr, int index, Throwable err) {
		super(THE_ELEMENT_AT + index + IN + expr + DOESNT_EXIST, err);
	}
}
