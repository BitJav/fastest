package client.blogic.testing.atcal.exceptions;

public class WithClauseNeededException extends CheckedException {
	private static final long serialVersionUID = -268600426940402682L;

	private static final String WITH_CLAUSE_NEEDED = "With clause should be defined when refining to ";

	public WithClauseNeededException(String destinationRefinementType) {
		super(WITH_CLAUSE_NEEDED + destinationRefinementType);
	}

	public WithClauseNeededException(String destinationRefinementType, Throwable err) {
		super(WITH_CLAUSE_NEEDED + destinationRefinementType, err);
	}
}