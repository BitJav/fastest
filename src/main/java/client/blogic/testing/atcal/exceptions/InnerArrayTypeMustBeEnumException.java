package client.blogic.testing.atcal.exceptions;

public class InnerArrayTypeMustBeEnumException extends CheckedException {
	private static final long serialVersionUID = 891756829784348757L;
	
	private static final String INNER_ARRAY_TYPE_MUST_BE_ENUM = "Inner array type must be enum";

	public InnerArrayTypeMustBeEnumException() {
		super(INNER_ARRAY_TYPE_MUST_BE_ENUM);
	}

	public InnerArrayTypeMustBeEnumException(Throwable err) {
		super(INNER_ARRAY_TYPE_MUST_BE_ENUM, err);
	}
}
