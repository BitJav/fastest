package client.blogic.testing.atcal.exceptions;

public class RefinementToNotImplementedTypeException extends CheckedException {
	private static final long serialVersionUID = -3320504481166308793L;

	private static final String TRYING_TO_REFINE_TO   = "Trying to refine to ";
	private static final String THAT_ISNT_IMPLEMENTED = " that it isn't implemented yet";

	public RefinementToNotImplementedTypeException(String type) {
		super(TRYING_TO_REFINE_TO + type + THAT_ISNT_IMPLEMENTED);
	}

	public RefinementToNotImplementedTypeException(String type, Throwable err) {
		super(TRYING_TO_REFINE_TO + type + THAT_ISNT_IMPLEMENTED, err);
	}
}
