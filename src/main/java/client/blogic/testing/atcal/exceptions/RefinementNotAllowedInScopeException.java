package client.blogic.testing.atcal.exceptions;

public class RefinementNotAllowedInScopeException extends CheckedException {

	private static final long serialVersionUID = -6039453689508790086L;

	private static final String CANT_BE_REFINED_TO = " can't be refined to ";
	private static final String INSIDE             = " inside ";
	private static final String APL_SCOPE          = " APL scope";

	public RefinementNotAllowedInScopeException(String specificationVar, String destinationRefinementType, String scopeType) {
		super(specificationVar + CANT_BE_REFINED_TO + destinationRefinementType + INSIDE + scopeType + APL_SCOPE);
	}

	public RefinementNotAllowedInScopeException(String specificationVar,
	                                            String destinationRefinementType,
	                                            String scopeType,
	                                            Throwable err) {
		super(specificationVar + CANT_BE_REFINED_TO + destinationRefinementType + INSIDE + scopeType + APL_SCOPE, err);
	}
}
