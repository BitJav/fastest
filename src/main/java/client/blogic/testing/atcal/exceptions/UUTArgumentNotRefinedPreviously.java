package client.blogic.testing.atcal.exceptions;

public class UUTArgumentNotRefinedPreviously extends CheckedException {
	private static final long serialVersionUID = 2848142582823347248L;

	private static final String THE_IMPLEMENTATION_VARIABLE = "The implementation variable ";
	private static final String ISNT_INCLUDED_OR_HAS_ERRORS = " isn't included in the refinement laws or its corresponding law or has errors";

	public UUTArgumentNotRefinedPreviously(String implementationVariable) {
		super(THE_IMPLEMENTATION_VARIABLE + implementationVariable + ISNT_INCLUDED_OR_HAS_ERRORS);
	}

	public UUTArgumentNotRefinedPreviously(String implementationVariable, Throwable err) {
		super(THE_IMPLEMENTATION_VARIABLE + implementationVariable + ISNT_INCLUDED_OR_HAS_ERRORS, err);
	}
}
