package client.blogic.testing.atcal.exceptions;

public class InexistingRefinemetRuleException extends CheckedException {
	private static final long serialVersionUID = 5129872828724215690L;

	private static final String REFINEMENT_RULE_DOESNT_EXIST = " refinement rule doesn't exist";

	public InexistingRefinemetRuleException(String ruleName) {
		super(ruleName + REFINEMENT_RULE_DOESNT_EXIST);
	}

	public InexistingRefinemetRuleException(String ruleName, Throwable err) {
		super(ruleName + REFINEMENT_RULE_DOESNT_EXIST, err);
	}
}
