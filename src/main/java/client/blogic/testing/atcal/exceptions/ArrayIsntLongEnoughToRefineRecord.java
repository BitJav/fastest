package client.blogic.testing.atcal.exceptions;

public class ArrayIsntLongEnoughToRefineRecord extends CheckedException {
	private static final long serialVersionUID = -8431801029533489187L;

	private static final String ARRAY_ISNT_LONG_ENOUGH_TO_REFINE_RECORD = "Array isn't long enough to refine record";

	public ArrayIsntLongEnoughToRefineRecord() {
		super(ARRAY_ISNT_LONG_ENOUGH_TO_REFINE_RECORD);
	}

	public ArrayIsntLongEnoughToRefineRecord(Throwable err) {
		super(ARRAY_ISNT_LONG_ENOUGH_TO_REFINE_RECORD, err);
	}
}