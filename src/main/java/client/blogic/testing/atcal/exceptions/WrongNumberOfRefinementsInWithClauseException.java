package client.blogic.testing.atcal.exceptions;

public class WrongNumberOfRefinementsInWithClauseException extends CheckedException {
	private static final long serialVersionUID = 5653940101618492438L;

	private static final String THE_NUMBER_OF_REFINEMENTS_IN_WITH_CLAUSE_DOESNT_MATCH_THE_ARITY_OF = "The number of refinements in WITH clause does not match the arity of ";
	private static final String CONTRACT_TYPES_SETTER                                              = " contract type's setter.";

	public WrongNumberOfRefinementsInWithClauseException(String contractName) {
		super(THE_NUMBER_OF_REFINEMENTS_IN_WITH_CLAUSE_DOESNT_MATCH_THE_ARITY_OF + contractName + CONTRACT_TYPES_SETTER);
	}

	public WrongNumberOfRefinementsInWithClauseException(Throwable err, String contractName) {
		super(THE_NUMBER_OF_REFINEMENTS_IN_WITH_CLAUSE_DOESNT_MATCH_THE_ARITY_OF + contractName + CONTRACT_TYPES_SETTER, err);
	}
}
