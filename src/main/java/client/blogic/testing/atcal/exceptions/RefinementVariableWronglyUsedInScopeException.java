package client.blogic.testing.atcal.exceptions;

public class RefinementVariableWronglyUsedInScopeException extends CheckedException {
	private static final long serialVersionUID = 6448707246617937348L;

	private static final String CANT_BE_SPECICIED_IN = " can't be specified in ";
	private static final String SCOPE                = " scope";

	public RefinementVariableWronglyUsedInScopeException(String refinementVar, String scopeType) {
		super(refinementVar + CANT_BE_SPECICIED_IN + scopeType + SCOPE);
	}

	public RefinementVariableWronglyUsedInScopeException(String specificationVar, String scopeType, Throwable err) {
		super(specificationVar + CANT_BE_SPECICIED_IN + scopeType + SCOPE, err);
	}
}
