package client.blogic.testing.atcal.exceptions;

public class TypeErrorException extends CheckedException {
	private static final long serialVersionUID = 6735578000776305801L;

	private static final String THERE_WAS_A_TYPE_ERROR_WITH_TYPE = "There was a type error with type ";
	public TypeErrorException(String typeCtx) {
		super(THERE_WAS_A_TYPE_ERROR_WITH_TYPE + typeCtx);
	}
	
	public TypeErrorException(String typeCtx, Throwable err) {
		super(THERE_WAS_A_TYPE_ERROR_WITH_TYPE + typeCtx, err);
	}
}
