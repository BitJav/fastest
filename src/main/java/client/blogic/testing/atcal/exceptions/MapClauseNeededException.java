package client.blogic.testing.atcal.exceptions;

public class MapClauseNeededException extends CheckedException {
	private static final long serialVersionUID = 3094257537445943157L;

	private static final String MAP_CLAUSE_NEEDED = "Map clause should be defined when refining to ";

	public MapClauseNeededException(String destinationRefinementType) {
		super(MAP_CLAUSE_NEEDED + destinationRefinementType);
	}

	public MapClauseNeededException(String destinationRefinementType, Throwable err) {
		super(MAP_CLAUSE_NEEDED + destinationRefinementType, err);
	}
}
