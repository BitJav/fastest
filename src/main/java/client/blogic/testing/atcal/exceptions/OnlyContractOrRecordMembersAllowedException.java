package client.blogic.testing.atcal.exceptions;

public class OnlyContractOrRecordMembersAllowedException extends CheckedException {
	private static final long serialVersionUID = -7650828721481226338L;

	private static final String ONLY_CONTRACT_OR_RECORD_MEMBERS_ALLOWED = "Inside an array APL scope and refining sets or lists, inner APL expressions should only be contract's members or record's members";

	public OnlyContractOrRecordMembersAllowedException() {
		super(ONLY_CONTRACT_OR_RECORD_MEMBERS_ALLOWED);
	}

	public OnlyContractOrRecordMembersAllowedException(Throwable err) {
		super(ONLY_CONTRACT_OR_RECORD_MEMBERS_ALLOWED, err);
	}
}