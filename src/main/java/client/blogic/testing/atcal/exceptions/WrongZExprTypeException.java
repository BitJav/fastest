package client.blogic.testing.atcal.exceptions;

public class WrongZExprTypeException extends CheckedException {
	private static final long serialVersionUID = -7534229986078797081L;

	private static final String SHOULD_BE = " should be ";

	public WrongZExprTypeException(String zExprText,
	                               String type) {
		super(zExprText + SHOULD_BE + type);
	}

	public WrongZExprTypeException(String zExprText, String type, Throwable err) {
		super(zExprText + SHOULD_BE + type, err);
	}
}
