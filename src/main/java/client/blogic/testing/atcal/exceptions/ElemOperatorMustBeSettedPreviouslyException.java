package client.blogic.testing.atcal.exceptions;

public class ElemOperatorMustBeSettedPreviouslyException extends CheckedException {
	private static final long serialVersionUID = -4175035176550056039L;

	private static final String ELEM_OPERATOR_MUST_BE_SETTED_PREVIOUSLY = "@ELEM operator must be set previously";

	public ElemOperatorMustBeSettedPreviouslyException() {
		super(ELEM_OPERATOR_MUST_BE_SETTED_PREVIOUSLY);
	}

	public ElemOperatorMustBeSettedPreviouslyException(Throwable err) {
		super(ELEM_OPERATOR_MUST_BE_SETTED_PREVIOUSLY, err);
	}
}
