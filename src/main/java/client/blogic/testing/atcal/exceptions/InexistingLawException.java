package client.blogic.testing.atcal.exceptions;

public class InexistingLawException extends CheckedException {
	private static final long serialVersionUID = 2810438940144279369L;

	private static final String LAW             = " law";
	private static final String DOESNT_EXIST_IN = " doesn't exist in ";
	private static final String REFINEMENT_RULE = " refinement rule";

	public InexistingLawException(String ruleName, String lawName) {
		super(lawName + LAW + DOESNT_EXIST_IN + ruleName + REFINEMENT_RULE);
	}

	public InexistingLawException(String ruleName, String lawName, Throwable err) {
		super(lawName + LAW + DOESNT_EXIST_IN + ruleName + REFINEMENT_RULE, err);
	}
}
