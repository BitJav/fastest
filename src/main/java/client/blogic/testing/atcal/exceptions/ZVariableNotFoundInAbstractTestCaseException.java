package client.blogic.testing.atcal.exceptions;

public class ZVariableNotFoundInAbstractTestCaseException extends CheckedException {
	private static final long serialVersionUID = -4362492219212453955L;
	
	private static final String UNKNOWN_IDENTIFIER = "Unknown identifier ";
	
	public ZVariableNotFoundInAbstractTestCaseException(String identifierName) {
		super(UNKNOWN_IDENTIFIER + identifierName);
	}

	public ZVariableNotFoundInAbstractTestCaseException(String identifierName, Throwable err) {
		super(UNKNOWN_IDENTIFIER + identifierName, err);
	}
}
