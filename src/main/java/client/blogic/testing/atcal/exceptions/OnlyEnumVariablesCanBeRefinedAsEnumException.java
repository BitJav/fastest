package client.blogic.testing.atcal.exceptions;

public class OnlyEnumVariablesCanBeRefinedAsEnumException extends CheckedException {
	private static final long serialVersionUID = -6852852576065222746L;

	private static final String ONLY_ENUM_CAN_BE_REFINED_AS_ENUM = "Only enum variables can be refined as enum";

	public OnlyEnumVariablesCanBeRefinedAsEnumException() {
		super(ONLY_ENUM_CAN_BE_REFINED_AS_ENUM);
	}

	public OnlyEnumVariablesCanBeRefinedAsEnumException(Throwable err) {
		super(ONLY_ENUM_CAN_BE_REFINED_AS_ENUM, err);
	}
}
