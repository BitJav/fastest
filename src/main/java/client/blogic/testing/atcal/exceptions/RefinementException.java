package client.blogic.testing.atcal.exceptions;

public class RefinementException extends CheckedException {
	private static final long serialVersionUID = 5910524216330424895L;

	public RefinementException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
}
