package client.blogic.testing.atcal.exceptions;

/**
 * This class is the super class of all the exceptions with errors that have to be
 * informed to the user and continue running the main program
 * 
 * @author Javier Bonet on 17/11/2019
 * 
 */
public class CheckedException extends AtcalException {
	private static final long serialVersionUID = -398257044863070757L;

	public CheckedException(String errorMessage) {
		super(errorMessage);
	}

	public CheckedException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

}
