package client.blogic.testing.atcal.exceptions;

public class InnerContractTypeMusBeEnumException extends CheckedException {
	private static final long serialVersionUID = 6365100467330613511L;

	private static final String INNER_CONTRACT_TYPE_MUST_BE_ENUM = "Inner array type must be enum";

	public InnerContractTypeMusBeEnumException() {
		super(INNER_CONTRACT_TYPE_MUST_BE_ENUM);
	}

	public InnerContractTypeMusBeEnumException(Throwable err) {
		super(INNER_CONTRACT_TYPE_MUST_BE_ENUM, err);
	}
}
