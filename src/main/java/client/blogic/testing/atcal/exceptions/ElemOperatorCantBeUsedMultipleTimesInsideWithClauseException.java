package client.blogic.testing.atcal.exceptions;

public class ElemOperatorCantBeUsedMultipleTimesInsideWithClauseException extends CheckedException {
	private static final long serialVersionUID = 7681710901472087511L;
	
	private static final String ELEM_OPERATOR_CANT_BE_USED_MULTIPLE_TIMES_INSIDE_WITH_CLAUSE = "@ELEM operator can't be used multiple times inside with clause";

	public ElemOperatorCantBeUsedMultipleTimesInsideWithClauseException() {
		super(ELEM_OPERATOR_CANT_BE_USED_MULTIPLE_TIMES_INSIDE_WITH_CLAUSE);
	}
	
	public ElemOperatorCantBeUsedMultipleTimesInsideWithClauseException(Throwable err) {
		super(ELEM_OPERATOR_CANT_BE_USED_MULTIPLE_TIMES_INSIDE_WITH_CLAUSE, err);
	}
	
}
