package client.blogic.testing.atcal.exceptions;

public class WrongUseOfElemOperatorException extends AtcalException {
	private static final long serialVersionUID = -6235615744123526342L;
	
	private static final String INVALID_USE_OF_ELEMENT_OPERATOR = "Invalid use of @ELEM operator, ";
	private static final String MUST_BE_A_SET = " must be a set.";

	public WrongUseOfElemOperatorException(String scopeValue) {
		super(INVALID_USE_OF_ELEMENT_OPERATOR + scopeValue + MUST_BE_A_SET);
	}

	public WrongUseOfElemOperatorException(String scopeValue, Throwable err) {
		super(INVALID_USE_OF_ELEMENT_OPERATOR + scopeValue + MUST_BE_A_SET, err);
	}
}
