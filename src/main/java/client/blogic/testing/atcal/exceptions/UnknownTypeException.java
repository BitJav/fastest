package client.blogic.testing.atcal.exceptions;

public class UnknownTypeException extends CheckedException {
	private static final long serialVersionUID = -2916608572993433577L;
	
	private static final String IS_AN_UNKNOWN_TYPE = " is an unknown type";

	public UnknownTypeException(String type) {
		super(type + IS_AN_UNKNOWN_TYPE);
	}

	public UnknownTypeException(String type, Throwable err) {
		super(type + IS_AN_UNKNOWN_TYPE, err);
	}
}
