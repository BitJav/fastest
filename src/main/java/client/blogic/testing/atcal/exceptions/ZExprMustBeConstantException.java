package client.blogic.testing.atcal.exceptions;

public class ZExprMustBeConstantException extends CheckedException {
	private static final long serialVersionUID = -213502931105732254L;

	private static final String THE_Z_EXPR       = "The Z Expr ";
	private static final String MUST_BE_CONSTANT = " must be constant";

	public ZExprMustBeConstantException(String zExpr) {
		super(THE_Z_EXPR + zExpr + MUST_BE_CONSTANT);
	}

	public ZExprMustBeConstantException(String zExpr, Throwable err) {
		super(THE_Z_EXPR + zExpr + MUST_BE_CONSTANT, err);
	}
}
