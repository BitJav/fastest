package client.blogic.testing.atcal.exceptions;

public class TryingToRefineBasicStructureWithElemOperatorException extends CheckedException {
	private static final long serialVersionUID = 959303280245645846L;

	private static final String TRYING_TO_REFINE_A_BASIC_STRUCTURE_AS = "Trying to refine a basic structure as ";
	private static final String WITH_ELEM_OPERATOR                    = " with @ELEM operator";

	public TryingToRefineBasicStructureWithElemOperatorException(String scopeValue) {
		super(TRYING_TO_REFINE_A_BASIC_STRUCTURE_AS + scopeValue + WITH_ELEM_OPERATOR);
	}

	public TryingToRefineBasicStructureWithElemOperatorException(String scopeValue, Throwable err) {
		super(TRYING_TO_REFINE_A_BASIC_STRUCTURE_AS + scopeValue + WITH_ELEM_OPERATOR, err);
	}
}
