package client.blogic.testing.atcal.exceptions;

public class ProductCannotBeEmptyException extends CheckedException {
	private static final long serialVersionUID = 3338547805192007923L;

	private static final String CANNOT_BE_EMPTY = " cannot be empty";

	public ProductCannotBeEmptyException(String product) {
		super(product + CANNOT_BE_EMPTY);
	}

	public ProductCannotBeEmptyException(String product, Throwable err) {
		super(product + CANNOT_BE_EMPTY, err);
	}
}