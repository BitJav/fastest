package client.blogic.testing.atcal.exceptions;

public class ExpressionMustBeSetOrListInInnerRefinementToEnum extends CheckedException {
	private static final long serialVersionUID = -9083257469287795944L;

	private static final String SHOULD_BE_SET_OR_LIST = " should be a set or list expressions when refining to enum inside a with clause";

	public ExpressionMustBeSetOrListInInnerRefinementToEnum(String zExpr) {
		super(zExpr + SHOULD_BE_SET_OR_LIST);
	}

	public ExpressionMustBeSetOrListInInnerRefinementToEnum(String zExpr, Throwable err) {
		super(zExpr + SHOULD_BE_SET_OR_LIST, err);
	}
}
