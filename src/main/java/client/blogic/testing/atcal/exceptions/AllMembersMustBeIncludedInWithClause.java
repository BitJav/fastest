package client.blogic.testing.atcal.exceptions;

public class AllMembersMustBeIncludedInWithClause extends CheckedException {
	private static final long serialVersionUID = 5266524607058752226L;

	private static final String ALL                                     = "All ";
	private static final String MEMBERS_MUST_BE_INCLUDED_IN_WITH_CLAUSE = " members must be included in with clause";

	public AllMembersMustBeIncludedInWithClause(String typeName) {
		super(ALL + addPossesiveApostrophe(typeName) + MEMBERS_MUST_BE_INCLUDED_IN_WITH_CLAUSE);
	}

	public AllMembersMustBeIncludedInWithClause(String typeName, Throwable err) {
		super(ALL + addPossesiveApostrophe(typeName) + MEMBERS_MUST_BE_INCLUDED_IN_WITH_CLAUSE, err);
	}

	private static String addPossesiveApostrophe(String noun) {
		String lastCharacter = noun.substring(noun.length() - 1);
		String result;
		if (lastCharacter.equals("s")) {
			result = noun + "'";
		} else {
			result = noun + "'s";
		}
		return result;
	}
}
