package client.blogic.testing.atcal.exceptions;

public class ZExprMustBeSetOrListException extends CheckedException {
	private static final long serialVersionUID = -8577456807998367294L;
	
	private static final String MUST_BE_SET_OR_LIST = " must be set or list";

	public ZExprMustBeSetOrListException(String expr) {
		super(expr + MUST_BE_SET_OR_LIST);
	}

	public ZExprMustBeSetOrListException(String expr, Throwable err) {
		super(expr + MUST_BE_SET_OR_LIST, err);
	}
}
