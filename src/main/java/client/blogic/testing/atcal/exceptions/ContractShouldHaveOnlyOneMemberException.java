package client.blogic.testing.atcal.exceptions;

public class ContractShouldHaveOnlyOneMemberException extends CheckedException {
	private static final long serialVersionUID = 7412666009989608984L;

	private static final String SHOULD_HAVE_ONLY_ONE_MEMBER = " should have only one member";

	public ContractShouldHaveOnlyOneMemberException(String contract) {
		super(contract + SHOULD_HAVE_ONLY_ONE_MEMBER);
	}

	public ContractShouldHaveOnlyOneMemberException(String contract, Throwable err) {
		super(contract + SHOULD_HAVE_ONLY_ONE_MEMBER, err);
	}
}
