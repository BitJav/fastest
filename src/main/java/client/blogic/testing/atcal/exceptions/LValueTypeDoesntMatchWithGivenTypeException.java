package client.blogic.testing.atcal.exceptions;

public class LValueTypeDoesntMatchWithGivenTypeException extends CheckedException {
	private static final long serialVersionUID = 5037745492323203793L;

	private static final String THE_VALUE                   = "The value ";
	private static final String WITH_TYPE                   = " with type ";
	private static final String DOESNT_MATCH_THE_GIVEN_TYPE = " doesn't match the given type ";

	public LValueTypeDoesntMatchWithGivenTypeException(String lvalueName, String lvalueType, String givenType) {
		super(THE_VALUE + lvalueName + WITH_TYPE + lvalueType + DOESNT_MATCH_THE_GIVEN_TYPE + givenType);
	}

	public LValueTypeDoesntMatchWithGivenTypeException(String lvalueName, String lvalueType, String givenType, Throwable err) {
		super(THE_VALUE + lvalueName + WITH_TYPE + lvalueType + DOESNT_MATCH_THE_GIVEN_TYPE + givenType, err);
	}
}
