package client.blogic.testing.atcal.helpers;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.ArrayType;
import client.blogic.testing.atcal.ContractType;
import client.blogic.testing.atcal.OuterAPLScopeInfo;
import client.blogic.testing.atcal.apl.APLLValue;

public class VariableNamingHelper {

	private static final String _TMP      = "_tmp";
	private static final String _IN       = "_in";
	private static final String CONNECTOR = "_";

	/**
	 * Builds a record variable name using {@code scopeName} and {@code outerAPLScopeInfo}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  recordName        Scope's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Record variable name
	 */
	public static String buildDefinitiveRecordVariableName(String recordName, OuterAPLScopeInfo outerAPLScopeInfo) {
		String finalRecordName = recordName.replaceAll("\\.", CONNECTOR);
		return finalRecordName + buildSuffix(recordName, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code memberName} and {@code suffix}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  memberName        Members's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Record variable name
	 */
	public static String buildDefinitiveRecordMemberVariableName(String memberName, OuterAPLScopeInfo outerAPLScopeInfo) {
		return memberName + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a record member variable name using {@code outerAPLScopeInfo.getName()} as scopeName, {@code memberName} and
	 * {@code suffix}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  memberName Contract member's name
	 * @return            Record member variable name with scope name as prefix
	 */
	public static String buildDefinitiveRecordMemberVariableNameWithScope(String memberName,
	                                                                      OuterAPLScopeInfo outerAPLScopeInfo) {
		String recordName = outerAPLScopeInfo.getName();
		return recordName + CONNECTOR + memberName + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a record member variable name using {@code outerAPLScopeInfo.getUpperAPLScope().getName()} as scopeName,
	 * {@code memberName} and {@code suffix}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  memberName Contract member's name
	 * @return            Record member variable name with upper scope name as prefix
	 */
	public static String buildDefinitiveRecordMemberVariableNameWithUpperScope(String memberName,
	                                                                           OuterAPLScopeInfo outerAPLScopeInfo) {
		String upperScopeName = outerAPLScopeInfo.getUpperAPLScope().getName();
		return upperScopeName + CONNECTOR + memberName + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code memberName} and {@code suffix}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  variableName      Variable's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Variable name
	 */
	public static String buildDefinitiveVariableName(String variableName, OuterAPLScopeInfo outerAPLScopeInfo) {
		return variableName + buildSuffix(variableName, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code contractName}.
	 * This variable name is meant to be used as intermediate
	 * 
	 * @param  contractName
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract variable name
	 */
	public static String buildTemporalContractVariableName(String contractName, OuterAPLScopeInfo outerAPLScopeInfo) {
		String finalScopeName = contractName.replaceAll("\\.", CONNECTOR);
		return finalScopeName + _TMP + buildSuffix(contractName, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code contractName} and {@code outerAPLScopeInfo} name as prefix.
	 * This variable name is meant to be used as intermediate
	 * 
	 * @param  contractName
	 * @param  outerAPLScopeInfo
	 * @return
	 */
	public static String buildTemporalContractVariableNameWithScope(String contractName, OuterAPLScopeInfo outerAPLScopeInfo) {
		String finalScopeName = contractName.replaceAll("\\.", CONNECTOR);
		String scopeName      = outerAPLScopeInfo.getName();
		return scopeName + CONNECTOR + finalScopeName + _TMP + buildSuffix(contractName, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code contractName} and both the {@code outerAPLScopeInfo} name and the upper APL Scope name
	 * as prefix
	 * 
	 * @param  contractName
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return
	 */
	public static String buildTemporalContractVariableNameWithUpperScope(String contractName,
	                                                                     OuterAPLScopeInfo outerAPLScopeInfo) {
		String    finalScopeName = contractName.replaceAll("\\.", CONNECTOR);
		APLLValue upperAPLScope  = outerAPLScopeInfo.getUpperAPLScope();
		String    upperScopeName = upperAPLScope != null ? upperAPLScope.getName() : "";

		return upperScopeName + CONNECTOR + finalScopeName + _TMP + buildSuffix(contractName, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code scopeName} and {@code suffix}.
	 * This variable name is meant to be used as intermediate
	 * 
	 * @param  scopeName
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract variable name
	 */
	public static String buildTemporalInnerContractVariableName(String scopeName, OuterAPLScopeInfo outerAPLScopeInfo) {
		String finalScopeName = scopeName.replaceAll("\\.", CONNECTOR);
		return finalScopeName + _IN + _TMP + buildSuffix(scopeName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract setter argument variable name using {@code setterArgument} and {@code suffix}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  setterArgument Contract setter argument's name
	 * @return                Contract setter argument name
	 */
	public static String buildDefinitiveContractSetterArgumentName(String setterArgument, OuterAPLScopeInfo outerAPLScopeInfo) {
		return setterArgument + buildSuffix(setterArgument, outerAPLScopeInfo);
	}

	/**
	 * Builds a variable name using {@code scopeName}, {@code memberName} and {@code suffix}.
	 * This variable name is meant to be used as intermediate
	 * 
	 * @param  scopeName
	 * @param  memberName Contract member's name
	 * @return            Contract variable name
	 */
	public static String buildTemporalContractMemberVariableName(String scopeName,
	                                                             String memberName,
	                                                             OuterAPLScopeInfo outerAPLScopeInfo) {
		String finalScopeName = scopeName.replaceAll("\\.", CONNECTOR);
		return finalScopeName + CONNECTOR + memberName + _TMP + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract member variable name using {@code memberName} and using info from {@code outerAPLScopeInfo}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  memberName        Contract setter argument's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract member variable name
	 */
	public static String buildDefinitiveContractMemberVariableName(String memberName, OuterAPLScopeInfo outerAPLScopeInfo) {
		return memberName + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract member variable name (preceded with the scope's name) using {@code memberName} and using info from
	 * {@code outerAPLScopeInfo}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  memberName        Contract setter argument's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract member variable name with scope name as prefix
	 */
	public static String buildDefinitiveContractMemberVariableNameWithScope(String memberName,
	                                                                        OuterAPLScopeInfo outerAPLScopeInfo) {
		String scopeName = outerAPLScopeInfo.getName();
		return scopeName + CONNECTOR + memberName + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract member variable name (preceded with the upper scope's name) using {@code memberName} and using info from
	 * {@code outerAPLScopeInfo}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  memberName        Contract setter argument's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract member variable name with scope name as prefix
	 */
	public static String buildDefinitiveContractMemberVariableNameWithUpperScope(String memberName,
	                                                                             OuterAPLScopeInfo outerAPLScopeInfo) {
		String upperScopeName = outerAPLScopeInfo.getUpperAPLScope().getName();
		return upperScopeName + CONNECTOR + memberName + buildSuffix(memberName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract variable name using {@code contractName} and using info from {@code outerAPLScopeInfo}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  contractName      Contract's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract variable name
	 */
	public static String buildDefinitiveContractVariableName(String contractName, OuterAPLScopeInfo outerAPLScopeInfo) {
		return contractName + buildSuffix(contractName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract variable name (preceded with the scope's name) using {@code contractName} and using info from
	 * {@code outerAPLScopeInfo}.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  contractName      Contract's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract variable name with scope name as prefix
	 */
	public static String buildDefinitiveContractVariableNameWithScope(String contractName, OuterAPLScopeInfo outerAPLScopeInfo) {
		String scopeName = outerAPLScopeInfo.getName();
		return scopeName + CONNECTOR + contractName + buildSuffix(contractName, outerAPLScopeInfo);
	}

	/**
	 * Builds a contract member variable name (preceded with the scope's name) using {@code memberName}
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  scopeName
	 * @param  memberName Contract setter argument's name
	 * @return            Contract member variable name
	 */
	public static String buildSimpleMemberVariableName(String scopeName, String memberName) {
		String finalScopeName = scopeName.replaceAll("\\.", CONNECTOR);
		return finalScopeName + CONNECTOR + memberName;
	}

	/**
	 * Builds a contract constructor variable name using {@code scopeName}, {@code constructorName} and {@code suffix}.
	 * This variable name is meant to be used as intermediate
	 * 
	 * @param  scopeName
	 * @param  constructorName   Contract constructor's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Contract constructor variable name
	 */
	public static String buildTemporalContractConstructorVariableName(String scopeName,
	                                                                  String constructorName,
	                                                                  OuterAPLScopeInfo outerAPLScopeInfo) {
		return buildTemporalContractMemberVariableName(scopeName, constructorName, outerAPLScopeInfo);
	}

	/**
	 * Builds an array inner array variable name (preceded with the scope's name) using {@code innerArrayName}
	 * and {@code outerAPLScopeInfo}'s counter as suffix.
	 * This variable name is meant to be used as definitive
	 * 
	 * @param  innerArrayName    Contract setter argument's name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   Array inner array variable name with scope name as prefix and counter as suffix
	 */
	public static String buildDefinitiveArrayInsideArrayVariableNameWithScope(String innerArrayName,
	                                                                          OuterAPLScopeInfo outerAPLScopeInfo) {
		String scopeName = outerAPLScopeInfo.getName();
		return scopeName + CONNECTOR + innerArrayName + outerAPLScopeInfo.getCounter();
	}

	/**
	 * Builds a variable using {@code name} and the counter in {@code outerAPLScopeInfo} as suffix
	 * 
	 * @param  name
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   A variable name with suffix
	 */
	public static String buildDefinitiveVariableNameWithSuffix(String name, OuterAPLScopeInfo outerAPLScopeInfo) {
		return name + outerAPLScopeInfo.getCounter();
	}

	/**
	 * Builds a variable using {@code outerAPLScopeInfo name} as prefix, {@code name} and the counter in {@code outerAPLScopeInfo}
	 * as suffix
	 * 
	 * @param  name
	 * @param  outerAPLScopeInfo
	 * @return                   A variable name with scope's name as prefix and its counter as suffix
	 */
	public static String buildDefinitiveVariableNameWithSuffixAndScope(String name, OuterAPLScopeInfo outerAPLScopeInfo) {
		return outerAPLScopeInfo.getName() + CONNECTOR + name + outerAPLScopeInfo.getCounter();
	}

	/**
	 * Concatenates {@code scopeName} with {@code name} using the {@link #CONNECTOR} literal
	 * 
	 * @param  scopeName
	 * @param  name
	 * @return           Concatenation of {@code scopeName} with {@code name} using the {@link #CONNECTOR} literal
	 */
	public static String buildCompoundVariableName(String scopeName, String name) {
		return scopeName + CONNECTOR + name;
	}

	/**
	 * Builds a temporal variable name
	 * 
	 * @param  name
	 * @return      A temporal variable name
	 */
	public static String buildTemporalVariableName(String name) {
		return name + _TMP;
	}

	/**
	 * It's the methods that knows how to calculate the correct suffix based on {@code outerAPLScopeInfo}
	 * 
	 * @param  memberName
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return
	 */
	private static String buildSuffix(String memberName, OuterAPLScopeInfo outerAPLScopeInfo) {
		String    resultSuffix;
		ATCALType aplScopeType;
		String    suffix = outerAPLScopeInfo.getCounter().toString();
		aplScopeType = outerAPLScopeInfo.getAplScope().getType();
		if (aplScopeType.contains(memberName) || outerAPLScopeInfo.getDepth() > 1) {
			resultSuffix = suffix;
		} else if (aplScopeType instanceof ArrayType) {
			resultSuffix = suffix;
		} else {
			if (memberName.equals(outerAPLScopeInfo.getName())) {
				aplScopeType = outerAPLScopeInfo.getAplScope().getType();
			} else if (outerAPLScopeInfo.isContractRefiningSetOrList()) {
				aplScopeType = ((ContractType) outerAPLScopeInfo.getAplScope().getType()).getUniqueMemberType();
			} else {
				aplScopeType = outerAPLScopeInfo.getAplScope().getType();
			}

			if (aplScopeType.contains(memberName) || outerAPLScopeInfo.getDepth() > 1) {
				resultSuffix = suffix;
			} else {
				resultSuffix = "";
			}
		}
		return resultSuffix;
	}

}
