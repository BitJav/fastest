package client.blogic.testing.atcal.helpers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.ArrayType;
import client.blogic.testing.atcal.ConstantMapper;
import client.blogic.testing.atcal.ContractType;
import client.blogic.testing.atcal.EnumType;
import client.blogic.testing.atcal.OuterAPLScopeInfo;
import client.blogic.testing.atcal.RecordType;
import client.blogic.testing.atcal.RefinementLawEvaluator;
import client.blogic.testing.atcal.apl.APLLValue;
import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.apl.CodeBlock;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.apl.expressions.ConsExpr;
import client.blogic.testing.atcal.apl.statements.APLStmt;
import client.blogic.testing.atcal.apl.statements.ArrayDeclStmt;
import client.blogic.testing.atcal.apl.statements.AssignStmt;
import client.blogic.testing.atcal.apl.statements.ConstructorCallStmt;
import client.blogic.testing.atcal.apl.statements.RecordStmt;
import client.blogic.testing.atcal.apl.statements.SetterCallStmt;
import client.blogic.testing.atcal.exceptions.AllMembersMustBeIncludedInWithClause;
import client.blogic.testing.atcal.mappingInfo.ElementInfo;
import client.blogic.testing.atcal.mappingInfo.ElementsInfo;
import client.blogic.testing.atcal.mappingInfo.Tuple;
import client.blogic.testing.atcal.parser.AtcalParser;
import client.blogic.testing.atcal.parser.AtcalParser.ConstMapContext;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprConst;
import client.blogic.testing.atcal.z.ast.ZExprList;
import client.blogic.testing.atcal.z.ast.ZExprSet;

public class StatementsHelper {

	/**
	 * Generates assignment statements to fill the array described by {@code aplArray}
	 * 
	 * @param  outerAPLScopeInfo
	 * @param  aplArray          Array to be filled
	 * @param  innerLValueName   Left value name corresponding to the lawRefinement inside the array with clause
	 * @return                   Assignment statements to fill the array described by {@code aplArray}
	 */
	public static CodeBlock generateFinalArrayAssignmentStatements(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                               APLArray aplArray,
	                                                               String innerLValueName) {

		CodeBlock assingStmts    = new CodeBlock();
		Integer   counter        = outerAPLScopeInfo.getCounter();
		ATCALType arrayInnerType = ((ArrayType) aplArray.getType()).getType();
		outerAPLScopeInfo.setCounter(0);
		String    contractVariableName;
		APLVar    contractVar;
		APLVar    arrayIndexVar;
		String    definitiveLValueName = innerLValueName;
		int       lvalueLength         = innerLValueName.length();
		char      firstChar            = innerLValueName.charAt(0);
		char      lastChar             = innerLValueName.charAt(lvalueLength - 1);
		APLLValue aplArrayIndex        = null;
		String    newIndex;
		for (int i = 0; i < counter; i++) {
			if (firstChar == '[' && lastChar == ']' && lvalueLength == 2) {
				aplArrayIndex        = aplArray.getNextIndex();
				definitiveLValueName = RefinementHelper.removeSquareBrackets(aplArrayIndex.getName());
			} else if (firstChar == '[' && lastChar == ']') {
				newIndex             = innerLValueName.substring(1, lvalueLength - 1);
				aplArrayIndex        = aplArray.getIndex(Integer.valueOf(newIndex));
				definitiveLValueName = RefinementHelper.removeSquareBrackets(aplArrayIndex.getName());
				definitiveLValueName = definitiveLValueName.substring(0, definitiveLValueName.length() - 1) + newIndex;
			} else {
				aplArrayIndex        = aplArray.getNextIndex();
			}
			arrayIndexVar        = new APLVar(aplArrayIndex.getName(), arrayInnerType);
			contractVariableName = VariableNamingHelper.buildDefinitiveVariableNameWithSuffixAndScope(definitiveLValueName,
			                                                                                          outerAPLScopeInfo);
			contractVar          = new APLVar(contractVariableName, arrayInnerType);
			assingStmts.addStmt(new AssignStmt(arrayIndexVar, contractVar));
			outerAPLScopeInfo.incrementCounter();
		}
		return assingStmts;
	}

	public static CodeBlock generateArrayInnerContractStmts(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                        ElementsInfo elementsInfo,
	                                                        APLArray aplArray,
	                                                        ContractType contractType) {
		CodeBlock                                 codeBlock              = new CodeBlock();
		ElementInfo                               contractInfo           = elementsInfo.getContractInfo();
		Map<String, List<Tuple<String, APLExpr>>> contractMembersMap     = contractInfo.getElementMembersMap();
		Map<String, ATCALType>                    contractMembersTypeMap = contractInfo.getElementMembersTypeMap();
		if (!contractMembersMap.isEmpty()) {
			List<String> setterArguments      = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
			List<String> constructorArguments = RefinementHelper.setToList(contractType.getConstArgs().keySet());
			if (contractInfo.areElementsInElementsMap(setterArguments)) {
				ArrayType arrayType = (ArrayType) aplArray.getType();
				APLLValue aplArrayIndex;
				APLVar    arrayIndexVar;
				String    contractVarName;
				APLVar    contractVar;
				CodeBlock contractAssignStatements;

				int quantityToBeCreated = calculateNumberOfElementstoBeCreated(contractMembersMap, setterArguments,
				                                                               arrayType.getSize());
				outerAPLScopeInfo.setCounter(0);
				for (int i = 0; i < quantityToBeCreated; i++) {
					aplArrayIndex   = aplArray.getNextIndex();
					contractVarName = VariableNamingHelper.buildTemporalContractVariableName(outerAPLScopeInfo.getName(),
					                                                                         outerAPLScopeInfo);
					contractVar     = new APLVar(contractVarName, contractType);

					contractAssignStatements = generateContractAssignmentStatements(outerAPLScopeInfo, contractMembersMap,
					                                                                contractMembersTypeMap, setterArguments,
					                                                                constructorArguments, contractVar, i);
					codeBlock.join(contractAssignStatements);
					arrayIndexVar = new APLVar(aplArrayIndex.getName(), contractType);
					codeBlock.addStmt(new AssignStmt(arrayIndexVar, contractVar));
					outerAPLScopeInfo.incrementCounter();
				}
			} else {
				// throw new AllMembersMustBeIncludedInWithClause(outerAPLScopeInfo.getName());
			}
		}
		return codeBlock;
	}

	public static CodeBlock generateArrayInnerRecordStmts(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                      ElementsInfo elementsInfo,
	                                                      RecordType type,
	                                                      APLArray aplArray) {
		CodeBlock                                 codeBlock            = new CodeBlock();
		ElementInfo                               recordInfo           = elementsInfo.getRecordInfo();
		Map<String, List<Tuple<String, APLExpr>>> recordMembersMap     = recordInfo.getElementMembersMap();
		Map<String, ATCALType>                    recordMembersTypeMap = recordInfo.getElementMembersTypeMap();
		if (!recordMembersMap.isEmpty()) {
			List<String> recordMembers = type.getMembers();
			if (recordInfo.areElementsInElementsMap(recordMembers)) {
				CodeBlock recordAssignStatements = new CodeBlock();
				ArrayType arrayType              = (ArrayType) aplArray.getType();
				int       quantityToBeCreated    = calculateNumberOfElementstoBeCreated(recordMembersMap, recordMembers,
				                                                                        arrayType.getSize());
				outerAPLScopeInfo.setCounter(0);
				for (int i = 0; i < quantityToBeCreated; i++) {
					APLLValue aplArrayIndex = aplArray.getNextIndex();
					APLVar    recordVar     = new APLVar(aplArrayIndex.getName(), type);

					recordAssignStatements = generateRecordAssignmentStatements(outerAPLScopeInfo, recordMembersMap,
					                                                            recordMembersTypeMap, recordMembers, recordVar,
					                                                            i);
					codeBlock.join(recordAssignStatements);
					outerAPLScopeInfo.incrementCounter();
				}
			} else {
				throw new AllMembersMustBeIncludedInWithClause(outerAPLScopeInfo.getName());
			}
		}
		return codeBlock;
	}

	public static CodeBlock generateContractInnerContractStmts(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                           ElementsInfo elementsInfo,
	                                                           ContractType outerContractType,
	                                                           ContractType contractType) {
		CodeBlock                                 codeBlock                  = new CodeBlock();
		ElementInfo                               contractContractMemberInfo = elementsInfo.getContractContractMemberInfo();
		Map<String, List<Tuple<String, APLExpr>>> contractMembersMap         = contractContractMemberInfo.getElementMembersMap();
		Map<String, ATCALType>                    contractMembersTypeMap     = contractContractMemberInfo
		    .getElementMembersTypeMap();
		if (!contractMembersMap.isEmpty()) {
			List<String> setterArguments      = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
			List<String> outerSetterArguments = RefinementHelper.setToList(outerContractType.getSetterArgs().keySet());
			if (contractContractMemberInfo.areElementsInElementsMap(setterArguments)) {
				APLVar    contractVar;
				APLVar    innerContractVar;
				String    innerContractVarName;
				CodeBlock contractAssignStatements;

				String       outerSetterArgumentName = outerContractType.getUniqueMemberName();
				ATCALType    outerSetterArgumentType = outerContractType.getUniqueMemberType();
				List<String> constructorArguments    = RefinementHelper.setToList(contractType.getConstArgs().keySet());
				int          quantityToBeCreated     = calculateNumberOfElementstoBeCreated(contractMembersMap, setterArguments);
				String       outerConstructorVarName = VariableNamingHelper
				    .buildTemporalContractVariableName(outerAPLScopeInfo.getName(), outerAPLScopeInfo);
				APLVar       outerConstructorVar     = new APLVar(outerConstructorVarName, outerContractType);
				outerAPLScopeInfo.setCounter(0);

				for (int i = 0; i < quantityToBeCreated; i++) {
					innerContractVarName     = VariableNamingHelper
					    .buildTemporalInnerContractVariableName(outerAPLScopeInfo.getName(), outerAPLScopeInfo);
					innerContractVar         = new APLVar(innerContractVarName, contractType);
					contractAssignStatements = generateContractAssignmentStatements(outerAPLScopeInfo, contractMembersMap,
					                                                                contractMembersTypeMap, setterArguments,
					                                                                constructorArguments, innerContractVar, i);
					codeBlock.join(contractAssignStatements);
					contractVar = new APLVar(outerSetterArgumentName, outerSetterArgumentType);
					codeBlock.addStmt(new AssignStmt(contractVar, innerContractVar));
					codeBlock.addStmt(new SetterCallStmt(outerConstructorVar, outerSetterArguments));
					outerAPLScopeInfo.incrementCounter();
				}
			} else {
				throw new AllMembersMustBeIncludedInWithClause(outerAPLScopeInfo.getName());
			}
		}
		return codeBlock;
	}

	public static CodeBlock generateContractInnerRecordStmts(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                         ElementsInfo elementsInfo,
	                                                         ContractType outerContractType,
	                                                         RecordType recordType) {
		CodeBlock                                 codeBlock                = new CodeBlock();
		ElementInfo                               contractRecordMemberInfo = elementsInfo.getContractRecordMemberInfo();
		Map<String, List<Tuple<String, APLExpr>>> recordMembersMap         = contractRecordMemberInfo.getElementMembersMap();
		Map<String, ATCALType>                    recordMembersTypeMap     = contractRecordMemberInfo.getElementMembersTypeMap();
		if (!recordMembersMap.isEmpty()) {
			List<String> recordMembers = recordType.getMembers();
			if (contractRecordMemberInfo.areElementsInElementsMap(recordMembers)) {
				int       quantityToBeCreated = calculateNumberOfElementstoBeCreated(recordMembersMap, recordMembers);
				CodeBlock recordAssignStatements;
				outerAPLScopeInfo.setCounter(0);

				for (int i = 0; i < quantityToBeCreated; i++) {
					recordAssignStatements = generateRecordAssignmentStatementsForContract(outerAPLScopeInfo, recordMembersMap,
					                                                                       recordMembersTypeMap, recordMembers,
					                                                                       outerContractType, i);
					codeBlock.join(recordAssignStatements);
					outerAPLScopeInfo.incrementCounter();
				}
			} else {
				throw new AllMembersMustBeIncludedInWithClause(outerAPLScopeInfo.getName());
			}
		}
		return codeBlock;
	}

	/**
	 * Generates the statements needed to fill the contract
	 * 
	 * @param  outerAPLScopeInfo    Contains information about the scope where the record is
	 * @param  recordMembersMap     Map that links record member's names with its modified member's names and corresponding value
	 * @param  recordMembersTypeMap Map that links record member's names with its types
	 * @param  recordMembers        List of the record's members
	 * @param  contractType         Contract that holds the record as unique member
	 * @param  elementIndex         Index of the element currently been created
	 * @return                      Statements needed to fill the contract
	 */
	private static CodeBlock generateRecordAssignmentStatementsForContract(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                                       Map<String, List<Tuple<String, APLExpr>>> recordMembersMap,
	                                                                       Map<String, ATCALType> recordMembersTypeMap,
	                                                                       List<String> recordMembers,
	                                                                       ContractType contractType,
	                                                                       int elementIndex) {
		CodeBlock codeBlock             = new CodeBlock();
		ATCALType contractMemberType    = contractType.getUniqueMemberType();
		String    contractMemberName    = contractType.getUniqueMemberName();
		String    modifiedRecordVarName = VariableNamingHelper.buildDefinitiveRecordMemberVariableName(contractMemberName,
		                                                                                               outerAPLScopeInfo);
		APLVar    modifiedRecordVar     = new APLVar(modifiedRecordVarName, contractMemberType);

		CodeBlock intermediateAssignStatements = generateRecordAssignmentStatements(outerAPLScopeInfo, recordMembersMap,
		                                                                            recordMembersTypeMap, recordMembers,
		                                                                            modifiedRecordVar, elementIndex);
		codeBlock.join(intermediateAssignStatements);

		APLVar recordVar = new APLVar(contractMemberName, contractMemberType);
		codeBlock.addStmt(new AssignStmt(recordVar, modifiedRecordVar));

		String       constructorVarName = VariableNamingHelper.buildTemporalContractVariableName(outerAPLScopeInfo.getName(),
		                                                                                         outerAPLScopeInfo);
		APLVar       constructorVar     = new APLVar(constructorVarName, contractType);
		List<String> setterArguments    = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
		codeBlock.addStmt(new SetterCallStmt(constructorVar, setterArguments));

		return codeBlock;
	}

	/**
	 * Generates the assignment statements needed to link a record variable to the record itself
	 * 
	 * @param  outerAPLScopeInfo    Contains information about the scope where the record is
	 * @param  recordMembersMap     Map that links record member's names with its modified member's names and corresponding value
	 * @param  recordMembersTypeMap Map that links record member's names with its types
	 * @param  recordMembers        List of the record's members
	 * @param  modifiedRecordVar    An {@link APLVar} that represents the record in an intermediate state
	 * @param  elementIndex         Index of the element currently been created
	 * @return                      Assignment statements needed to link a record variable to the record itself
	 */
	private static CodeBlock generateRecordAssignmentStatements(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                            Map<String, List<Tuple<String, APLExpr>>> recordMembersMap,
	                                                            Map<String, ATCALType> recordMembersTypeMap,
	                                                            List<String> recordMembers,
	                                                            APLVar modifiedRecordVar,
	                                                            int elementIndex) {
		CodeBlock codeBlock = new CodeBlock();
		CodeBlock intermediateAssignStatements;
		String    recordMemberName;
		ATCALType recordMemberType;
		String    modifiedRecordMemberName;
		APLExpr   value;
		// String finalRecordMemberName;

		for (int i = 0; i < recordMembers.size(); i++) {
			recordMemberName             = recordMembers.get(i);
			recordMemberType             = recordMembersTypeMap.get(recordMemberName);
			modifiedRecordMemberName     = VariableNamingHelper
			    .buildDefinitiveRecordMemberVariableNameWithScope(recordMemberName, outerAPLScopeInfo);
			value                        = recordMembersMap.get(recordMemberName).get(elementIndex).getValue();
			intermediateAssignStatements = generateAssignmentStatement(modifiedRecordMemberName, recordMemberType, value);
			codeBlock.join(intermediateAssignStatements);
		}
		// Add the record assignment
		List<String> indexedRecordMembers = RefinementHelper.generateIndexedMemberNames(recordMembers, outerAPLScopeInfo);
		codeBlock.addStmt(new RecordStmt(modifiedRecordVar, indexedRecordMembers));

		return codeBlock;
	}

	/**
	 * Generates the assignment statements needed to link a contract variable to the contract itself
	 * 
	 * @param  outerAPLScopeInfo      Contains information about the scope where the record is
	 * @param  contractMembersMap     Map that links contract member's names with its modified member's names and corresponding
	 *                                value
	 * @param  contractMembersTypeMap Map that links contract member's names with its types
	 * @param  setterArguments        List of the contract's members
	 * @param  constructorArguments
	 * @param  modifiedContractVar    An {@link APLVar} that represents the contract in an intermediate state
	 * @param  elementIndex           Index of the element currently been created
	 * @return                        Assignment statements needed to link a contract variable to the contract itself
	 */
	private static CodeBlock generateContractAssignmentStatements(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                              Map<String, List<Tuple<String, APLExpr>>> contractMembersMap,
	                                                              Map<String, ATCALType> contractMembersTypeMap,
	                                                              List<String> setterArguments,
	                                                              List<String> constructorArguments,
	                                                              APLVar modifiedContractVar,
	                                                              int elementIndex) {
		CodeBlock codeBlock = new CodeBlock();

		String                 setterArgumentName;
		ATCALType              setterArgumentType;
		String                 modifiedSetterArgumentName;
		APLExpr                value;
		CodeBlock              assignStatements;
		Tuple<String, APLExpr> currentTuple;
		// Add the constructor call statement
		codeBlock.addStmt(new ConstructorCallStmt(modifiedContractVar, constructorArguments));
		for (int j = 0; j < setterArguments.size(); j++) {
			setterArgumentName         = setterArguments.get(j);
			setterArgumentType         = contractMembersTypeMap.get(setterArgumentName);
			currentTuple               = contractMembersMap.get(setterArgumentName).get(elementIndex);
			modifiedSetterArgumentName = currentTuple.getKey();
			value                      = currentTuple.getValue();
			assignStatements           = generateAssignmentStatement(modifiedSetterArgumentName, setterArgumentType, value);
			codeBlock.join(assignStatements);
		}
		// Add the setter call statement
		List<String> indexedSetterArguments = RefinementHelper.generateIndexedMemberNames(setterArguments, outerAPLScopeInfo);
		codeBlock.addStmt(new SetterCallStmt(modifiedContractVar, indexedSetterArguments));

		return codeBlock;
	}

	/**
	 * Generates an assignment statement that links {@code modifiedMemberName} to {@code value}<br>
	 * Omits the assignment if assigning a variable to itself<br>
	 * 
	 * @param  modifiedMemberName Modified Record member name
	 * @param  memberType
	 * @param  value
	 * @return                    An instance of {@link CodeBlock} formed by an assignment statement
	 */
	public static CodeBlock generateAssignmentStatement(String modifiedMemberName, ATCALType memberType, APLExpr value) {
		CodeBlock assignStatments   = new CodeBlock();
		APLVar    modifiedMemberVar = new APLVar(modifiedMemberName, memberType);

		if (!(value instanceof APLVar) || !modifiedMemberName.equals(((APLVar) value).getName())) {
			assignStatments.addStmt(new AssignStmt(modifiedMemberVar, value));
		}
		return assignStatments;
	}

	/**
	 * @param  evaluator  {@link RefinementLawEvaluator} needed to get the map that holds the constants mappings
	 * @param  aplScope   Variable which will implement {@code zScope}
	 * @param  zExprConst Z expression that contains the values to be translated. Must be of type ZExprSet, ZExprList or ZExprProd
	 * @param  enumType   Enum type needed to get implementation enum value
	 * @param  constMaps  Context that holds mapping info between z enum and implementation enum values
	 * @return            An {@link AssignStmt} assigning the implementation variable to its corresponding enum implementation
	 *                    value
	 */
	public static AssignStmt generateEnumAssigment(RefinementLawEvaluator evaluator,
	                                               APLLValue aplScope,
	                                               ZExprConst zExprConst,
	                                               EnumType enumType,
	                                               List<ConstMapContext> constMaps) {
		Map<ZExprConst, ConsExpr> customMap = Maps.newHashMap();
		for (AtcalParser.ConstMapContext constMap : constMaps) {
			customMap.put(ZExprConst.basic(constMap.ID(0).getText(), zExprConst.getZVarName()),
			              enumType.getElemByName(constMap.ID(1).getText()));
		}
		OuterAPLScopeInfo outerAPLScopeInfo = evaluator.getOuterAPLScopeInfo();

		APLExpr aplExpr    = evaluator.getZVarConstantMaps().getOrDefault(zExprConst.getZVarName(), new ConstantMapper())
		    .toEnum(zExprConst, customMap);
		String  aplVarName = aplScope.getName();
		if (outerAPLScopeInfo != null) {
			APLLValue outerAPLScope = outerAPLScopeInfo.getAplScope();
			if (outerAPLScope.getType() instanceof ContractType) {
				aplVarName = VariableNamingHelper.buildDefinitiveContractMemberVariableNameWithScope(aplScope.getName(),
				                                                                                     outerAPLScopeInfo);
			} else if (outerAPLScope.getType() instanceof RecordType) {
				aplVarName = VariableNamingHelper.buildDefinitiveRecordMemberVariableNameWithScope(aplScope.getName(),
				                                                                                   outerAPLScopeInfo);
			} else {
				aplVarName = VariableNamingHelper.buildDefinitiveVariableName(aplScope.getName(), outerAPLScopeInfo);
			}
		}
		APLVar aplVar = new APLVar(aplVarName, aplScope.getType());
		return new AssignStmt(aplVar, aplExpr);
	}

	/**
	 * Generates statements to hold data from {@code expr} in a unary contract.
	 * 
	 * @param  expr              Data to be saved in the contract
	 * @param  newAPLScope       APL value with a unary contract as type
	 * @param  outerAPLScopeInfo
	 * @return                   Statements to hold data from {@code expr} in a unary contract.
	 */
	public static CodeBlock generateUnaryContractStmts(ZExpr expr, APLLValue newAPLScope, OuterAPLScopeInfo outerAPLScopeInfo) {
		CodeBlock         contractStmts              = new CodeBlock();
		OuterAPLScopeInfo newOuterAPLScopeInfo       = new OuterAPLScopeInfo(newAPLScope, true, outerAPLScopeInfo);
		String            newAPLScopeName            = newAPLScope.getName();
		ContractType      contractType               = (ContractType) newAPLScope.getType();
		ATCALType         contractInnerType          = contractType.getUniqueMemberType();
		String            contractSetterArgumentName = contractType.getUniqueMemberName();
		Iterator<ZExpr>   iterator                   = RefinementHelper.getIterator(expr);

		ZExpr        element;
		APLExpr      value;
		String       tempContractVariableName    = VariableNamingHelper
		    .buildTemporalContractVariableNameWithScope(newAPLScopeName, outerAPLScopeInfo);
		APLVar       tempContractVar             = new APLVar(tempContractVariableName, contractType);
		String       memberName                  = VariableNamingHelper
		    .buildDefinitiveContractMemberVariableNameWithScope(contractSetterArgumentName, newOuterAPLScopeInfo);
		APLVar       memberVar                   = new APLVar(memberName, contractInnerType);
		List<String> setterArguments             = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
		List<String> constructorArguments        = RefinementHelper.setToList(contractType.getConstArgs().keySet());
		List<String> indexedSetterArguments      = RefinementHelper.generateIndexedMemberNames(setterArguments,
		                                                                                       newOuterAPLScopeInfo);
		List<String> indexedConstructorArguments = RefinementHelper.generateIndexedMemberNames(constructorArguments,
		                                                                                       newOuterAPLScopeInfo);
		String       contractVariableName        = VariableNamingHelper
		    .buildDefinitiveContractVariableNameWithScope(newAPLScopeName, outerAPLScopeInfo);
		APLVar       contractVar                 = new APLVar(contractVariableName, contractType);

		contractStmts.addStmt(new ConstructorCallStmt(tempContractVar, indexedConstructorArguments));

		while (iterator.hasNext()) {
			element = (ZExpr) iterator.next();
			value   = contractInnerType.fromZExpr(element);
			contractStmts.addStmt(new AssignStmt(memberVar, value));
			contractStmts.addStmt(new SetterCallStmt(tempContractVar, indexedSetterArguments));
			newOuterAPLScopeInfo.incrementCounter();
		}

		contractStmts.addStmt(new AssignStmt(contractVar, tempContractVar));

		return contractStmts;
	}

	/**
	 * Generates statements to hold data from {@code expr} in a unary contract.
	 * This contract has siblings in an upper scope.
	 * 
	 * @param  expr              Data to be saved in the contract
	 * @param  aplScope          APL value with a unary contract as type
	 * @param  outerAPLScopeInfo
	 * @return                   Statements to hold data from {@code expr} in a unary contract.
	 */
	public static CodeBlock generateUnaryContractWithSiblingsStmts(ZExpr expr,
	                                                               APLLValue aplScope,
	                                                               OuterAPLScopeInfo outerAPLScopeInfo) {
		CodeBlock         contractStmts              = new CodeBlock();
		OuterAPLScopeInfo newOuterAPLScopeInfo       = new OuterAPLScopeInfo(outerAPLScopeInfo);
		ContractType      contractType               = (ContractType) aplScope.getType();
		ATCALType         contractInnerType          = contractType.getUniqueMemberType();
		String            contractSetterArgumentName = contractType.getUniqueMemberName();
		Iterator<ZExpr>   iterator                   = RefinementHelper.getIterator(expr);

		ZExpr        element;
		APLExpr      value;
		String       tempContractVariableName    = VariableNamingHelper.buildTemporalContractVariableName(aplScope.getName(),
		                                                                                                  newOuterAPLScopeInfo);
		APLVar       tempContractVar             = new APLVar(tempContractVariableName, contractType);
		String       memberName                  = VariableNamingHelper
		    .buildDefinitiveContractMemberVariableNameWithScope(contractSetterArgumentName, newOuterAPLScopeInfo);
		APLVar       memberVar                   = new APLVar(memberName, contractInnerType);
		List<String> setterArguments             = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
		List<String> constructorArguments        = RefinementHelper.setToList(contractType.getConstArgs().keySet());
		List<String> indexedSetterArguments      = RefinementHelper.generateIndexedMemberNames(setterArguments,
		                                                                                       newOuterAPLScopeInfo);
		List<String> indexedConstructorArguments = RefinementHelper.generateIndexedMemberNames(constructorArguments,
		                                                                                       newOuterAPLScopeInfo);
		String       contractVariableName        = VariableNamingHelper
		    .buildDefinitiveContractMemberVariableNameWithScope(aplScope.getName(), newOuterAPLScopeInfo);
		APLVar       contractVar                 = new APLVar(contractVariableName, contractType);

		contractStmts.addStmt(new ConstructorCallStmt(tempContractVar, indexedConstructorArguments));

		while (iterator.hasNext()) {
			element = (ZExpr) iterator.next();
			value   = contractInnerType.fromZExpr(element);
			contractStmts.addStmt(new AssignStmt(memberVar, value));
			contractStmts.addStmt(new SetterCallStmt(tempContractVar, indexedSetterArguments));
			newOuterAPLScopeInfo.incrementCounter();
		}

		contractStmts.addStmt(new AssignStmt(contractVar, tempContractVar));

		return contractStmts;
	}

	/**
	 * Gets the first nested level ArrayDeclStmt from the {@code codeBlock}
	 * 
	 * @param  codeBlock
	 * @param  aplScopeName
	 * @return              The first nested level ArrayDeclStmt from the {@code codeBlock}
	 */
	public static List<ArrayDeclStmt> getArrayDeclStmts(CodeBlock codeBlock, String aplScopeName) {
		List<ArrayDeclStmt> arrayDeclaredStatements = new ArrayList<>();
		List<APLStmt>       aplStmts                = codeBlock.getStmtList();
		for (APLStmt aplStmt : aplStmts) {
			if (aplStmt instanceof ArrayDeclStmt) {
				ArrayDeclStmt arrayDeclStmt             = (ArrayDeclStmt) aplStmt;
				String        arrayDeclName             = arrayDeclStmt.getName();
				String        arrayDeclNameWithoutScope = arrayDeclName.substring(aplScopeName.length() + 1);
				if (arrayDeclName.startsWith(aplScopeName) && !arrayDeclNameWithoutScope.contains("_")) {
					arrayDeclaredStatements.add((ArrayDeclStmt) aplStmt);
				}
			}
		}
		return arrayDeclaredStatements;
	}

	/**
	 * I use the first of the map's member in order to get the quantity of elements that have to be created
	 * 
	 * @param  <E>         Parameter added to avoid compiling errors
	 * @param  membersMap  Mapping that relates members and list of objects (it doesn't matter what kind of elements)
	 * @param  membersList List of members
	 * @return             Number of element have to be created
	 */
	private static <E> int calculateNumberOfElementstoBeCreated(Map<String, List<E>> membersMap, List<String> membersList) {
		return membersMap.get(membersList.get(0)).size();
	}

	/**
	 * I use the first of the map's member in order to get the quantity of elements that have to be created
	 * 
	 * @param  <E>         Parameter added to avoid compiling errors
	 * @param  membersMap  Mapping that relates members and list of objects (it doesn't matter what kind of elements)
	 * @param  membersList List of members
	 * @return             Number of element have to be created
	 */
	private static <E> int calculateNumberOfElementstoBeCreated(Map<String, List<E>> membersMap,
	                                                            List<String> membersList,
	                                                            int arraySize) {
		return Math.min(membersMap.get(membersList.get(0)).size(), arraySize);
	}

	public static CodeBlock generateContractStmtsFromCollection(ZExpr collectionExpr, APLVar contractVar) {
		CodeBlock       contractStmts        = new CodeBlock();
		ContractType    contractType         = (ContractType) contractVar.getType();
		ATCALType       uniqueMemberType     = contractType.getUniqueMemberType();
		String          uniqueMemberName     = contractType.getUniqueMemberName();
		APLVar          uniqueMemberVar      = new APLVar(uniqueMemberName, uniqueMemberType);
		List<String>    constructorArguments = RefinementHelper.setToList(contractType.getConstArgs().keySet());
		List<String>    setterArguments      = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
		String          tempVariableName     = VariableNamingHelper.buildTemporalVariableName(contractVar.getName());
		APLVar          tempContractVar      = new APLVar(tempVariableName, contractType);
		Iterator<ZExpr> iterator             = RefinementHelper.getIterator(collectionExpr);

		contractStmts.addStmt(new ConstructorCallStmt(tempContractVar, constructorArguments));

		ZExpr   element;
		APLExpr value;
		while (iterator.hasNext()) {
			element = (ZExpr) iterator.next();
			value   = uniqueMemberType.fromZExpr(element);
			contractStmts.addStmt(new AssignStmt(uniqueMemberVar, value));
			contractStmts.addStmt(new SetterCallStmt(tempContractVar, setterArguments));
		}

		contractStmts.addStmt(new AssignStmt(contractVar, tempContractVar));
		return contractStmts;
	}

	/**
	 * Generates assignment statements to fill the contract described by {@code contractVar}
	 * 
	 * @param  outerAPLScopeInfo
	 * @param  contractVar       Array to be filled
	 * @param  innerLValueName   Left value name corresponding to the lawRefinement inside the array with clause
	 * @return                   Assignment statements to fill the array described by {@code aplArray}
	 */
	public static CodeBlock generateFinalContractAssignmentStatements(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                                  APLVar outerContractVar,
	                                                                  String innerLValueName) {
		CodeBlock    assingStmts       = new CodeBlock();
		Integer      counter           = outerAPLScopeInfo.getCounter();
		ContractType outerContractType = (ContractType) outerContractVar.getType();
		String       outerContractName = outerContractVar.getName();
		ATCALType    uniqueMemberType  = ((ContractType) outerContractVar.getType()).getUniqueMemberType();
		outerAPLScopeInfo.setCounter(0);

		List<String> setterArguments = RefinementHelper.setToList(outerContractType.getSetterArgs().keySet());
		List<String> indexedSetterArguments;

		APLLValue aplArrayIndex;
		String    contractVariableName;
		APLVar    contractVar;
		APLVar    arrayIndexVar;
		String    newIndex;
		for (int i = 0; i < counter; i++) {
			// aplArrayIndex = aplArray.getNextIndex();
			// arrayIndexVar = new APLVar(aplArrayIndex.getName(), uniqueMemberType);

			// contractVariableName = VariableNamingHelper.buildDefinitiveVariableNameWithSuffixAndScope(outerContractName,
			// outerAPLScopeInfo);
			// contractVar = new APLVar(outerContractName, uniqueMemberType);

			indexedSetterArguments = RefinementHelper.generateIndexedMemberNames(setterArguments, outerAPLScopeInfo);
			assingStmts.addStmt(new SetterCallStmt(outerContractVar, indexedSetterArguments));
			outerAPLScopeInfo.incrementCounter();
		}
		return assingStmts;
	}

}
