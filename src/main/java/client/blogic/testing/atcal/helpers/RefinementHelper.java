package client.blogic.testing.atcal.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.ArrayType;
import client.blogic.testing.atcal.ConstantMapper;
import client.blogic.testing.atcal.ContractType;
import client.blogic.testing.atcal.EnumType;
import client.blogic.testing.atcal.FloatType;
import client.blogic.testing.atcal.IntType;
import client.blogic.testing.atcal.LValueFactory;
import client.blogic.testing.atcal.OuterAPLScopeInfo;
import client.blogic.testing.atcal.RecordType;
import client.blogic.testing.atcal.ReferenceType;
import client.blogic.testing.atcal.RefinementLawEvaluator;
import client.blogic.testing.atcal.StringType;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.apl.expressions.ConsExpr;
import client.blogic.testing.atcal.apl.expressions.StringExpr;
import client.blogic.testing.atcal.apl.expressions.APLArray.APLArrayIndex;
import client.blogic.testing.atcal.apl.statements.APLStmt;
import client.blogic.testing.atcal.apl.statements.ArrayDeclStmt;
import client.blogic.testing.atcal.apl.statements.AssignStmt;
import client.blogic.testing.atcal.apl.statements.ConstructorCallStmt;
import client.blogic.testing.atcal.apl.statements.RecordStmt;
import client.blogic.testing.atcal.apl.statements.SetterCallStmt;
import client.blogic.testing.atcal.apl.APLLValue;
import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.apl.CodeBlock;
import client.blogic.testing.atcal.exceptions.ArrayIsntLongEnoughToRefineRecord;
import client.blogic.testing.atcal.exceptions.ElemOperatorCantBeUsedMultipleTimesInsideWithClauseException;
import client.blogic.testing.atcal.exceptions.ExpressionMustBeSetOrListInInnerRefinementToEnum;
import client.blogic.testing.atcal.exceptions.InnerArrayTypeMustBeStringException;
import client.blogic.testing.atcal.exceptions.MapClauseNeededException;
import client.blogic.testing.atcal.exceptions.OnlyContractOrRecordMembersAllowedException;
import client.blogic.testing.atcal.exceptions.OnlyEnumVariablesCanBeRefinedAsEnumException;
import client.blogic.testing.atcal.exceptions.ProductCannotBeEmptyException;
import client.blogic.testing.atcal.exceptions.RefinementNotAllowedInScopeException;
import client.blogic.testing.atcal.exceptions.RefinementVariableWronglyUsedInScopeException;
import client.blogic.testing.atcal.exceptions.SpecificationVariableCantBeRefinedToTypeException;
import client.blogic.testing.atcal.exceptions.WithClauseNeededException;
import client.blogic.testing.atcal.exceptions.WithClauseNotAllowedException;
import client.blogic.testing.atcal.exceptions.ZExprMustBeConstantException;
import client.blogic.testing.atcal.mappingInfo.ElementsInfo;
import client.blogic.testing.atcal.parser.AtcalParser.ConstMapContext;
import client.blogic.testing.atcal.parser.AtcalParser.ElemExprContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawRefinementContext;
import client.blogic.testing.atcal.parser.AtcalParser.ProdProjContext;
import client.blogic.testing.atcal.parser.AtcalParser.RefinementContext;
import client.blogic.testing.atcal.parser.AtcalParser.SetCardContext;
import client.blogic.testing.atcal.parser.AtcalParser.SetDomContext;
import client.blogic.testing.atcal.parser.AtcalParser.SetProjContext;
import client.blogic.testing.atcal.parser.AtcalParser.SetRanContext;
import client.blogic.testing.atcal.parser.AtcalParser.WithRefContext;
import client.blogic.testing.atcal.parser.AtcalParser.ZExprContext;
import client.blogic.testing.atcal.parser.AtcalParser.ZExprsContext;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprConst;
import client.blogic.testing.atcal.z.ast.ZExprList;
import client.blogic.testing.atcal.z.ast.ZExprNum;
import client.blogic.testing.atcal.z.ast.ZExprProd;
import client.blogic.testing.atcal.z.ast.ZExprSchema;
import client.blogic.testing.atcal.z.ast.ZExprSet;
import client.blogic.testing.atcal.z.ast.ZExprString;
import client.blogic.testing.atcal.z.ast.ZVar;

public class RefinementHelper {
	public static final String zScopeVarName = "zScope";

	/**
	 * ##############################################################################
	 * ##################### Detailed cases of refinement methods ###################
	 * ##############################################################################
	 * Note:
	 * ----
	 * Notice that the following method definitions are detailed to
	 * very specific cases when some of them can be unified. This is
	 * done in order to differentiate each of them in case that, in
	 * the future, the treatment of some varies somehow
	 * @param ctx 
	 */

	public static CodeBlock refineAsString(RefinementContext ctx,
	                                       RefinementLawEvaluator refinementLawEvaluator,
	                                       APLLValue newAPLScope) {
		CodeBlock                   codeBlock               = new CodeBlock();
		ATCALType                   newAPLScopeType         = newAPLScope.getType();
		Map<String, ConstantMapper> zVarConstantMaps        = refinementLawEvaluator.getZVarConstantMaps();
		APLExpr                     aplExpr                 = null;
		String                      newAPLScopeTypeCategory = newAPLScopeType.typeCategory();
		ContextType                 zScopeType              = refinementLawEvaluator.getzScopeType();
		ZExpr                       zScope                  = refinementLawEvaluator.getZScopeVarible();
		APLLValue                   aplScope                = refinementLawEvaluator.getAPLScope();
		boolean                     insideAPLScope          = aplScope != null ? true : false;
		boolean                     existsWithClause        = ctx.withRef() != null ? true : false;
		if (existsWithClause) {
			throw new WithClauseNotAllowedException(newAPLScopeTypeCategory);
		}
		if (!insideAPLScope) {
			if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
				aplExpr = newAPLScopeType.fromZExpr(zScope);
			} else if (zScope instanceof ZExprProd) {
				throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
			} else if (zScope instanceof ZExprString) {
				aplExpr = newAPLScopeType.fromZExpr(zScope);
			} else if (zScope instanceof ZExprConst) {
				ZExprConst zExprConst = (ZExprConst) zScope;
				aplExpr = zVarConstantMaps.getOrDefault(zExprConst.getZVarName(), new ConstantMapper()).toString(zExprConst);
			} // It should reach to this else-branch when zScope is:
			  // - ZExprAuto
			  // - ZExprNum
			else {
				aplExpr = newAPLScopeType.fromZExpr(zScope);
			}
			AssignStmt assignStmt = new AssignStmt(newAPLScope, aplExpr);
			codeBlock.addStmt(assignStmt);
		} else {
			OuterAPLScopeInfo outerAPLScopeInfo;
			ATCALType         aplScopeType                = aplScope.getType();
			String            aplScopeTypeCategory        = aplScopeType.typeCategory();
			boolean           isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			refinementLawEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					ArrayType       arrayType      = (ArrayType) aplScopeType;
					ATCALType       arrayInnerType = arrayType.getType();
					Iterator<ZExpr> iterator       = getIterator(zScope, arrayType.getSize());
					ZExpr           element;
					String          member;
					String          memberName;
					if (arrayInnerType instanceof ContractType) {
						while (iterator.hasNext()) {
							element           = iterator.next();
							aplExpr           = newAPLScopeType.fromZExpr(element);
							outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
							member            = newAPLScope.getName();
							memberName        = VariableNamingHelper
							    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
							refinementLawEvaluator.getElementsInfo().addContractMemberInfoToMaps(member, memberName, aplExpr,
							                                                                     newAPLScopeType);
							outerAPLScopeInfo.incrementCounter();
						}
					} else if (arrayInnerType instanceof RecordType) {
						while (iterator.hasNext()) {
							element           = iterator.next();
							aplExpr           = newAPLScopeType.fromZExpr(element);
							outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
							member            = newAPLScope.getName();
							memberName        = VariableNamingHelper
							    .buildDefinitiveRecordMemberVariableNameWithScope(member, outerAPLScopeInfo);
							refinementLawEvaluator.getElementsInfo().addRecordMemberInfoToMaps(member, memberName, aplExpr,
							                                                                   newAPLScopeType);
							outerAPLScopeInfo.incrementCounter();
						}
					} else {
						throw new OnlyContractOrRecordMembersAllowedException();
					}

					// I put a return here because at this point all the work have been done
					// and there aren't any APLStmts to add
					return codeBlock;
				} else if (zScope instanceof ZExprProd) {
					throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
				} else if (zScope instanceof ZExprString) {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}
				AssignStmt assignStmt = new AssignStmt(newAPLScope, aplExpr);
				codeBlock.addStmt(assignStmt);
			} else if (aplScopeType instanceof ContractType) {
				String member;
				String memberName;
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (isContractRefiningSetOrList) {
						Iterator<ZExpr> iterator          = getIterator(zScope);
						ContractType    contractType      = (ContractType) aplScopeType;
						ATCALType       contractInnerType = contractType.getUniqueMemberType();
						ZExpr           element;
						if (contractInnerType instanceof ContractType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractContractMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else if (contractInnerType instanceof RecordType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveRecordMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractRecordMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							throw new OnlyContractOrRecordMembersAllowedException();
						}

						return codeBlock;
					} else {
						aplExpr = newAPLScopeType.fromZExpr(zScope);
					}
				} else if (zScope instanceof ZExprProd) {
					throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
				} else if (zScope instanceof ZExprString) {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}
				outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
				member            = newAPLScope.getName();
				memberName        = VariableNamingHelper.buildDefinitiveContractMemberVariableNameWithScope(member,
				                                                                                            outerAPLScopeInfo);
				APLVar     contractMemberVar = new APLVar(memberName, newAPLScopeType);
				AssignStmt assignStmt        = new AssignStmt(contractMemberVar, aplExpr);
				codeBlock.addStmt(assignStmt);
			} else if (aplScopeType instanceof RecordType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				} else if (zScope instanceof ZExprProd) {
					throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
				} else if (zScope instanceof ZExprString) {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}
				outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
				String    recordMember           = newAPLScope.getName();
				String    recordMemberVarName    = VariableNamingHelper
				    .buildDefinitiveRecordMemberVariableNameWithScope(recordMember, outerAPLScopeInfo);
				CodeBlock recordAssignStatements = StatementsHelper.generateAssignmentStatement(recordMemberVarName,
				                                                                                newAPLScopeType, aplExpr);
				codeBlock.join(recordAssignStatements);
			}
		}
		return codeBlock;
	}

	/**
	 * Refines {@code zScope} to an integer or a float
	 * 
	 * @param  ctx
	 * @param  refinementLawEvaluator This refinement law evaluator would be used only to store record and contract
	 *                                member's information into maps
	 * @param  zScopeType             Type of Z context from which the {@code zScope} is generated
	 * @param  zScope                 Z expression to be refined
	 * @param  aplScope               Outer APL scope, if any
	 * @param  newAPLScope            Defines the variable that will hold the {@code zScope} value
	 * @param  insideAPLScope         True if the refinement occurs inside an APL scope
	 * @param  existsWithClause       True if the current refinement context have with-clause
	 * @return                        CodeBlock with the corresponding statements
	 */
	public static CodeBlock refineAsIntOrFloat(RefinementContext ctx,
	                                           RefinementLawEvaluator refinementLawEvaluator,
	                                           APLLValue newAPLScope) {
		CodeBlock   codeBlock               = new CodeBlock();
		ATCALType   newAPLScopeType         = newAPLScope.getType();
		APLExpr     aplExpr                 = null;
		String      newAPLScopeTypeCategory = newAPLScopeType.typeCategory();
		ContextType zScopeType              = refinementLawEvaluator.getzScopeType();
		ZExpr       zScope                  = refinementLawEvaluator.getZScopeVarible();
		APLLValue   aplScope                = refinementLawEvaluator.getAPLScope();
		boolean     insideAPLScope          = aplScope != null ? true : false;
		boolean     existsWithClause        = ctx.withRef() != null ? true : false;
		if (existsWithClause) {
			throw new WithClauseNotAllowedException(newAPLScopeTypeCategory);
		}
		if (!insideAPLScope) {
			if (zScope instanceof ZExprSet || zScope instanceof ZExprList || zScope instanceof ZExprProd
			    || zScope instanceof ZExprString) {
				throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
			} else if (zScope instanceof ZExprConst) {
				if (newAPLScopeType instanceof IntType) {
					Map<String, ConstantMapper> zVarConstantMaps = refinementLawEvaluator.getZVarConstantMaps();
					ZExprConst                  zExprConst       = ((ZExprConst) zScope);
					aplExpr = zVarConstantMaps.getOrDefault(zExprConst.getZVarName(), new ConstantMapper()).toInt(zExprConst);
				} else {
					throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
				}
			} // It should reach to this else-branch when zScope is:
			  // - ZExprAuto
			  // - ZExprNum
			else {
				aplExpr = newAPLScopeType.fromZExpr(zScope);
			}
			AssignStmt assignStmt = new AssignStmt(newAPLScope, aplExpr);
			codeBlock.addStmt(assignStmt);
		} else {
			OuterAPLScopeInfo outerAPLScopeInfo;
			ATCALType         aplScopeType                = aplScope.getType();
			String            aplScopeTypeCategory        = aplScopeType.typeCategory();
			boolean           isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
			// If newAPLScope is incorrectly used inside aplScope, then stop processing and throw exception
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			refinementLawEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (!existsWithClause) {
						ArrayType       arrayType      = (ArrayType) aplScope.getType();
						ATCALType       arrayInnerType = arrayType.getType();
						Iterator<ZExpr> iterator       = getIterator(zScope, arrayType.getSize());
						ZExpr           element;
						String          member;
						String          memberName;
						if (arrayInnerType instanceof ContractType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo().addContractMemberInfoToMaps(member, memberName, aplExpr,
								                                                                     newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else if (arrayInnerType instanceof RecordType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveRecordMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo().addRecordMemberInfoToMaps(member, memberName, aplExpr,
								                                                                   newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							throw new OnlyContractOrRecordMembersAllowedException();
						}
						return codeBlock;
					} else {
						throw new WithClauseNotAllowedException(newAPLScopeType.typeCategory());
					}
				} else if (zScope instanceof ZExprProd) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else if (zScope instanceof ZExprString) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}
				AssignStmt assignStmt = new AssignStmt(newAPLScope, aplExpr);
				codeBlock.addStmt(assignStmt);
			} else if (aplScopeType instanceof ContractType) {
				String member;
				String memberName;
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (isContractRefiningSetOrList) {
						Iterator<ZExpr> iterator          = getIterator(zScope);
						ContractType    contractType      = (ContractType) aplScopeType;
						ATCALType       contractInnerType = contractType.getUniqueMemberType();
						ZExpr           element;
						if (contractInnerType instanceof ContractType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractContractMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else if (contractInnerType instanceof RecordType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveRecordMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractRecordMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							throw new OnlyContractOrRecordMembersAllowedException();
						}

						return codeBlock;
					} else {
						throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
						    aplScopeTypeCategory);
					}
				} else if (zScope instanceof ZExprProd || zScope instanceof ZExprString) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}

				outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
				member            = newAPLScope.getName();
				memberName        = VariableNamingHelper.buildDefinitiveContractMemberVariableNameWithScope(member,
				                                                                                            outerAPLScopeInfo);
				APLVar     contractMemberVar = new APLVar(memberName, newAPLScopeType);
				AssignStmt assignStmt        = new AssignStmt(contractMemberVar, aplExpr);
				codeBlock.addStmt(assignStmt);
			} else if (aplScopeType instanceof RecordType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList || zScope instanceof ZExprProd
				    || zScope instanceof ZExprString) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}
				outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
				String     recordMember        = newAPLScope.getName();
				String     recordMemberVarName = VariableNamingHelper
				    .buildDefinitiveRecordMemberVariableNameWithScope(recordMember, outerAPLScopeInfo);
				APLVar     recordMemberVar     = new APLVar(recordMemberVarName, newAPLScopeType);
				AssignStmt assignStmt          = new AssignStmt(recordMemberVar, aplExpr);
				codeBlock.addStmt(assignStmt);
			}
		}
		return codeBlock;
	}

	public static CodeBlock refineAsEnum(RefinementContext ctx,
	                                     RefinementLawEvaluator refinementLawEvaluator,
	                                     APLLValue newAPLScope) {
		CodeBlock   codeBlock               = new CodeBlock();
		boolean     existsMapClause         = ctx.constMapping() != null ? true : false;
		EnumType    newAPLScopeType         = (EnumType) newAPLScope.getType();
		String      newAPLScopeTypeCategory = newAPLScopeType.typeCategory();
		ContextType zScopeType              = refinementLawEvaluator.getzScopeType();
		ZExpr       zScope                  = refinementLawEvaluator.getZScopeVarible();
		APLLValue   aplScope                = refinementLawEvaluator.getAPLScope();
		boolean     insideAPLScope          = aplScope != null ? true : false;
		
		ZExprSchema                 zExprSchema            = refinementLawEvaluator.getzScope();
		Map<String, ATCALType>      types                  = refinementLawEvaluator.getTypes();
		LValueFactory               lValueFactory          = refinementLawEvaluator.getlValueFactory();
		Map<String, ConstantMapper> zVarConstantMaps       = refinementLawEvaluator.getZVarConstantMaps();
		BiMap<ZVar, String>         zVarToImpVarName       = refinementLawEvaluator.getzVarToImpVarName();
		ElementsInfo                elementsInfo           = refinementLawEvaluator.getElementsInfo();
		Map<String, Integer>        scopeNameToPositionMap = new HashMap<>(refinementLawEvaluator.getScopeNameToPositionMap());
		OuterAPLScopeInfo           outerAPLScopeInfo      = refinementLawEvaluator.getOuterAPLScopeInfo();
		Map<String, ZExpr>          processedElements      = refinementLawEvaluator.getProcessedElements();
		RefinementLawEvaluator      newScopeEvaluator      = new RefinementLawEvaluator(zExprSchema, newAPLScope, types,
		    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);

		if (!insideAPLScope) {
			if (zScope instanceof ZExprConst) {
				if (!existsMapClause) {
					throw new MapClauseNeededException(newAPLScopeTypeCategory);
				}
				codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
			} else {
				throw new OnlyEnumVariablesCanBeRefinedAsEnumException();
			}
		} else {
			ATCALType aplScopeType         = aplScope.getType();
			String    aplScopeTypeCategory = aplScopeType.typeCategory();
			newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
			newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
			boolean isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
			// If newAPLScope is incorrectly used inside aplScope, then stop processing and throw exception
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			newScopeEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (!existsMapClause) {
						throw new MapClauseNeededException(newAPLScopeTypeCategory);
					}
					ArrayType       arrayType = (ArrayType) aplScopeType;
					ZExpr           element;
					Iterator<ZExpr> iterator  = getIterator(zScope, arrayType.getSize());
					ZExprSchema     newZScope;
					CodeBlock       constMappingCodeBlockStmt;
					// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator must be
					// created in order to process the map-clause
					while (iterator.hasNext()) {
						element                   = iterator.next();
						newZScope                 = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
						newScopeEvaluator         = new RefinementLawEvaluator(newZScope, newAPLScope, types, lValueFactory,
						    zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
						constMappingCodeBlockStmt = newScopeEvaluator.visit(ctx.constMapping());

						codeBlock.join(constMappingCodeBlockStmt);
					}
					return codeBlock;
				} else {
					throw new ExpressionMustBeSetOrListInInnerRefinementToEnum(zScope.toString());
				}
			} else if (aplScopeType instanceof ContractType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (isContractRefiningSetOrList) {
						if (!existsMapClause) {
							throw new MapClauseNeededException(newAPLScopeTypeCategory);
						}
						Iterator<ZExpr> iterator          = getIterator(zScope);
						ContractType    contractType      = (ContractType) aplScopeType;
						ATCALType       contractInnerType = contractType.getUniqueMemberType();
						ZExpr           element;
						ZExprSchema     newZScope;
						CodeBlock       constMappingCodeBlockStmt;
						String          member;
						String          memberName;
						APLExpr         aplExpr;
						if (contractInnerType instanceof ContractType) {
							// Using each of the zScope's elements as new Z scope, a new instance of
							// RefinementLawEvaluator must be created in order to process the map-clause
							while (iterator.hasNext()) {
								element                   = iterator.next();
								newZScope                 = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
								newScopeEvaluator         = new RefinementLawEvaluator(newZScope, newAPLScope, types,
								    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
								constMappingCodeBlockStmt = newScopeEvaluator.visit(ctx.constMapping());

								codeBlock.join(constMappingCodeBlockStmt);
							}
							return codeBlock;
						} else if (contractInnerType instanceof RecordType) {
							while (iterator.hasNext()) {
								element           = iterator.next();
								aplExpr           = newAPLScopeType.fromZExpr(element);
								outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
								member            = newAPLScope.getName();
								memberName        = VariableNamingHelper
								    .buildDefinitiveRecordMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractRecordMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							throw new OnlyContractOrRecordMembersAllowedException();
						}

						return codeBlock;
					} else {
						throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
						    aplScopeTypeCategory);
					}
				} else if (zScope instanceof ZExprConst) {
					if (!existsMapClause) {
						throw new MapClauseNeededException(newAPLScopeTypeCategory);
					}
					codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
				} else {
					throw new OnlyEnumVariablesCanBeRefinedAsEnumException();
				}
			} else if (aplScopeType instanceof RecordType) {
				if (zScope instanceof ZExprConst) {
					if (!existsMapClause) {
						throw new MapClauseNeededException(newAPLScopeTypeCategory);
					}
					codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
				} else {
					throw new OnlyEnumVariablesCanBeRefinedAsEnumException();
				}
			}
		}
		return codeBlock;
	}

	/**
	 * Refines {@code zScope} to a contract
	 * 
	 * @param  ctx
	 * @param  refinementLawEvaluator This refinement law evaluator would be used only to store record and contract
	 *                                member's information into maps and to get data to create new RefinementLawEvaluator
	 *                                instances
	 * @param  zScopeType             Type of Z context from which the {@code zScope} is generated
	 * @param  zScope                 Z expression to be refined
	 * @param  aplScope               Outer APL scope, if any
	 * @param  newAPLScope            Defines the variable that will hold the {@code zScope} value
	 * @param  insideAPLScope         True if the refinement occurs inside an APL scope
	 * @param  existsWithClause       True if the current refinement context have with-clause
	 * @return                        CodeBlock with the corresponding statements
	 */
	public static CodeBlock refineAsContract(RefinementContext ctx,
	                                         RefinementLawEvaluator refinementLawEvaluator,
	                                         APLLValue newAPLScope) {
		CodeBlock    codeBlock        = new CodeBlock();
		ContractType newAPLScopeType  = (ContractType) newAPLScope.getType();
		ContextType  zScopeType       = refinementLawEvaluator.getzScopeType();
		ZExpr        zScope           = refinementLawEvaluator.getZScopeVarible();
		APLLValue    aplScope         = refinementLawEvaluator.getAPLScope();
		boolean      insideAPLScope   = aplScope != null ? true : false;
		boolean      existsWithClause = ctx.withRef() != null ? true : false;
		boolean      existsMapClause  = ctx.constMapping() != null ? true : false;

		ZExprSchema                 zExprSchema            = refinementLawEvaluator.getzScope();
		Map<String, ATCALType>      types                  = refinementLawEvaluator.getTypes();
		LValueFactory               lValueFactory          = refinementLawEvaluator.getlValueFactory();
		Map<String, ConstantMapper> zVarConstantMaps       = refinementLawEvaluator.getZVarConstantMaps();
		BiMap<ZVar, String>         zVarToImpVarName       = refinementLawEvaluator.getzVarToImpVarName();
		ElementsInfo                elementsInfo           = refinementLawEvaluator.getElementsInfo();
		Map<String, Integer>        scopeNameToPositionMap = new HashMap<>(refinementLawEvaluator.getScopeNameToPositionMap());
		OuterAPLScopeInfo           outerAPLScopeInfo      = refinementLawEvaluator.getOuterAPLScopeInfo();
		Map<String, ZExpr>          processedElements      = refinementLawEvaluator.getProcessedElements();
		RefinementLawEvaluator      newScopeEvaluator      = new RefinementLawEvaluator(zExprSchema, newAPLScope, types,
		    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
		if (!insideAPLScope) {
			if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
				if (existsMapClause) {
					newScopeEvaluator.setContractRefiningSetOrList(true);
					codeBlock = newScopeEvaluator.visit(ctx.constMapping());
				} else {
					if (existsWithClause) {
						newScopeEvaluator.setContractRefiningSetOrList(true);
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						ATCALType contractUniqueMember = newAPLScopeType.getUniqueMemberType();
						if (isBasicType(contractUniqueMember)) {
							CodeBlock uniqueMemberContractStmts = StatementsHelper.generateUnaryContractStmts(zScope, newAPLScope,
							                                                                                  outerAPLScopeInfo);
							codeBlock.join(uniqueMemberContractStmts);
						}
					}
				}
			} else {
				if (existsWithClause) {
					if (zScope instanceof ZExprProd) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else if (zScope instanceof ZExprString) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else if (zScope instanceof ZExprConst) {
						throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(),
						    newAPLScopeType.typeCategory());
					} // It should reach to this else-branch when zScope is:
					  // - ZExprAuto
					  // - ZExprNum
					else {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					}
				} else {
					throw new WithClauseNeededException(newAPLScopeType.typeCategory());
				}
			}
			return codeBlock;
		} else {
			ATCALType aplScopeType         = aplScope.getType();
			String    aplScopeTypeCategory = aplScopeType.typeCategory();
			newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
			newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
			boolean isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
			// If newAPLScope is incorrectly used inside aplScope, then stop processing and throw exception
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			newScopeEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
						if (existsWithClause) {
							newScopeEvaluator.setContractRefiningSetOrList(true);
							codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
						} else {
							ATCALType contractUniqueMember = newAPLScopeType.getUniqueMemberType();
							if (isBasicType(contractUniqueMember)) {
								CodeBlock uniqueMemberContractStmts = StatementsHelper
								    .generateUnaryContractStmts(zScope, newAPLScope, outerAPLScopeInfo);
								codeBlock.join(uniqueMemberContractStmts);
								String    contractVariableName = VariableNamingHelper
								    .buildDefinitiveContractVariableNameWithScope(newAPLScope.getName(), outerAPLScopeInfo);
								APLVar    contractVar          = new APLVar(contractVariableName, newAPLScopeType);
								APLArray  aplArray             = (APLArray) aplScope;
								APLLValue arrayIndex           = aplArray.getNextIndex();
								codeBlock.addStmt(new AssignStmt(arrayIndex, contractVar));
							}
						}
					} else {
						if (existsWithClause) {
							ArrayType       arrayType           = (ArrayType) aplScopeType;
							Iterator<ZExpr> iterator            = getIterator(zScope, arrayType.getSize());
							ZExpr           element;
							ZExprSchema     newZScope;
							CodeBlock       withRefCodeBlockStmt;
							int             currentElementIndex = 0;
							// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator must
							// be created in order to process the with-clause
							while (iterator.hasNext()) {
								element           = iterator.next();
								newZScope         = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
								newScopeEvaluator = new RefinementLawEvaluator(newZScope, newAPLScope, types, lValueFactory,
								    zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
								scopeNameToPositionMap.put(zScope.getContextName(), currentElementIndex);
								newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
								newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
								withRefCodeBlockStmt = newScopeEvaluator.visit(ctx.withRef());
								codeBlock.join(withRefCodeBlockStmt);
								// Increment the counter in order to point to the next Z scope element
								// in the following iteration
								currentElementIndex++;
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							ATCALType contractUniqueMember = newAPLScopeType.getUniqueMemberType();
							if (isBasicType(contractUniqueMember)) {
								ArrayType       arrayType = (ArrayType) aplScopeType;
								Iterator<ZExpr> iterator  = getIterator(zScope, arrayType.getSize());
								while (iterator.hasNext()) {
									ZExpr     element                   = iterator.next();
									CodeBlock uniqueMemberContractStmts = StatementsHelper
									    .generateUnaryContractWithSiblingsStmts(element, newAPLScope, outerAPLScopeInfo);
									codeBlock.join(uniqueMemberContractStmts);
									outerAPLScopeInfo.incrementCounter();
								}
							} else {
								throw new WithClauseNeededException(newAPLScopeType.typeCategory());
							}
						}
					}
					return codeBlock;
				} else {
					if (existsWithClause) {
						if (zScope instanceof ZExprProd) {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						} else if (zScope instanceof ZExprString) {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						} else {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						}
					} else {
						throw new WithClauseNeededException(newAPLScopeType.typeCategory());
					}
				}
			} else {
				if (aplScopeType instanceof ContractType) {
					if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
						if (isContractRefiningSetOrList) {
							if (zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
								if (existsWithClause) {
									newScopeEvaluator.setContractRefiningSetOrList(true);
									codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
								} else {
									ATCALType contractUniqueMember = newAPLScopeType.getUniqueMemberType();
									if (isBasicType(contractUniqueMember)) {
										CodeBlock uniqueMemberContractStmts = StatementsHelper
										    .generateUnaryContractStmts(zScope, newAPLScope, outerAPLScopeInfo);
										codeBlock.join(uniqueMemberContractStmts);
									}
								}
							} else {
								Iterator<ZExpr> iterator            = getIterator(zScope);
								int             currentElementIndex = 0;
								ZExpr           element;
								ZExprSchema     newZScope;
								CodeBlock       withRefCodeBlockStmt;
								// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator
								// must be
								// created in order to process the with-clause
								while (iterator.hasNext()) {
									element           = iterator.next();
									newZScope         = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
									newScopeEvaluator = new RefinementLawEvaluator(newZScope, newAPLScope, types, lValueFactory,
									    zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
									scopeNameToPositionMap.put(zScope.getContextName(), currentElementIndex);
									newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
									newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
									withRefCodeBlockStmt = newScopeEvaluator.visit(ctx.withRef());
									codeBlock.join(withRefCodeBlockStmt);
									// Increment the counter in order to point to the next Z scope element
									// in the following iteration
									currentElementIndex++;
									outerAPLScopeInfo.incrementCounter();
								}
							}
						} else {
							if (existsWithClause) {
								newScopeEvaluator.setContractRefiningSetOrList(true);
								codeBlock = newScopeEvaluator.visit(ctx.withRef());
							} else if (existsMapClause) {
								newScopeEvaluator.setContractRefiningSetOrList(true);
								codeBlock = newScopeEvaluator.visit(ctx.constMapping());
							}
						}
					} else {
						if (existsWithClause) {
							if (zScope instanceof ZExprProd) {
								codeBlock = newScopeEvaluator.visit(ctx.withRef());
							} else if (zScope instanceof ZExprString) {
								codeBlock = newScopeEvaluator.visit(ctx.withRef());
							} else {
								codeBlock = newScopeEvaluator.visit(ctx.withRef());
							}
						} else {
							throw new WithClauseNeededException(newAPLScopeType.typeCategory());
						}
					}
				} else if (aplScopeType instanceof RecordType) {
					if (existsWithClause) {
						if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						} else if (zScope instanceof ZExprProd) {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						} else if (zScope instanceof ZExprString) {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						} else {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						}
					} else {
						throw new WithClauseNeededException(newAPLScopeType.typeCategory());
					}
				}
			}
		}
		return codeBlock;

	}

	public static CodeBlock refineAsRecord(RefinementContext ctx,
	                                       RefinementLawEvaluator refinementLawEvaluator,
	                                       APLLValue newAPLScope) {
		CodeBlock   codeBlock               = new CodeBlock();
		RecordType  newAPLScopeType         = (RecordType) newAPLScope.getType();
		String      newAPLScopeTypeCategory = newAPLScopeType.typeCategory();
		ContextType zScopeType              = refinementLawEvaluator.getzScopeType();
		ZExpr       zScope                  = refinementLawEvaluator.getZScopeVarible();
		APLLValue   aplScope                = refinementLawEvaluator.getAPLScope();
		boolean     insideAPLScope          = aplScope != null ? true : false;
		boolean     existsWithClause        = ctx.withRef() != null ? true : false;

		ZExprSchema                 zExprSchema            = refinementLawEvaluator.getzScope();
		Map<String, ATCALType>      types                  = refinementLawEvaluator.getTypes();
		LValueFactory               lValueFactory          = refinementLawEvaluator.getlValueFactory();
		Map<String, ConstantMapper> zVarConstantMaps       = refinementLawEvaluator.getZVarConstantMaps();
		BiMap<ZVar, String>         zVarToImpVarName       = refinementLawEvaluator.getzVarToImpVarName();
		ElementsInfo                elementsInfo           = refinementLawEvaluator.getElementsInfo();
		Map<String, Integer>        scopeNameToPositionMap = new HashMap<>(refinementLawEvaluator.getScopeNameToPositionMap());
		OuterAPLScopeInfo           outerAPLScopeInfo      = refinementLawEvaluator.getOuterAPLScopeInfo();
		Map<String, ZExpr>          processedElements      = refinementLawEvaluator.getProcessedElements();
		RefinementLawEvaluator      newScopeEvaluator      = new RefinementLawEvaluator(zExprSchema, newAPLScope, types,
		    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
		if (!insideAPLScope) {
			if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
				if (existsWithClause) {
					codeBlock = newScopeEvaluator.visit(ctx.withRef());
				} else {
					throw new WithClauseNeededException(newAPLScopeType.typeCategory());
				}
			} else if (zScope instanceof ZExprProd) {
				if (existsWithClause) {
					codeBlock = newScopeEvaluator.visit(ctx.withRef());
				} else {
					ZExprProd            product         = (ZExprProd) zScope;
					ImmutableList<ZExpr> productElements = product.getValues();

					// I consider that a product without elements is an error
					if (productElements.size() == 0) {
						throw new ProductCannotBeEmptyException(product.toString());
					}
					if (!newAPLScopeType.canImplementDirectly(product)) {
						throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
					}
					CodeBlock recordStmts = generateRecordStmts(product, newAPLScope, newAPLScopeType);
					codeBlock.join(recordStmts);
				}
			} else if (zScope instanceof ZExprString) {
				if (existsWithClause) {
					codeBlock = newScopeEvaluator.visit(ctx.withRef());
				} else {
					throw new WithClauseNeededException(newAPLScopeTypeCategory);
				}
			} else if (zScope instanceof ZExprConst) {
				throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
			} // It should reach to this else-branch when zScope is:
			  // - ZExprAuto
			  // - ZExprNum
			else {
				if (existsWithClause) {
					codeBlock = newScopeEvaluator.visit(ctx.withRef());
				} else {
					throw new WithClauseNeededException(newAPLScopeTypeCategory);
				}
			}
		} else {
			ATCALType aplScopeType         = aplScope.getType();
			String    aplScopeTypeCategory = aplScopeType.typeCategory();
			newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
			newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
			boolean isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
			// If newAPLScope is incorrectly used inside aplScope, then stop processing and throw exception
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			newScopeEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					ArrayType       arrayType = (ArrayType) aplScopeType;
					Iterator<ZExpr> iterator  = getIterator(zScope, arrayType.getSize());
					if (existsWithClause) {
						ZExpr       element;
						ZExprSchema newZScope;
						CodeBlock   withRefCodeBlockStmt;
						int         currentElementIndex = 0;
						// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator must be
						// created in order to process the with-clause
						while (iterator.hasNext()) {
							element           = iterator.next();
							newZScope         = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
							newScopeEvaluator = new RefinementLawEvaluator(newZScope, newAPLScope, types, lValueFactory,
							    zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
							scopeNameToPositionMap.put(zScope.getContextName(), currentElementIndex);
							newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
							newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
							withRefCodeBlockStmt = newScopeEvaluator.visit(ctx.withRef());
							codeBlock.join(withRefCodeBlockStmt);
							// Increment the counter in order to point to the next Z scope element
							// in the following iteration
							currentElementIndex++;
							outerAPLScopeInfo.incrementCounter();
						}
					} else {
						ZExpr   element;
						APLExpr aplExpr;
						while (iterator.hasNext()) {
							element = iterator.next();
							aplExpr = newAPLScopeType.fromZExpr(element);
							String recordMember     = newAPLScope.getName();
							String recordMemberName = VariableNamingHelper
							    .buildDefinitiveRecordMemberVariableName(recordMember, outerAPLScopeInfo);
							refinementLawEvaluator.getElementsInfo().addRecordMemberInfoToMaps(recordMember, recordMemberName,
							                                                                   aplExpr, newAPLScopeType);
							outerAPLScopeInfo.incrementCounter();
						}
					}
					return codeBlock;
				} else if (zScope instanceof ZExprProd) {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				} else if (zScope instanceof ZExprString) {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				} else {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				}
			} else if (aplScopeType instanceof ContractType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (existsWithClause) {
						if (isContractRefiningSetOrList) {
							Iterator<ZExpr> iterator            = getIterator(zScope);
							int             currentElementIndex = 0;
							ZExpr           element;
							ZExprSchema     newZScope;
							CodeBlock       withRefCodeBlockStmt;
							// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator must
							// be
							// created in order to process the with-clause
							while (iterator.hasNext()) {
								element           = iterator.next();
								newZScope         = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
								newScopeEvaluator = new RefinementLawEvaluator(newZScope, newAPLScope, types, lValueFactory,
								    zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
								scopeNameToPositionMap.put(zScope.getContextName(), currentElementIndex);
								newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
								newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
								withRefCodeBlockStmt = newScopeEvaluator.visit(ctx.withRef());
								codeBlock.join(withRefCodeBlockStmt);
								// Increment the counter in order to point to the next Z scope element
								// in the following iteration
								currentElementIndex++;
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							codeBlock = newScopeEvaluator.visit(ctx.withRef());
						}
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				} else if (zScope instanceof ZExprProd) {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						ZExprProd            product         = (ZExprProd) zScope;
						ImmutableList<ZExpr> productElements = product.getValues();

						// I consider that a product without elements is an error
						if (productElements.size() == 0) {
							throw new ProductCannotBeEmptyException(product.toString());
						}
						if (!newAPLScopeType.canImplementDirectly(product)) {
							throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(),
							    newAPLScopeTypeCategory);
						}
						CodeBlock recordStmts = generateRecordStmts(product, newAPLScope, newAPLScopeType);
						codeBlock.join(recordStmts);
					}
				} else if (zScope instanceof ZExprString) {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				} else {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				}
			} else if (aplScopeType instanceof RecordType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				} else if (zScope instanceof ZExprProd) {
					ZExprProd            product         = (ZExprProd) zScope;
					ImmutableList<ZExpr> productElements = product.getValues();

					// I consider that a product without elements is an error
					if (productElements.size() == 0) {
						throw new ProductCannotBeEmptyException(product.toString());
					}
					if (!newAPLScopeType.canImplementDirectly(product)) {
						throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
					}
					CodeBlock recordStmts = generateRecordStmts(product, newAPLScope, newAPLScopeType);
					codeBlock.join(recordStmts);
				} else if (zScope instanceof ZExprString) {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				} else {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				}
			}
		}
		return codeBlock;
	}

	/**
	 * Refines {@code zScope} to a array
	 * 
	 * @param  ctx
	 * @param  refinementLawEvaluator   This refinement law evaluator would be used only to store record and contract
	 *                                  member's information into maps and to get data to create new RefinementLawEvaluator
	 *                                  instances
	 * @param  zScopeType               Type of Z context from which the {@code zScope} is generated
	 * @param  zScope                   Z expression to be refined
	 * @param  aplScope                 Outer APL scope, if any
	 * @param  newAPLScope              Defines the variable that will hold the {@code zScope} value
	 * @param  insideAPLScope           True if the refinement occurs inside an APL scope
	 * @param  existsWithClause         True if the current refinement context have with-clause
	 * @param  existsMapClause          True if the current refinement context have map-clause
	 * @param  refiningArrayInsideArray True if the method is processing the refinement of an array that is inside other array.
	 *                                  Note that this parameter comes with {@code innerArrayNameSuffix}
	 * @param  innerArrayNameSuffix     Suffix used to differentiate inner array declare statement names. Note that this parameter
	 *                                  comes with {@code refiningArrayInsideArray}
	 * @return                          CodeBlock with the corresponding statements
	 */
	public static CodeBlock refineAsArray(RefinementContext ctx,
	                                      RefinementLawEvaluator refinementLawEvaluator,
	                                      APLLValue newAPLScope,
	                                      boolean insideAPLScope) {
		CodeBlock            codeBlock                   = new CodeBlock();
		ArrayType            newAPLScopeType             = (ArrayType) newAPLScope.getType();
		ATCALType            newAPLScopeInnerType        = newAPLScopeType.getType();
		String               newAPLScopeTypeCategory     = newAPLScopeType.typeCategory();
		Map<String, Integer> scopeNameToPositionMap      = new HashMap<>(refinementLawEvaluator.getScopeNameToPositionMap());
		OuterAPLScopeInfo    outerAPLScopeInfo           = refinementLawEvaluator.getOuterAPLScopeInfo();
		boolean              isRefiningArrayInsideArray  = refinementLawEvaluator.isRefiningArrayInsideArray();
		boolean              isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
		ContextType          zScopeType                  = refinementLawEvaluator.getzScopeType();
		ZExpr                zScope                      = refinementLawEvaluator.getZScopeVarible();
		APLLValue            aplScope                    = refinementLawEvaluator.getAPLScope();
		boolean              existsWithClause            = ctx.withRef() != null ? true : false;
		boolean              existsMapClause             = ctx.constMapping() != null ? true : false;
		String               modifiedNewAPLScopeName;
		APLArray             modifiedNewAPLScope;
		if (isRefiningArrayInsideArray || isContractRefiningSetOrList) {
			modifiedNewAPLScopeName = VariableNamingHelper
			    .buildDefinitiveArrayInsideArrayVariableNameWithScope(newAPLScope.getName(), outerAPLScopeInfo);
			modifiedNewAPLScope     = new APLArray(modifiedNewAPLScopeName, newAPLScopeType);
		} else {
			modifiedNewAPLScopeName = newAPLScope.getName();
			modifiedNewAPLScope     = (APLArray) newAPLScope;
		}

		ZExprSchema                 zExprSchema       = refinementLawEvaluator.getzScope();
		Map<String, ATCALType>      types             = refinementLawEvaluator.getTypes();
		LValueFactory               lValueFactory     = refinementLawEvaluator.getlValueFactory();
		Map<String, ConstantMapper> zVarConstantMaps  = refinementLawEvaluator.getZVarConstantMaps();
		BiMap<ZVar, String>         zVarToImpVarName  = refinementLawEvaluator.getzVarToImpVarName();
		ElementsInfo                elementsInfo      = refinementLawEvaluator.getElementsInfo();
		Map<String, ZExpr>          processedElements = refinementLawEvaluator.getProcessedElements();
		RefinementLawEvaluator      newScopeEvaluator = new RefinementLawEvaluator(zExprSchema, modifiedNewAPLScope, types,
		    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);

		if (!insideAPLScope) {
			if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
				CodeBlock arrayIndexStmts;
				if (existsWithClause) {
					if (isBasicType(newAPLScopeInnerType) && newAPLScopeInnerType.canImplementDirectly(zScope)
					    && !(containsElemExpression(ctx))) {
						arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);
						codeBlock.join(arrayIndexStmts);
					}
					codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
				} else {
					if (newAPLScopeInnerType instanceof EnumType) {
						if (!existsMapClause) {
							throw new MapClauseNeededException(newAPLScopeTypeCategory);
						}
						codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
					} else {
						ArrayDeclStmt arrayDeclStmt = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
						    newAPLScopeType.getSize());
						arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);
						codeBlock.addStmt(arrayDeclStmt);
						codeBlock.join(arrayIndexStmts);
					}

				}
			} else if (zScope instanceof ZExprProd) {
				if (existsWithClause) {
					codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
				} else {
					if (newAPLScopeInnerType instanceof EnumType) {
						if (!existsMapClause) {
							throw new MapClauseNeededException(newAPLScopeTypeCategory);
						}
						codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
					} else {
						ZExprProd            product         = (ZExprProd) zScope;
						ImmutableList<ZExpr> productElements = product.getValues();

						// I consider that a product without elements is an error
						if (productElements.size() == 0) {
							throw new ProductCannotBeEmptyException(product.toString());
						}
						if (!newAPLScopeInnerType.canImplementDirectly(product.getValue(0))) {
							throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(),
							    newAPLScopeTypeCategory);
						}

						ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
						    newAPLScopeType.getSize());
						CodeBlock     arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);

						codeBlock.addStmt(arrayDeclStmt);
						codeBlock.join(arrayIndexStmts);
					}
				}
			} else if (zScope instanceof ZExprString) {
				if (existsWithClause) {
					codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
				} else {
					if (newAPLScopeInnerType instanceof StringType) {
						ZExprString   stringExpr      = (ZExprString) zScope;
						String        string          = stringExpr.getStr();
						ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
						    newAPLScopeType.getSize());
						CodeBlock     arrayIndexStmts = generateArrayIndexStmts(string, modifiedNewAPLScope);
						codeBlock.addStmt(arrayDeclStmt);
						codeBlock.join(arrayIndexStmts);
					} else {
						throw new InnerArrayTypeMustBeStringException(modifiedNewAPLScopeName);
					}
				}
			} else if (zScope instanceof ZExprConst) {
				throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
			} // It should reach to this else-branch when zScope is:
			  // - ZExprAuto
			  // - ZExprNum
			else {
				if (existsWithClause) {
					codeBlock = newScopeEvaluator.visit(ctx.withRef());
				} else {
					throw new WithClauseNeededException(newAPLScopeTypeCategory);
				}
			}
		} else {
			ATCALType aplScopeType         = aplScope.getType();
			String    aplScopeTypeCategory = aplScopeType.typeCategory();
			newScopeEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
			newScopeEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
			// If newAPLScope is incorrectly used inside aplScope, then stop processing and throw exception
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			newScopeEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					ArrayType       arrayType = (ArrayType) aplScopeType;
					Iterator<ZExpr> iterator  = getIterator(zScope, arrayType.getSize());
					ZExpr           element;
					ZExprSchema     newZScope;
					if (existsWithClause) {
						if (zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
							codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
						} else {
							CodeBlock withRefCodeBlockStmt;
							// Using each of the zScope's elements as new Z scope, a new
							// instance of RefinementLawEvaluator must be
							// created in order to process the with-clause
							while (iterator.hasNext()) {
								element              = iterator.next();
								newZScope            = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
								newScopeEvaluator    = new RefinementLawEvaluator(newZScope, modifiedNewAPLScope, types,
								    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
								withRefCodeBlockStmt = newScopeEvaluator.visit(ctx.withRef());

								codeBlock.join(withRefCodeBlockStmt);
							}
						}

					} else {
						if (newAPLScopeInnerType instanceof EnumType) {
							if (!existsMapClause) {
								throw new MapClauseNeededException(newAPLScopeTypeCategory);
							}
							CodeBlock constMappingCodeBlockStmt;
							// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator must
							// be created in order to process the map-clause
							while (iterator.hasNext()) {
								element                   = iterator.next();
								newZScope                 = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
								newScopeEvaluator         = new RefinementLawEvaluator(newZScope, newAPLScope, types,
								    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
								constMappingCodeBlockStmt = newScopeEvaluator.visit(ctx.constMapping());

								codeBlock.join(constMappingCodeBlockStmt);
							}
						} else {
							if (zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
								ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
								    newAPLScopeType.getSize());
								CodeBlock     arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);
								codeBlock.addStmt(arrayDeclStmt);
								codeBlock.join(arrayIndexStmts);
							} else {
								RefinementLawEvaluator newRefinementLawEvaluator;
								CodeBlock              arrayCodeBlockStmts;
								int                    currentElementIndex = 0;

								// For each zScope element, create a new instance of RefinementLawEvaluator with the element as
								// new Z scope and then call recursively refineAsArray to refine the element.
								// --------------------------------------------------------------------------------------------------
								// The recursive call is considered to be done outside an APL scope because there's no WITH
								// clause and this way the refinement will progress. (if we call recursively with insideAPLScope
								// in true, then the refinement will return wrong results)
								while (iterator.hasNext()) {
									element                   = iterator.next();
									newZScope                 = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
									newRefinementLawEvaluator = new RefinementLawEvaluator(newZScope, newAPLScope, types,
									    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
									newRefinementLawEvaluator.setRefiningArrayInsideArray(true);
									newRefinementLawEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
									scopeNameToPositionMap.put(zScope.getContextName(), currentElementIndex);
									newRefinementLawEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
									arrayCodeBlockStmts = refineAsArray(ctx, newRefinementLawEvaluator, newAPLScope, false);

									codeBlock.join(arrayCodeBlockStmts);
									currentElementIndex++;
									outerAPLScopeInfo.incrementCounter();
								}
							}
						}
					}
					return codeBlock;
				} else if (zScope instanceof ZExprProd) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof EnumType) {
							if (!existsMapClause) {
								throw new MapClauseNeededException(newAPLScopeTypeCategory);
							}
							codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
						} else {
							CodeBlock arrayStmts = generateArrayStmtsFromProduct((ZExprProd) zScope, modifiedNewAPLScope);
							codeBlock.join(arrayStmts);
						}
					}
				} else if (zScope instanceof ZExprString) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof StringType) {
							CodeBlock arrayStmts = generateArrayStmtsFromString((ZExprString) zScope, modifiedNewAPLScope);
							codeBlock.join(arrayStmts);
						} else {
							throw new InnerArrayTypeMustBeStringException(modifiedNewAPLScopeName);
						}
					}
				} else {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				}
			} else if (aplScopeType instanceof ContractType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (isContractRefiningSetOrList) {
						ZExpr           element;
						ZExprSchema     newZScope;
						Iterator<ZExpr> iterator = getIterator(zScope);
						if (existsWithClause) {
							if (zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
								codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
							} else {
								CodeBlock withRefCodeBlockStmt;
								// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator
								// must
								// be created in order to process the with-clause
								while (iterator.hasNext()) {
									element              = iterator.next();
									newZScope            = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
									newScopeEvaluator    = new RefinementLawEvaluator(newZScope, modifiedNewAPLScope, types,
									    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
									withRefCodeBlockStmt = newScopeEvaluator.visit(ctx.withRef());

									codeBlock.join(withRefCodeBlockStmt);
								}
							}
						} else {
							if (newAPLScopeInnerType instanceof EnumType) {
								if (!existsMapClause) {
									throw new MapClauseNeededException(newAPLScopeTypeCategory);
								}
								CodeBlock constMappingCodeBlockStmt;
								// Using each of the zScope's elements as new Z scope, a new instance of RefinementLawEvaluator
								// must be created in order to process the map-clause
								while (iterator.hasNext()) {
									element                   = iterator.next();
									newZScope                 = ZExprSchema.add(zExprSchema, new ZVar(zScopeVarName, element));
									newScopeEvaluator         = new RefinementLawEvaluator(newZScope, newAPLScope, types,
									    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
									constMappingCodeBlockStmt = newScopeEvaluator.visit(ctx.constMapping());

									codeBlock.join(constMappingCodeBlockStmt);
								}
							} else {
								if (zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
									ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
									    newAPLScopeType.getSize());
									CodeBlock     arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);
									codeBlock.addStmt(arrayDeclStmt);
									codeBlock.join(arrayIndexStmts);
								} else {
									RefinementLawEvaluator newRefinementLawEvaluator;
									CodeBlock              arrayCodeBlockStmts;
									int                    currentElementIndex = 0;
									// For each zScope element, create a new instance of RefinementLawEvaluator with the element
									// as
									// new Z scope and then call recursively refineAsArray to refine the element.
									// --------------------------------------------------------------------------------------------------
									// The recursive call is considered to be done outside an APL scope because there's no WITH
									// clause and this way the refinement will progress. (if we call recursively with
									// insideAPLScope
									// in true, then the refinement will return wrong results)
									while (iterator.hasNext()) {
										element                   = iterator.next();
										newZScope                 = ZExprSchema.add(zExprSchema,
										                                            new ZVar(zScopeVarName, element));
										newRefinementLawEvaluator = new RefinementLawEvaluator(newZScope, newAPLScope, types,
										    lValueFactory, zVarConstantMaps, zVarToImpVarName, elementsInfo, processedElements);
										newRefinementLawEvaluator.setRefiningArrayInsideArray(true);
										newRefinementLawEvaluator.setScopeNameToPositionMap(scopeNameToPositionMap);
										scopeNameToPositionMap.put(zScope.getContextName(), currentElementIndex);
										newRefinementLawEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
										arrayCodeBlockStmts = refineAsArray(ctx, newRefinementLawEvaluator, newAPLScope, false);

										codeBlock.join(arrayCodeBlockStmts);
										currentElementIndex++;
										outerAPLScopeInfo.incrementCounter();
									}
								}
							}
						}
						return codeBlock;
					} else {
						if (existsWithClause) {
							codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
						} else {
							if (newAPLScopeInnerType instanceof EnumType) {
								if (!existsMapClause) {
									throw new MapClauseNeededException(newAPLScopeTypeCategory);
								}
								codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
							} else {
								ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
								    newAPLScopeType.getSize());
								CodeBlock     arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);

								codeBlock.addStmt(arrayDeclStmt);
								codeBlock.join(arrayIndexStmts);
							}
						}
					}
				} else if (zScope instanceof ZExprProd) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof EnumType) {
							if (!existsMapClause) {
								throw new MapClauseNeededException(newAPLScopeTypeCategory);
							}
							codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
						} else {
							CodeBlock arrayStmts = generateArrayStmtsFromProduct((ZExprProd) zScope, modifiedNewAPLScope);
							codeBlock.join(arrayStmts);
						}
					}
				} else if (zScope instanceof ZExprString) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof StringType) {
							CodeBlock arrayStmts = generateArrayStmtsFromString((ZExprString) zScope, newAPLScope);
							codeBlock.join(arrayStmts);
						} else {
							throw new InnerArrayTypeMustBeStringException(modifiedNewAPLScopeName);
						}
					}
				} else {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				}
			} else if (aplScopeType instanceof RecordType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof EnumType) {
							if (!existsMapClause) {
								throw new MapClauseNeededException(newAPLScopeTypeCategory);
							}
							codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
						} else {
							ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(newAPLScopeType, modifiedNewAPLScopeName,
							    newAPLScopeType.getSize());
							CodeBlock     arrayIndexStmts = generateArrayIndexStmts(zScope, modifiedNewAPLScope);

							codeBlock.addStmt(arrayDeclStmt);
							codeBlock.join(arrayIndexStmts);
						}
					}
				} else if (zScope instanceof ZExprProd) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof EnumType) {
							if (!existsMapClause) {
								throw new MapClauseNeededException(newAPLScopeTypeCategory);
							}
							codeBlock.join(newScopeEvaluator.visit(ctx.constMapping()));
						} else {
							CodeBlock arrayStmts = generateArrayStmtsFromProduct((ZExprProd) zScope, modifiedNewAPLScope);
							codeBlock.join(arrayStmts);
						}
					}
				} else if (zScope instanceof ZExprString) {
					if (existsWithClause) {
						codeBlock.join(newScopeEvaluator.visit(ctx.withRef()));
					} else {
						if (newAPLScopeInnerType instanceof StringType) {
							CodeBlock arrayStmts = generateArrayStmtsFromString((ZExprString) zScope, newAPLScope);
							codeBlock.join(arrayStmts);
						} else {
							throw new InnerArrayTypeMustBeStringException(modifiedNewAPLScopeName);
						}
					}
				} else {
					if (existsWithClause) {
						codeBlock = newScopeEvaluator.visit(ctx.withRef());
					} else {
						throw new WithClauseNeededException(newAPLScopeTypeCategory);
					}
				}
			}
		}
		return codeBlock;
	}

	public static boolean containsElemExpression(RefinementContext ctx) {
		boolean                    result         = false;
		List<LawRefinementContext> lawRefinements = ctx.withRef().lawRefinement();
		for (LawRefinementContext lawRefinementContext : lawRefinements) {
			ZExprContext zExpr = lawRefinementContext.zExprs().zExpr();
			if (zExpr instanceof ProdProjContext) {
				ProdProjContext prodProjCtx = (ProdProjContext) zExpr;
				if (prodProjCtx.zExpr() instanceof ElemExprContext) {
					result = true;
				}
			} else if (zExpr instanceof SetDomContext) {
				SetDomContext setDomCtx = (SetDomContext) zExpr;
				if (setDomCtx.zExpr() instanceof ElemExprContext) {
					result = true;
				}
			} else if (zExpr instanceof SetRanContext) {
				SetRanContext setRanCtx = (SetRanContext) zExpr;
				if (setRanCtx.zExpr() instanceof ElemExprContext) {
					result = true;
				}
			} else if (zExpr instanceof SetProjContext) {
				SetProjContext setProjCtx = (SetProjContext) zExpr;
				if (setProjCtx.zExpr() instanceof ElemExprContext) {
					result = true;
				}
			}
			if (result) {
				break;
			}
		}

		return result;
	}

	/**
	 * Refines {@code zScope} to a reference
	 * 
	 * @param  ctx
	 * @param  refinementLawEvaluator This refinement law evaluator would be used only to store record and contract
	 *                                member's information into maps
	 * @param  newAPLScope            The variable that will hold the {@code zScope} value
	 * @return                        CodeBlock with the corresponding statements
	 */
	public static CodeBlock refineAsReference(RefinementContext ctx,
	                                          RefinementLawEvaluator refinementLawEvaluator,
	                                          APLLValue newAPLScope) {
		CodeBlock   codeBlock               = new CodeBlock();
		ATCALType   newAPLScopeType         = newAPLScope.getType();
		APLExpr     aplExpr                 = null;
		String      newAPLScopeTypeCategory = newAPLScopeType.typeCategory();
		ContextType zScopeType              = refinementLawEvaluator.getzScopeType();
		ZExpr       zScope                  = refinementLawEvaluator.getZScopeVarible();
		APLLValue   aplScope                = refinementLawEvaluator.getAPLScope();
		boolean     insideAPLScope          = aplScope != null ? true : false;
		boolean     existsWithClause        = ctx.withRef() != null ? true : false;
		if (existsWithClause) {
			throw new WithClauseNotAllowedException(newAPLScopeTypeCategory);
		}
		if (!insideAPLScope) {
			if (zScope instanceof ZExprNum || zScope instanceof ZExprAuto) {
				aplExpr = newAPLScopeType.fromZExpr(zScope);
			} else {
				throw new SpecificationVariableCantBeRefinedToTypeException(zScope.toString(), newAPLScopeTypeCategory);
			}
			AssignStmt assignStmt = new AssignStmt(newAPLScope, aplExpr);
			codeBlock.addStmt(assignStmt);
		} else {
			OuterAPLScopeInfo outerAPLScopeInfo           = refinementLawEvaluator.getOuterAPLScopeInfo();
			ATCALType         aplScopeType                = aplScope.getType();
			String            aplScopeTypeCategory        = aplScopeType.typeCategory();
			boolean           isContractRefiningSetOrList = refinementLawEvaluator.isContractRefiningSetOrList();
			// If newAPLScope is incorrectly used inside aplScope, then stop processing and throw exception
			if (!isLValueCorrectInsideAPLScope(zScopeType, aplScope, newAPLScope, isContractRefiningSetOrList)) {
				throw new RefinementVariableWronglyUsedInScopeException(newAPLScope.toString(), aplScopeTypeCategory);
			}
			refinementLawEvaluator.addProcessedVariable(newAPLScope.getName());
			if (aplScopeType instanceof ArrayType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					ArrayType       arrayType      = (ArrayType) aplScope.getType();
					ATCALType       arrayInnerType = arrayType.getType();
					Iterator<ZExpr> iterator       = getIterator(zScope, arrayType.getSize());
					ZExpr           element;
					String          member         = newAPLScope.getName();
					String          memberName;
					if (arrayInnerType instanceof ContractType) {
						while (iterator.hasNext()) {
							element    = iterator.next();
							aplExpr    = newAPLScopeType.fromZExpr(element);
							memberName = VariableNamingHelper
							    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
							refinementLawEvaluator.getElementsInfo().addContractMemberInfoToMaps(member, memberName, aplExpr,
							                                                                     newAPLScopeType);
							outerAPLScopeInfo.incrementCounter();
						}
					} else if (arrayInnerType instanceof RecordType) {
						while (iterator.hasNext()) {
							element    = iterator.next();
							aplExpr    = newAPLScopeType.fromZExpr(element);
							memberName = VariableNamingHelper.buildDefinitiveRecordMemberVariableNameWithScope(member,
							                                                                                   outerAPLScopeInfo);
							refinementLawEvaluator.getElementsInfo().addRecordMemberInfoToMaps(member, memberName, aplExpr,
							                                                                   newAPLScopeType);
							outerAPLScopeInfo.incrementCounter();
						}
					} else {
						throw new OnlyContractOrRecordMembersAllowedException();
					}
					return codeBlock;
				} else if (zScope instanceof ZExprProd || zScope instanceof ZExprString) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else {
					aplExpr = newAPLScopeType.fromZExpr(zScope);
				}
				AssignStmt assignStmt = new AssignStmt(newAPLScope, aplExpr);
				codeBlock.addStmt(assignStmt);
			} else if (aplScopeType instanceof ContractType) {
				String member = newAPLScope.getName();
				String memberName;
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList) {
					if (isContractRefiningSetOrList) {
						Iterator<ZExpr> iterator          = getIterator(zScope);
						ContractType    contractType      = (ContractType) aplScopeType;
						ATCALType       contractInnerType = contractType.getUniqueMemberType();
						ZExpr           element;
						if (contractInnerType instanceof ContractType) {
							while (iterator.hasNext()) {
								element    = iterator.next();
								aplExpr    = newAPLScopeType.fromZExpr(element);
								memberName = VariableNamingHelper
								    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractContractMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else if (contractInnerType instanceof RecordType) {
							while (iterator.hasNext()) {
								element    = iterator.next();
								aplExpr    = newAPLScopeType.fromZExpr(element);
								memberName = VariableNamingHelper
								    .buildDefinitiveRecordMemberVariableNameWithScope(member, outerAPLScopeInfo);
								refinementLawEvaluator.getElementsInfo()
								    .addContractRecordMemberInfoToMaps(member, memberName, aplExpr, newAPLScopeType);
								outerAPLScopeInfo.incrementCounter();
							}
						} else {
							throw new OnlyContractOrRecordMembersAllowedException();
						}
						return codeBlock;
					} else {
						throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
						    aplScopeTypeCategory);
					}
				} else if (zScope instanceof ZExprProd || zScope instanceof ZExprString) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else {
					aplExpr           = newAPLScopeType.fromZExpr(zScope);
					outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
					member            = newAPLScope.getName();
					memberName        = VariableNamingHelper
					    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
					APLVar     contractMemberVar = new APLVar(memberName, newAPLScopeType);
					AssignStmt assignStmt        = new AssignStmt(contractMemberVar, aplExpr);
					codeBlock.addStmt(assignStmt);
				}
			} else if (aplScopeType instanceof RecordType) {
				if (zScope instanceof ZExprSet || zScope instanceof ZExprList || zScope instanceof ZExprProd
				    || zScope instanceof ZExprString) {
					throw new RefinementNotAllowedInScopeException(zScope.toString(), newAPLScopeTypeCategory,
					    aplScopeTypeCategory);
				} else {
					aplExpr           = newAPLScopeType.fromZExpr(zScope);
					outerAPLScopeInfo = refinementLawEvaluator.getOuterAPLScopeInfo();
					String     recordMember        = newAPLScope.getName();
					String     recordMemberVarName = VariableNamingHelper
					    .buildDefinitiveRecordMemberVariableNameWithScope(recordMember, outerAPLScopeInfo);
					APLVar     recordMemberVar     = new APLVar(recordMemberVarName, newAPLScopeType);
					AssignStmt assignStmt          = new AssignStmt(recordMemberVar, aplExpr);
					codeBlock.addStmt(assignStmt);
				}
			}
		}
		return codeBlock;
	}

	private static CodeBlock generateArrayStmtsFromString(ZExprString stringExpr, APLLValue aplArray) {
		String    string     = stringExpr.getStr();
		ArrayType arrayType  = (ArrayType) aplArray.getType();
		CodeBlock arrayStmts = new CodeBlock();

		ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(arrayType, aplArray.getName(), arrayType.getSize());
		CodeBlock     arrayIndexStmts = generateArrayIndexStmts(string, aplArray);

		arrayStmts.addStmt(arrayDeclStmt);
		arrayStmts.join(arrayIndexStmts);
		return arrayStmts;
	}

	/**
	 * Generates the corresponding statements associated to the translation of {@code product} to an the array {@code aplArray}
	 * 
	 * @param  product  Z product to be translated
	 * @param  aplArray Implementation variable meant to hold {@code product}'s values
	 * @return          The corresponding statements associated to the translation of {@code product} to an the array
	 *                  {@code aplArray}
	 */
	private static CodeBlock generateArrayStmtsFromProduct(ZExprProd product, APLLValue aplArray) {
		ImmutableList<ZExpr> productElements = product.getValues();
		ArrayType            arrayType       = (ArrayType) aplArray.getType();
		ATCALType            arrayInnerType  = arrayType.getType();
		CodeBlock            arrayStmts      = new CodeBlock();

		// I consider that a product without elements is an error
		if (productElements.size() == 0) {
			throw new ProductCannotBeEmptyException(product.toString());
		}
		if (!arrayInnerType.canImplementDirectly(product.getValue(0))) {
			throw new SpecificationVariableCantBeRefinedToTypeException(product.toString(), arrayType.toString());
		}

		ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(arrayType, aplArray.getName(), arrayType.getSize());
		CodeBlock     arrayIndexStmts = generateArrayIndexStmts(product, aplArray);

		arrayStmts.addStmt(arrayDeclStmt);
		arrayStmts.join(arrayIndexStmts);
		return arrayStmts;
	}

	/**
	 * Generates the statements needed to hold the values of {@code zScope} in array indexes
	 * 
	 * @param  zScope    Z expression that contains the values to be translated. Must be of type ZExprSet, ZExprList or ZExprProd
	 * @param  aplLValue Variable which will implement {@code zScope}
	 * @return           A code block with the APLStmts needed to hold the values of {@code zScope} in array indexes
	 */
	private static CodeBlock generateArrayIndexStmts(ZExpr zScope, APLLValue aplLValue) {
		CodeBlock       arrayIndexStmts = new CodeBlock();
		ArrayType       arrayType       = (ArrayType) aplLValue.getType();
		Iterator<ZExpr> iterator        = getIterator(zScope, arrayType.getSize());
		ATCALType       arrayInnerType  = arrayType.getType();
		APLArray        aplArray        = new APLArray(aplLValue.getName(), arrayType);
		ZExpr           element;
		APLLValue       arrayIndex;
		APLExpr         aplExpr;
		boolean         isUnaryContract = false;

		if (arrayInnerType instanceof ContractType && ((ContractType) arrayInnerType).hasUniqueMember()) {
			isUnaryContract = true;
		}

		while (iterator.hasNext()) {
			element    = iterator.next();
			arrayIndex = aplArray.getNextIndex();
			if (isUnaryContract && isSetOrList(element)) {
				String variableName = VariableNamingHelper.buildCompoundVariableName(aplLValue.getName(), "contract");
				aplExpr = new APLVar(variableName, arrayInnerType);
				CodeBlock contractStmts = StatementsHelper.generateContractStmtsFromCollection(element, (APLVar) aplExpr);
				arrayIndexStmts.join(contractStmts);
			} else {
				aplExpr = arrayInnerType.fromZExpr(element);
			}
			arrayIndexStmts.addStmt(new AssignStmt(arrayIndex, aplExpr));
		}
		return arrayIndexStmts;
	}

	/**
	 * This is a simplified version of {@link RefinementHelper#generateArrayIndexStmts(ZExpr, APLLValue)}
	 * Generates the statements needed to hold each character from {@code string} in an array index
	 * 
	 * @param  string
	 * @param  aplLValue
	 * @return           A code block with the APLStmts needed to hold each character from {@code string} in an array index
	 */
	private static CodeBlock generateArrayIndexStmts(String string, APLLValue aplLValue) {
		CodeBlock           arrayIndexStmts    = new CodeBlock();
		Iterator<Character> iterator           = Lists.charactersOf(string).iterator();
		ArrayType           arrayType          = (ArrayType) aplLValue.getType();
		Iterator<Character> restrictedIterator = restrictIterator(iterator, arrayType.getSize());
		APLArray            aplArray           = new APLArray(aplLValue.getName(), arrayType);
		APLExpr             aplExpr;
		while (restrictedIterator.hasNext()) {
			String    character  = restrictedIterator.next().toString();
			APLLValue arrayIndex = aplArray.getNextIndex();
			aplExpr = new StringExpr(character);
			arrayIndexStmts.addStmt(new AssignStmt(arrayIndex, aplExpr));
		}
		return arrayIndexStmts;
	}

	private static boolean isBasicType(ATCALType type) {
		boolean result = false;
		if (type instanceof IntType || type instanceof FloatType || type instanceof StringType || type instanceof ReferenceType) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	/**
	 * Generates the statements needed to hold the values of {@code product} in a record of type {@code recordType}
	 * 
	 * @param  product         Z product
	 * @param  recordImpLValue Variable which will implement {@code product}
	 * @param  recordType      Type of {@code recordImpLValue}
	 * @return                 A code block with the APLStmts needed to hold the values of {@code product} in a record of type
	 *                         {@code recordType}
	 */
	private static CodeBlock generateRecordStmts(ZExprProd product, APLLValue recordImpLValue, RecordType recordType) {
		CodeBlock recordStmts = new CodeBlock();

		List<String>         recordMembers  = recordType.getMembers();
		ImmutableList<ZExpr> productMembers = product.getValues();
		APLVar               recordVar      = new APLVar(recordImpLValue.getName(), recordType);
		String               recordField;
		APLExpr              value;
		ATCALType            recordFieldType;
		APLVar               fieldVar;
		for (int i = 0; i < recordMembers.size(); i++) {
			recordField     = recordMembers.get(i);
			recordFieldType = recordType.getFieldType(recordField).get();
			value           = recordFieldType.fromZExpr(productMembers.get(i));
			fieldVar        = new APLVar(recordField, recordFieldType);
			recordStmts.addStmt(new AssignStmt(fieldVar, value));
		}
		recordStmts.addStmt(new RecordStmt(recordVar, recordMembers));
		return recordStmts;
	}

	/**
	 * Generates the statements needed to hold the enum values of {@code zScope} in array indexes
	 * 
	 * @param  evaluator {@link RefinementLawEvaluator} needed to get the map that holds the constants mappings
	 * @param  zScope    Z expression that contains the values to be translated. Must be of type ZExprSet, ZExprList or ZExprProd
	 * @param  aplScope  Variable which will implement {@code zScope}
	 * @param  enumType  Enum type needed to get implementation enum value
	 * @param  constMaps Context that holds mapping info between z enum and implementation enum values
	 * @return           A code block with the APLStmts needed to hold the values of {@code zScope} in array indexes
	 */
	public static CodeBlock generateEnumArrayIndexStmts(RefinementLawEvaluator evaluator,
	                                                    ZExpr zScope,
	                                                    APLLValue aplScope,
	                                                    EnumType enumType,
	                                                    List<ConstMapContext> constMaps) {
		CodeBlock       arrayIndexStmts = new CodeBlock();
		ArrayType       arrayType       = (ArrayType) aplScope.getType();
		Iterator<ZExpr> iterator        = getIterator(zScope, arrayType.getSize());
		// ATCALType arrayInnerType = arrayType.getType();
		APLArray aplArray = new APLArray(aplScope.getName(), arrayType);

		// Fill the constants map
		Map<ZExprConst, ConsExpr> customMap = Maps.newHashMap();
		for (ConstMapContext constMap : constMaps) {
			customMap.put(ZExprConst.basic(constMap.ID(0).getText(), "set or list constant"),
			              enumType.getElemByName(constMap.ID(1).getText()));
		}

		APLLValue  arrayIndex;
		APLExpr    aplExpr;
		ZExpr      element;
		ZExprConst zConstMember;
		while (iterator.hasNext()) {
			element = iterator.next();
			if (element instanceof ZExprConst) {
				zConstMember = (ZExprConst) element;
				arrayIndex   = aplArray.getNextIndex();
				aplExpr      = evaluator.getZVarConstantMaps().getOrDefault(zConstMember.getZVarName(), new ConstantMapper())
				    .toEnum(zConstMember, customMap);
				arrayIndexStmts.addStmt(new AssignStmt(arrayIndex, aplExpr));
			} else {
				throw new ZExprMustBeConstantException(element.toString());
			}
		}

		return arrayIndexStmts;
	}

	/**
	 * Generates the statements needed to hold the enum values of {@code zScope} in contract
	 * 
	 * @param  evaluator {@link RefinementLawEvaluator} needed to get the map that holds the constants mappings
	 * @param  zScope    Z expression that contains the values to be translated. Must be of type ZExprSet, ZExprList or ZExprProd
	 * @param  aplScope  Variable which will implement {@code zScope}
	 * @param  enumType  Enum type needed to get implementation enum value
	 * @param  constMaps Context that holds mapping info between z enum and implementation enum values
	 * @return           A {@link CodeBlock} with the {@link APLStmt} needed to hold the values of {@code zScope} in contract
	 */
	public static CodeBlock generateEnumContractStmts(RefinementLawEvaluator evaluator,
	                                                  ZExpr zScope,
	                                                  APLLValue aplScope,
	                                                  EnumType enumType,
	                                                  List<ConstMapContext> constMaps) {
		CodeBlock       contractStmts   = new CodeBlock();
		Iterator<ZExpr> iterator        = getIterator(zScope);
		ContractType    contractType    = (ContractType) aplScope.getType();
		APLVar          contractVar     = new APLVar(aplScope.getName(), contractType);
		List<String>    constructorArgs = RefinementHelper.setToList(contractType.getConstArgs().keySet());
		List<String>    setterArgs      = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
		// Create an OuterAPLScopeInfo in order to build variable names
		OuterAPLScopeInfo outerAPLScopeInfo = new OuterAPLScopeInfo(aplScope, null, 0, 1, 0, true);

		contractStmts.addStmt(new ConstructorCallStmt(contractVar, constructorArgs));

		// Fill the constants map
		Map<ZExprConst, ConsExpr> customMap = Maps.newHashMap();
		for (ConstMapContext constMap : constMaps) {
			customMap.put(ZExprConst.basic(constMap.ID(0).getText(), "set or list constant"),
			              enumType.getElemByName(constMap.ID(1).getText()));
		}

		APLExpr      aplExpr;
		ZExpr        element;
		ZExprConst   zConstMember;
		String       contractMemberName;
		String       contractMemberVariableName;
		ATCALType    contractMemberType;
		APLVar       contractMemberVar;
		List<String> indexedSetterArguments;
		while (iterator.hasNext()) {
			element = iterator.next();
			if (element instanceof ZExprConst) {
				zConstMember = (ZExprConst) element;
				aplExpr      = evaluator.getZVarConstantMaps().getOrDefault(zConstMember.getZVarName(), new ConstantMapper())
				    .toEnum(zConstMember, customMap);
				contractMemberName         = contractType.getUniqueMemberName();
				contractMemberType         = contractType.getUniqueMemberType();
				contractMemberVariableName = VariableNamingHelper
				    .buildDefinitiveContractMemberVariableNameWithScope(contractMemberName, outerAPLScopeInfo);
				contractMemberVar          = new APLVar(contractMemberVariableName, contractMemberType);
				indexedSetterArguments     = RefinementHelper.generateIndexedMemberNames(setterArgs, outerAPLScopeInfo);
				contractStmts.addStmt(new AssignStmt(contractMemberVar, aplExpr));
				contractStmts.addStmt(new SetterCallStmt(contractVar, indexedSetterArguments));
			} else {
				throw new ZExprMustBeConstantException(element.toString());
			}
		}

		return contractStmts;
	}

	/**
	 * Generates a members list with the scope name as prefix (if possible) and adds a suffix (if needed)
	 * 
	 * @param  memberNames       List of member's names
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   A members list with the scope name as prefix (if possible) and adds a suffix (if needed)
	 */
	public static List<String> generateIndexedMemberNames(List<String> memberNames, OuterAPLScopeInfo outerAPLScopeInfo) {
		List<String> indexedMemberNames = new ArrayList<>();
		for (String setterArgument : memberNames) {
			String setterArgumentName;
			if (outerAPLScopeInfo != null) {
				setterArgumentName = VariableNamingHelper.buildDefinitiveContractMemberVariableNameWithScope(setterArgument,
				                                                                                             outerAPLScopeInfo);
			} else {
				setterArgumentName = VariableNamingHelper.buildDefinitiveContractSetterArgumentName(setterArgument,
				                                                                                    outerAPLScopeInfo);
			}
			indexedMemberNames.add(setterArgumentName);
		}
		return indexedMemberNames;
	}

	/**
	 * Generates a members list with the scope name as prefix and adds a suffix (if needed)
	 * 
	 * @param  memberNames       List of member's names
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   A members list with the scope name as prefix and adds a suffix (if needed)
	 */
	public static List<String> generateIndexedMemberNamesWithScope(List<String> memberNames,
	                                                               OuterAPLScopeInfo outerAPLScopeInfo) {
		List<String> indexedMemberNames = new ArrayList<>();
		for (String setterArgument : memberNames) {
			String setterArgumentName = VariableNamingHelper
			    .buildDefinitiveContractMemberVariableNameWithScope(setterArgument, outerAPLScopeInfo);
			indexedMemberNames.add(setterArgumentName);
		}
		return indexedMemberNames;
	}

	/**
	 * Generates a members list adding a suffix (if needed)
	 * 
	 * @param  memberNames       List of member's names
	 * @param  outerAPLScopeInfo APL scope currently processed. Used to define if the suffix is needed
	 * @return                   A members list adding a suffix (if needed)
	 */
	public static List<String> generateIndexedMemberNamesWithoutScope(List<String> memberNames,
	                                                                  OuterAPLScopeInfo outerAPLScopeInfo) {
		List<String> indexedMemberNames = new ArrayList<>();
		String       setterArgumentName;
		for (String setterArgument : memberNames) {
			setterArgumentName = VariableNamingHelper.buildDefinitiveContractSetterArgumentName(setterArgument,
			                                                                                    outerAPLScopeInfo);

			indexedMemberNames.add(setterArgumentName);
		}
		return indexedMemberNames;
	}

	/**
	 * Translates a set into a list
	 * 
	 * @param  set
	 * @return     List with set elements
	 */
	public static List<String> setToList(Set<String> set) {
		List<String> list = new ArrayList<>();
		for (String element : set) {
			list.add(element);
		}
		return list;
	}

	public static boolean haveZExprsSetCardContext(ZExprsContext ctx) {
		boolean result = false;
		if (ctx.zExpr() instanceof SetCardContext) {
			result = true;
		} else {
			if (ctx.zExprs() != null) {
				result = haveZExprsSetCardContext(ctx.zExprs());
			}
		}

		return result;
	}

	/**
	 * Verify if {@code newAPLScope} lvalue is correctly used inside APL scope {@code aplScope}
	 * 
	 * @param  zScopeType  Type of the current Z scope
	 * @param  aplScope    Outer APL scope
	 * @param  newAPLScope Inner APL scope
	 * @return             true if {@code newAPLScope} lvalue is correctly used inside APL scope {@code aplScope}
	 */
	public static boolean isLValueCorrectInsideAPLScope(ContextType zScopeType,
	                                                    APLLValue aplScope,
	                                                    APLLValue newAPLScope,
	                                                    boolean isContractRefiningSetOrList) {
		ATCALType aplScopeType = aplScope.getType();
		String    newAPLName   = newAPLScope.getName();
		boolean   result       = false;
		if (aplScopeType instanceof ArrayType) {
			ArrayType arrayType         = (ArrayType) aplScopeType;
			ATCALType aplScopeInnerType = arrayType.getType();
			ATCALType newAPLScopeType   = newAPLScope.getType();
			if (newAPLScope instanceof APLArray) {
				// ArrayType newAPLScopeArrayType = (ArrayType) newAPLScopeType;
				// if (aplScopeInnerType.equals(newAPLScopeArrayType) && zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
				// result = true;
				// } else {
				// result = false;
				// }
				result = true;
			} else if (newAPLScope instanceof APLArrayIndex) {
				APLArrayIndex arrayIndex = (APLArrayIndex) newAPLScope;
				result = aplScope.getName().equals(arrayIndex.getParent().getName());
			} else { // APLVar
				if (zScopeType.equals(ContextType.SET_CARDINALITY)) {
					result = true;
				} else {
					APLVar aplVar = (APLVar) newAPLScope;
					result = isVarCorrectInType(zScopeType, aplVar, aplScopeInnerType);
				}
			}
		} else if (aplScopeType instanceof ContractType) {
			ContractType contractType = (ContractType) aplScopeType;
			if (isContractRefiningSetOrList) {
				if (zScopeType.equals(ContextType.SET_CARDINALITY)) {
					result = true;
				} else {
					// In this case, the contract should have only one member
					result = contractType.getUniqueMemberName().equals(newAPLName);
				}

			} else {
				Map<String, ATCALType> contractMembers = contractType.getSetterArgs();
				result = contractMembers.containsKey(newAPLName);
			}
		} else if (aplScopeType instanceof RecordType) {
			RecordType recordType = (RecordType) aplScopeType;
			result = recordType.getMembers().contains(newAPLName);
		} else {
			throw new RuntimeException("isLValueCorrectInsideAPLScope should be called with array, contract or record");
		}
		return result;
	}

	/**
	 * Verifies if it's correct to use {@code aplVar} inside an array APL scope with {@code arrayInnerType} as the array inner
	 * type
	 * 
	 * @param  zScopeType     Type of the current Z scope
	 * @param  aplVar         {@link APLVar} used inside an APL scope with type {@code arrayInnerType}
	 * @param  arrayInnerType Outer APL scope type
	 * @return                true if it's correct to use {@code aplVar} inside an array APL scope with {@code arrayInnerType}
	 *                        as the array inner type
	 */
	public static boolean isVarCorrectInType(ContextType zScopeType, APLVar aplVar, ATCALType arrayInnerType) {
		// Get rid off the APL scope name and the dot
		String    varName = aplVar.getName();
		ATCALType varType = aplVar.getType();
		boolean   result  = false;
		if (arrayInnerType instanceof ContractType) {
			ContractType contractType = (ContractType) arrayInnerType;
			if (contractType.hasUniqueMember()) {
				result = varType.equals(contractType);
			} else {
				Map<String, ATCALType> contractMembers = contractType.getSetterArgs();
				ATCALType              memberType      = contractMembers.get(varName);
				result = memberType != null ? memberType.equals(varType) : false;
			}
		} else if (arrayInnerType instanceof RecordType) {
			RecordType          recordType = (RecordType) arrayInnerType;
			Optional<ATCALType> optType    = recordType.getFieldType(varName);
			result = !optType.isEmpty() ? optType.get().equals(varType) : false;
			// } else if (arrayInnerType instanceof IntType) {
			// result = zScopeType.equals(ContextType.SET_CARDINALITY) ? true : false;
		} // If inner type is ArrayType, then allow every name
		else if (arrayInnerType instanceof ArrayType) {
			result = true;
		} else {
			result = varType.equals(arrayInnerType);
		}
		// If arrayInnerType is any of the others, then an aplVar with arrayInnerType can't be used in an APL scope with
		// type arrayInnerType

		return result;
	}

	/**
	 * Returns the iterator of the zExpr if it's a ZExprSet, ZExprList or ZExprProd
	 * Otherwise it throws an exception. So, this method should only be called with those types as argument
	 *
	 * @param  zExpr
	 * @return       The zExpr iterator
	 */
	public static Iterator<ZExpr> getIterator(ZExpr zExpr) {
		Iterator<ZExpr> iterator;
		if (zExpr instanceof ZExprSet) {
			iterator = ((ZExprSet) zExpr).iterator();
		} else if (zExpr instanceof ZExprList) {
			iterator = ((ZExprList) zExpr).iterator();
		} else if (zExpr instanceof ZExprProd) {
			iterator = ((ZExprProd) zExpr).getValues().iterator();
		} else {
			throw new RuntimeException("getIterator should only be called with ZExprSet or ZExprList");
		}

		return iterator;
	}

	/**
	 * Returns the iterator of the zExpr if it's a {@link ZExprSet}, {@link ZExprList} or {@link ZExprProd},
	 * "truncating" it to {@code arraySize} number of elements
	 * Otherwise it throws an exception. So, this method should only be called with those types as argument
	 * 
	 * @param  zExpr
	 * @return       The zExpr iterator
	 */
	public static Iterator<ZExpr> getIterator(ZExpr zExpr, int arraySize) {
		if (zExpr instanceof ZExprProd) {
			ImmutableList<ZExpr> recordMembers      = ((ZExprProd) zExpr).getValues();
			int                  recordMembersCount = recordMembers.size();
			if (recordMembersCount > arraySize) {
				throw new ArrayIsntLongEnoughToRefineRecord();
			}
		}
		Iterator<ZExpr> iterator           = getIterator(zExpr);
		Iterator<ZExpr> restrictedIterator = restrictIterator(iterator, arraySize);
		return restrictedIterator;
	}

	/**
	 * Restricts the {@code iterator} in order to have a {@code limit} number of elements
	 * 
	 * @param  <E>      Iterator elements' type
	 * @param  iterator
	 * @param  limit
	 * @return          Limited iterator
	 */
	private static <E> Iterator<E> restrictIterator(Iterator<E> iterator, int limit) {
		List<E> list    = new ArrayList<>();
		int     counter = 0;
		while (iterator.hasNext() && counter < limit) {
			E e = iterator.next();
			list.add(e);
			counter++;
		}
		return list.iterator();
	}

	public static void checkElemOperatorAppearsOnlyOnce(WithRefContext ctx) {
		int           counter = 0;
		ZExprsContext zExprs;
		for (LawRefinementContext lawRefinementCtx : ctx.lawRefinement()) {
			zExprs = lawRefinementCtx.zExprs();
			if (isElemOperator(zExprs)) {
				counter++;
			}
			if (counter > 1) {
				throw new ElemOperatorCantBeUsedMultipleTimesInsideWithClauseException();
			}
		}

	}

	public static boolean isSpecifyingElemOperator(WithRefContext ctx) {
		return isElemOperator(ctx.lawRefinement(0).zExprs());
	}

	public static boolean isElemOperator(ZExprsContext ctx) {
		boolean result = false;
		if (ctx.zExpr() instanceof ElemExprContext && ctx.zExprs() == null) {
			result = true;
		}
		return result;
	}

	public static String getLValueName(WithRefContext ctx) {
		return ctx.lawRefinement(0).refinement(0).lvalue().getText();
	}

	/**
	 * ##############################################################################
	 * ###################### ContextType enumerated and methods ####################
	 * ##############################################################################
	 */

	/**
	 * Enumerated type used to identify the Z scope that it's currently being refined
	 * 
	 * @author Javier Bonet on 30/10/2019
	 * 
	 */
	public enum ContextType {
		/**
		 * When refining a SetCardContext
		 */
		SET_CARDINALITY,
		/**
		 * When refining an ElemContext
		 */
		POWER_SET_ELEMENT,
		/**
		 * When the current refined law insn't a special case
		 */
		NON_SPECIAL_CASE
	}

	/**
	 * Returns the {@link ContextType} of the ZExprContext passed as argument
	 * 
	 * @param  ctx ZExprContext to analyze
	 * @return     The {@link ContextType} of the ZExprContext passed as argument
	 */
	public static ContextType zExprToContextType(ZExprContext ctx) {
		ContextType contextType = ContextType.NON_SPECIAL_CASE;
		if (ctx instanceof SetCardContext) {
			contextType = ContextType.SET_CARDINALITY;
		} else if (ctx instanceof ElemExprContext) {
			contextType = ContextType.POWER_SET_ELEMENT;
		}
		return contextType;
	}

	/**
	 * Returns the {@link ContextType} of the ZExprsContext passed as argument
	 * 
	 * @param  ctx ZExprsContext to analyze
	 * @return     The {@link ContextType} of the ZExprsContext passed as argument
	 */
	public static ContextType zExprsToContextType(ZExprsContext ctx) {
		ContextType contextType = ContextType.NON_SPECIAL_CASE;
		contextType = zExprToContextType(ctx.zExpr());
		switch (contextType) {
		case SET_CARDINALITY:
			break;
		case POWER_SET_ELEMENT:
			break;
		default:
			if (ctx.zExprs() != null) {
				contextType = zExprsToContextType(ctx.zExprs());
			}
			break;
		}

		return contextType;
	}

	/**
	 * Generates a new {@link APLLValue} if {@code aplScope} is an array index
	 * 
	 * @param  aplScope
	 * @return          A new {@link APLLValue} if {@code aplScope} is an array index
	 */
	public static APLLValue generateNewAPLScopeIfNeeded(APLLValue aplScope) {
		APLLValue modifiedAPLScope;
		if (aplScope instanceof APLArrayIndex) {
			APLArrayIndex arrayIndex                          = (APLArrayIndex) aplScope;
			String        arrayIndexNameWithoutSquareBrackets = removeSquareBrackets(arrayIndex.getName());
			ATCALType     arrayIndexType                      = arrayIndex.getType();
			if (arrayIndexType instanceof ArrayType) {
				modifiedAPLScope = new APLArray(arrayIndexNameWithoutSquareBrackets, (ArrayType) arrayIndexType);
			} else {
				modifiedAPLScope = new APLVar(arrayIndexNameWithoutSquareBrackets, arrayIndexType);
			}
		} else {
			modifiedAPLScope = aplScope;
		}
		return modifiedAPLScope;
	}

	public static String removeSquareBrackets(String name) {
		return name.replaceAll("\\[", "").replaceAll("\\]", "");
	}

	private static boolean isSetOrList(ZExpr expr) {
		return expr instanceof ZExprSet || expr instanceof ZExprList;
	}
}