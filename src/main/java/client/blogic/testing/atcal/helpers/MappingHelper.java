package client.blogic.testing.atcal.helpers;

import java.util.Map;
import java.util.Set;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.ArrayType;
import client.blogic.testing.atcal.ContractType;
import client.blogic.testing.atcal.OuterAPLScopeInfo;
import client.blogic.testing.atcal.RecordType;
import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.mappingInfo.ElementsInfo;

public class MappingHelper {
	public static void completeRecordInfoMap(OuterAPLScopeInfo outerAPLScopeInfo,
	                                         ElementsInfo elementsInfo,
	                                         RecordType recordType) {
		int    counter = outerAPLScopeInfo.getCounter();
		String variableName;
		APLVar aplVar;
		String memberVariableName;
		// Reset the outer APL scope counter in order to generate correctly the member names
		outerAPLScopeInfo.setCounter(0);
		for (String member : recordType.getMembers()) {
			ATCALType memberType = recordType.getFieldType(member).get();
			if (isComplexType(memberType)) {
				for (int i = 0; i < counter; i++) {
					variableName       = VariableNamingHelper.buildDefinitiveRecordMemberVariableNameWithScope(member,
					                                                                                           outerAPLScopeInfo);
					aplVar             = new APLVar(variableName, memberType);
					memberVariableName = VariableNamingHelper.buildDefinitiveRecordMemberVariableNameWithScope(member,
					                                                                                           outerAPLScopeInfo);
					elementsInfo.addRecordMemberInfoToMaps(member, memberVariableName, aplVar, memberType);

					// Increment the outer APL scope counter in order to recover the previous counter state
					outerAPLScopeInfo.incrementCounter();
				}
			}
		}
	}

	public static void completeContractInfoMap(OuterAPLScopeInfo outerAPLScopeInfo,
	                                           ElementsInfo elementsInfo,
	                                           ContractType contractType) {
		int                    counter            = outerAPLScopeInfo.getCounter();
		Map<String, ATCALType> setterArguments    = contractType.getSetterArgs();
		Set<String>            processedVariables = elementsInfo.getProcessedvariables();
		String                 recordMemberName;
		APLVar                 recordMemberVar;
		String                 memberVariableName;
		// Reset the outer APL scope counter in order to generate correctly the member names
		outerAPLScopeInfo.setCounter(0);
		for (String member : setterArguments.keySet()) {
			ATCALType memberType = setterArguments.get(member);
			if (isComplexType(memberType) && processedVariables.contains(member)) {
				for (int i = 0; i < counter; i++) {
					recordMemberName   = VariableNamingHelper
					    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
					recordMemberVar    = new APLVar(recordMemberName, memberType);
					memberVariableName = VariableNamingHelper
					    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
					elementsInfo.addContractMemberInfoToMaps(member, memberVariableName, recordMemberVar, memberType);

					// Increment the outer APL scope counter in order to recover the previous counter state
					outerAPLScopeInfo.incrementCounter();
				}
			}
		}
	}

	public static void completeContractRecordMemberInfoMap(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                       ElementsInfo elementsInfo,
	                                                       RecordType innerRecordType) {
		int    counter = outerAPLScopeInfo.getCounter();
		String recordMemberName;
		APLVar recordMemberVar;
		String memberVariableName;
		// Reset the outer APL scope counter in order to generate correctly the member names
		outerAPLScopeInfo.setCounter(0);
		for (String memberName : innerRecordType.getMembers()) {
			ATCALType memberType = innerRecordType.getFieldType(memberName).get();
			if (isComplexType(memberType)) {
				for (int i = 0; i < counter; i++) {
					recordMemberName   = VariableNamingHelper.buildDefinitiveRecordMemberVariableNameWithScope(memberName,
					                                                                                           outerAPLScopeInfo);
					recordMemberVar    = new APLVar(recordMemberName, memberType);
					memberVariableName = VariableNamingHelper.buildDefinitiveRecordMemberVariableName(memberName,
					                                                                                  outerAPLScopeInfo);
					elementsInfo.addContractRecordMemberInfoToMaps(memberName, memberVariableName, recordMemberVar, memberType);

					// Increment the outer APL scope counter in order to recover the previous counter state
					outerAPLScopeInfo.incrementCounter();
				}
			}
		}
	}

	public static void completeContractContractMemberInfoMap(OuterAPLScopeInfo outerAPLScopeInfo,
	                                                         ElementsInfo elementsInfo,
	                                                         ContractType innerContractType) {
		int                    counter         = outerAPLScopeInfo.getCounter();
		Map<String, ATCALType> setterArguments = innerContractType.getSetterArgs();
		String                 variableName;
		APLVar                 aplVar;
		String                 memberVariableName;
		// Reset the outer APL scope counter in order to generate correctly the member names
		outerAPLScopeInfo.setCounter(0);
		for (String member : setterArguments.keySet()) {
			ATCALType memberType = setterArguments.get(member);
			if (isComplexType(memberType)) {
				for (int i = 0; i < counter; i++) {
					variableName       = VariableNamingHelper
					    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
					aplVar             = new APLVar(variableName, memberType);
					memberVariableName = VariableNamingHelper
					    .buildDefinitiveContractMemberVariableNameWithScope(member, outerAPLScopeInfo);
					elementsInfo.addContractContractMemberInfoToMaps(member, memberVariableName, aplVar, memberType);

					// Increment the outer APL scope counter in order to recover the previous counter state
					outerAPLScopeInfo.incrementCounter();
				}
			}
		}
	}

	private static boolean isComplexType(ATCALType type) {
		return (type instanceof RecordType || type instanceof ContractType || type instanceof ArrayType);
	}
}
