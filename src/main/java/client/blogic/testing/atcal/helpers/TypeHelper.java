package client.blogic.testing.atcal.helpers;

import java.util.Iterator;

import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprConst;
import client.blogic.testing.atcal.z.ast.ZExprNum;
import client.blogic.testing.atcal.z.ast.ZExprString;

public class TypeHelper {

	/**
	 * Given an {@code iterator} it gets the first of its elements (if any) to decide if the iterator holds
	 * {@link InnerAbstractType#BASIC} or {@link InnerAbstractType#COMPLEX} type elements. If there aren't any elements, then it
	 * returns {@link InnerAbstractType#NO_ELEMENTS}
	 * 
	 * @param  iterator
	 * @return
	 */
	public static InnerAbstractType getInnerAbstractType(Iterator<ZExpr> iterator) {
		if (iterator.hasNext()) {
			ZExpr element = iterator.next();
			if (element instanceof ZExprAuto || element instanceof ZExprConst || element instanceof ZExprNum
			    || element instanceof ZExprString) {
				return InnerAbstractType.BASIC;
			} else {
				return InnerAbstractType.COMPLEX;
			}
		} else {
			return InnerAbstractType.NO_ELEMENTS;
		}
	}

	public enum InnerAbstractType {
		BASIC, COMPLEX, NO_ELEMENTS
	}
}
