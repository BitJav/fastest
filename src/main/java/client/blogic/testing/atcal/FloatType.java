package client.blogic.testing.atcal;

import client.blogic.testing.atcal.apl.expressions.LongExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprNum;

/**
 * Created by Cristian on 06/05/15.
 */
public class FloatType extends ATCALType {

	// Conversion to float expression
	@Override
	public LongExpr fromZExpr(ZExpr zExpr) {
		if (zExpr instanceof ZExprNum) {
			return new LongExpr(((ZExprNum) zExpr).getNum());
		} else if (zExpr instanceof ZExprAuto) {
			return new LongExpr(0xDEADBEEF);
		}
		// Unsupported conversion
		throw new RuntimeException("Unsupported operation.");
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		boolean result = false;
		if (expr instanceof ZExprNum || expr instanceof ZExprAuto) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public String typeCategory() {
		return "Float";
	}

	@Override
	public boolean contains(String member) {
		return false;
	}

	@Override
	public String toString() {
		return "Float";
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
