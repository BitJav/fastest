// Generated from Atcal.g4 by ANTLR 4.3
package client.blogic.testing.atcal.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AtcalParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__38=1, T__37=2, T__36=3, T__35=4, T__34=5, T__33=6, T__32=7, T__31=8, 
		T__30=9, T__29=10, T__28=11, T__27=12, T__26=13, T__25=14, T__24=15, T__23=16, 
		T__22=17, T__21=18, T__20=19, T__19=20, T__18=21, T__17=22, T__16=23, 
		T__15=24, T__14=25, T__13=26, T__12=27, T__11=28, T__10=29, T__9=30, T__8=31, 
		T__7=32, T__6=33, T__5=34, T__4=35, T__3=36, T__2=37, T__1=38, T__0=39, 
		STMTEND=40, LAWS=41, PLCODE=42, ID=43, NUMBER=44, PLUS=45, MINUS=46, MUL=47, 
		DIV=48, MOD=49, DOM=50, RAN=51, PROJ=52, TUPPROJ=53, INTER=54, UNION=55, 
		DIFF=56, ELEM=57, AUTO=58, CARD=59, STRING=60, WS=61;
	public static final String[] tokenNames = {
		"<INVALID>", "'.@EPILOGUE'", "'.@PREAMBLE'", "'AS'", "'SETTER'", "'MODULE'", 
		"'DATATYPE'", "'{'", "'<=='", "'==>'", "'}'", "'='", "'CONSTRUCTOR'", 
		"'@PREAMBLE'", "'@RRULE'", "'('", "'WITH'", "','", "'.'", "'STRING'", 
		"'->'", "'MAP'", "'GETTER'", "'INT'", "'@UUT'", "':'", "'['", "'++'", 
		"'<'", "'ARRAY'", "']'", "'>'", "'RECORD'", "'REFERENCE'", "'@EPILOGUE'", 
		"'<==>'", "'FLOAT'", "'@DATATYPES'", "')'", "'ENUM'", "STMTEND", "'@LAWS'", 
		"PLCODE", "ID", "NUMBER", "'+'", "'-'", "'*'", "'/'", "'%'", "'@DOM'", 
		"'@RAN'", "PROJ", "TUPPROJ", "'/\\'", "'\\/'", "'~'", "'@ELEM'", "'@AUTOFILL'", 
		"'@CARD'", "STRING", "WS"
	};
	public static final int
		RULE_refinementRule = 0, RULE_preamble = 1, RULE_datatypes = 2, RULE_typeDec = 3, 
		RULE_type = 4, RULE_args = 5, RULE_contractMembers = 6, RULE_constMapping = 7, 
		RULE_constMap = 8, RULE_laws = 9, RULE_uut = 10, RULE_epilogue = 11, RULE_law = 12, 
		RULE_biRefLaw = 13, RULE_lawRefinement = 14, RULE_lawReference = 15, RULE_zExprs = 16, 
		RULE_refinement = 17, RULE_lvalue = 18, RULE_withRef = 19, RULE_zExpr = 20;
	public static final String[] ruleNames = {
		"refinementRule", "preamble", "datatypes", "typeDec", "type", "args", 
		"contractMembers", "constMapping", "constMap", "laws", "uut", "epilogue", 
		"law", "biRefLaw", "lawRefinement", "lawReference", "zExprs", "refinement", 
		"lvalue", "withRef", "zExpr"
	};

	@Override
	public String getGrammarFileName() { return "Atcal.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AtcalParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RefinementRuleContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public DatatypesContext datatypes() {
			return getRuleContext(DatatypesContext.class,0);
		}
		public UutContext uut() {
			return getRuleContext(UutContext.class,0);
		}
		public PreambleContext preamble() {
			return getRuleContext(PreambleContext.class,0);
		}
		public EpilogueContext epilogue() {
			return getRuleContext(EpilogueContext.class,0);
		}
		public TerminalNode PLCODE() { return getToken(AtcalParser.PLCODE, 0); }
		public LawsContext laws() {
			return getRuleContext(LawsContext.class,0);
		}
		public RefinementRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_refinementRule; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitRefinementRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RefinementRuleContext refinementRule() throws RecognitionException {
		RefinementRuleContext _localctx = new RefinementRuleContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_refinementRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42); match(T__25);
			setState(43); match(ID);
			setState(45);
			_la = _input.LA(1);
			if (_la==T__26) {
				{
				setState(44); preamble();
				}
			}

			setState(48);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(47); datatypes();
				}
			}

			setState(50); laws();
			setState(52);
			_la = _input.LA(1);
			if (_la==PLCODE) {
				{
				setState(51); match(PLCODE);
				}
			}

			setState(54); uut();
			setState(56);
			_la = _input.LA(1);
			if (_la==T__5) {
				{
				setState(55); epilogue();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreambleContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public List<TerminalNode> PLCODE() { return getTokens(AtcalParser.PLCODE); }
		public TerminalNode STMTEND(int i) {
			return getToken(AtcalParser.STMTEND, i);
		}
		public List<TerminalNode> STMTEND() { return getTokens(AtcalParser.STMTEND); }
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public TerminalNode PLCODE(int i) {
			return getToken(AtcalParser.PLCODE, i);
		}
		public PreambleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preamble; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitPreamble(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PreambleContext preamble() throws RecognitionException {
		PreambleContext _localctx = new PreambleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_preamble);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58); match(T__26);
			setState(63); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(63);
				switch (_input.LA(1)) {
				case PLCODE:
					{
					setState(59); match(PLCODE);
					}
					break;
				case ID:
					{
					setState(60); match(ID);
					setState(61); match(T__37);
					setState(62); match(STMTEND);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(65); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==PLCODE || _la==ID );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DatatypesContext extends ParserRuleContext {
		public TypeDecContext typeDec(int i) {
			return getRuleContext(TypeDecContext.class,i);
		}
		public List<TypeDecContext> typeDec() {
			return getRuleContexts(TypeDecContext.class);
		}
		public DatatypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_datatypes; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitDatatypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DatatypesContext datatypes() throws RecognitionException {
		DatatypesContext _localctx = new DatatypesContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_datatypes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(67); match(T__2);
			setState(69); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(68); typeDec();
				}
				}
				setState(71); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__33 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeDecContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public TerminalNode STMTEND() { return getToken(AtcalParser.STMTEND, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypeDecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeDec; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitTypeDec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeDecContext typeDec() throws RecognitionException {
		TypeDecContext _localctx = new TypeDecContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_typeDec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73); match(T__33);
			setState(74); match(ID);
			setState(75); match(T__28);
			setState(76); type();
			setState(77); match(STMTEND);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	 
		public TypeContext() { }
		public void copyFrom(TypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArrayTypeContext extends TypeContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode NUMBER() { return getToken(AtcalParser.NUMBER, 0); }
		public ArrayTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitArrayType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecordTypeContext extends TypeContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public RecordTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitRecordType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringTypeContext extends TypeContext {
		public StringTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitStringType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EnumTypeContext extends TypeContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public EnumTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitEnumType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContractTypeContext extends TypeContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public List<ContractMembersContext> contractMembers() {
			return getRuleContexts(ContractMembersContext.class);
		}
		public ContractMembersContext contractMembers(int i) {
			return getRuleContext(ContractMembersContext.class,i);
		}
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public TerminalNode STRING() { return getToken(AtcalParser.STRING, 0); }
		public ContractTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitContractType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NameTypeContext extends TypeContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public NameTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNameType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReferenceTypeContext extends TypeContext {
		public ReferenceTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitReferenceType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntTypeContext extends TypeContext {
		public IntTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitIntType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FloatTypeContext extends TypeContext {
		public FloatTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitFloatType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type);
		int _la;
		try {
			setState(124);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new NameTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(79); match(ID);
				}
				break;
			case T__16:
				_localctx = new IntTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(80); match(T__16);
				}
				break;
			case T__3:
				_localctx = new FloatTypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(81); match(T__3);
				}
				break;
			case T__20:
				_localctx = new StringTypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(82); match(T__20);
				}
				break;
			case T__6:
				_localctx = new ReferenceTypeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(83); match(T__6);
				}
				break;
			case T__10:
				_localctx = new ArrayTypeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(84); match(T__10);
				setState(85); type();
				setState(86); match(T__24);
				setState(87); match(NUMBER);
				setState(88); match(T__1);
				}
				break;
			case T__0:
				_localctx = new EnumTypeContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(90); match(T__0);
				setState(91); match(ID);
				setState(92); args();
				}
				break;
			case T__7:
				_localctx = new RecordTypeContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(93); match(T__7);
				setState(94); match(ID);
				setState(95); match(T__24);
				setState(96); match(ID);
				setState(97); match(T__14);
				setState(98); type();
				setState(105);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__22) {
					{
					{
					setState(99); match(T__22);
					setState(100); match(ID);
					setState(101); match(T__14);
					setState(102); type();
					}
					}
					setState(107);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(108); match(T__1);
				}
				break;
			case T__34:
			case T__27:
				_localctx = new ContractTypeContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(112);
				_la = _input.LA(1);
				if (_la==T__34) {
					{
					setState(110); match(T__34);
					setState(111); match(STRING);
					}
				}

				setState(114); match(T__27);
				setState(115); match(ID);
				setState(116); contractMembers();
				setState(117); match(T__35);
				setState(118); match(ID);
				setState(119); contractMembers();
				setState(120); match(T__17);
				setState(121); match(ID);
				setState(122); contractMembers();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_args);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126); match(T__24);
			setState(135);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(127); match(ID);
				setState(132);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__22) {
					{
					{
					setState(128); match(T__22);
					setState(129); match(ID);
					}
					}
					setState(134);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(137); match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContractMembersContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public ContractMembersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contractMembers; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitContractMembers(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ContractMembersContext contractMembers() throws RecognitionException {
		ContractMembersContext _localctx = new ContractMembersContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_contractMembers);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(139); match(T__24);
			setState(152);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(140); match(ID);
				setState(141); match(T__14);
				setState(142); type();
				setState(149);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__22) {
					{
					{
					setState(143); match(T__22);
					setState(144); match(ID);
					setState(145); match(T__14);
					setState(146); type();
					}
					}
					setState(151);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(154); match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstMappingContext extends ParserRuleContext {
		public ConstMapContext constMap(int i) {
			return getRuleContext(ConstMapContext.class,i);
		}
		public List<ConstMapContext> constMap() {
			return getRuleContexts(ConstMapContext.class);
		}
		public ConstMappingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constMapping; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitConstMapping(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstMappingContext constMapping() throws RecognitionException {
		ConstMappingContext _localctx = new ConstMappingContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_constMapping);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(156); match(T__18);
			setState(157); match(T__13);
			setState(158); constMap();
			setState(163);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__22) {
				{
				{
				setState(159); match(T__22);
				setState(160); constMap();
				}
				}
				setState(165);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(166); match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstMapContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public TerminalNode STRING() { return getToken(AtcalParser.STRING, 0); }
		public TerminalNode NUMBER() { return getToken(AtcalParser.NUMBER, 0); }
		public ConstMapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constMap; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitConstMap(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstMapContext constMap() throws RecognitionException {
		ConstMapContext _localctx = new ConstMapContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_constMap);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(168); match(ID);
			setState(169); match(T__19);
			setState(170);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ID) | (1L << NUMBER) | (1L << STRING))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LawsContext extends ParserRuleContext {
		public List<LawContext> law() {
			return getRuleContexts(LawContext.class);
		}
		public LawContext law(int i) {
			return getRuleContext(LawContext.class,i);
		}
		public TerminalNode STMTEND(int i) {
			return getToken(AtcalParser.STMTEND, i);
		}
		public List<TerminalNode> STMTEND() { return getTokens(AtcalParser.STMTEND); }
		public LawsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_laws; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitLaws(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LawsContext laws() throws RecognitionException {
		LawsContext _localctx = new LawsContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_laws);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(172); match(LAWS);
			setState(178);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(173); law();
					setState(174); match(STMTEND);
					}
					} 
				}
				setState(180);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UutContext extends ParserRuleContext {
		public UutContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uut; }
	 
		public UutContext() { }
		public void copyFrom(UutContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class UUTNoRetValContext extends UutContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public TerminalNode STMTEND() { return getToken(AtcalParser.STMTEND, 0); }
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public UUTNoRetValContext(UutContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitUUTNoRetVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UUTRetValContext extends UutContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public TerminalNode STMTEND() { return getToken(AtcalParser.STMTEND, 0); }
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public UUTRetValContext(UutContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitUUTRetVal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UutContext uut() throws RecognitionException {
		UutContext _localctx = new UutContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_uut);
		try {
			setState(195);
			switch (_input.LA(1)) {
			case T__15:
				_localctx = new UUTNoRetValContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(181); match(T__15);
				setState(182); match(ID);
				setState(183); args();
				setState(184); match(STMTEND);
				}
				break;
			case ID:
				_localctx = new UUTRetValContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(186); match(ID);
				setState(187); match(T__31);
				setState(188); match(T__15);
				setState(189); match(ID);
				setState(190); args();
				setState(191); match(T__36);
				setState(192); type();
				setState(193); match(STMTEND);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EpilogueContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public List<TerminalNode> PLCODE() { return getTokens(AtcalParser.PLCODE); }
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public TerminalNode PLCODE(int i) {
			return getToken(AtcalParser.PLCODE, i);
		}
		public EpilogueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_epilogue; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitEpilogue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EpilogueContext epilogue() throws RecognitionException {
		EpilogueContext _localctx = new EpilogueContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_epilogue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197); match(T__5);
			setState(201); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(201);
				switch (_input.LA(1)) {
				case PLCODE:
					{
					setState(198); match(PLCODE);
					}
					break;
				case ID:
					{
					setState(199); match(ID);
					setState(200); match(T__38);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(203); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==PLCODE || _la==ID );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LawContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public LawRefinementContext lawRefinement() {
			return getRuleContext(LawRefinementContext.class,0);
		}
		public LawReferenceContext lawReference() {
			return getRuleContext(LawReferenceContext.class,0);
		}
		public BiRefLawContext biRefLaw() {
			return getRuleContext(BiRefLawContext.class,0);
		}
		public LawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_law; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitLaw(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LawContext law() throws RecognitionException {
		LawContext _localctx = new LawContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_law);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(205); match(ID);
				setState(206); match(T__14);
				}
				break;
			}
			setState(212);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(209); lawRefinement();
				}
				break;

			case 2:
				{
				setState(210); biRefLaw();
				}
				break;

			case 3:
				{
				setState(211); lawReference();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BiRefLawContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public RefinementContext refinement(int i) {
			return getRuleContext(RefinementContext.class,i);
		}
		public List<RefinementContext> refinement() {
			return getRuleContexts(RefinementContext.class);
		}
		public BiRefLawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_biRefLaw; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitBiRefLaw(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BiRefLawContext biRefLaw() throws RecognitionException {
		BiRefLawContext _localctx = new BiRefLawContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_biRefLaw);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214); match(ID);
			setState(215); match(T__4);
			setState(216); refinement();
			setState(221);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__22) {
				{
				{
				setState(217); match(T__22);
				setState(218); refinement();
				}
				}
				setState(223);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LawRefinementContext extends ParserRuleContext {
		public RefinementContext refinement(int i) {
			return getRuleContext(RefinementContext.class,i);
		}
		public List<RefinementContext> refinement() {
			return getRuleContexts(RefinementContext.class);
		}
		public ZExprsContext zExprs() {
			return getRuleContext(ZExprsContext.class,0);
		}
		public LawRefinementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lawRefinement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitLawRefinement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LawRefinementContext lawRefinement() throws RecognitionException {
		LawRefinementContext _localctx = new LawRefinementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_lawRefinement);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(224); zExprs();
			setState(225); match(T__30);
			setState(226); refinement();
			setState(231);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(227); match(T__22);
					setState(228); refinement();
					}
					} 
				}
				setState(233);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LawReferenceContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AtcalParser.ID); }
		public TerminalNode LAWS() { return getToken(AtcalParser.LAWS, 0); }
		public TerminalNode ID(int i) {
			return getToken(AtcalParser.ID, i);
		}
		public LawReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lawReference; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitLawReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LawReferenceContext lawReference() throws RecognitionException {
		LawReferenceContext _localctx = new LawReferenceContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_lawReference);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(234); match(ID);
			setState(235); match(T__21);
			setState(236);
			_la = _input.LA(1);
			if ( !(_la==LAWS || _la==ID) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ZExprsContext extends ParserRuleContext {
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public ZExprsContext zExprs() {
			return getRuleContext(ZExprsContext.class,0);
		}
		public ZExprsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zExprs; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitZExprs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ZExprsContext zExprs() throws RecognitionException {
		ZExprsContext _localctx = new ZExprsContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_zExprs);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238); zExpr(0);
			setState(241);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				{
				setState(239); match(T__30);
				setState(240); zExprs();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RefinementContext extends ParserRuleContext {
		public ConstMappingContext constMapping() {
			return getRuleContext(ConstMappingContext.class,0);
		}
		public LvalueContext lvalue() {
			return getRuleContext(LvalueContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public WithRefContext withRef() {
			return getRuleContext(WithRefContext.class,0);
		}
		public RefinementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_refinement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitRefinement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RefinementContext refinement() throws RecognitionException {
		RefinementContext _localctx = new RefinementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_refinement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(243); lvalue();
			setState(244); match(T__36);
			setState(245); type();
			setState(247);
			_la = _input.LA(1);
			if (_la==T__18) {
				{
				setState(246); constMapping();
				}
			}

			setState(250);
			_la = _input.LA(1);
			if (_la==T__23) {
				{
				setState(249); withRef();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LvalueContext extends ParserRuleContext {
		public LvalueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lvalue; }
	 
		public LvalueContext() { }
		public void copyFrom(LvalueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VarLValueContext extends LvalueContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public VarLValueContext(LvalueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitVarLValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayLValueContext extends LvalueContext {
		public TerminalNode NUMBER() { return getToken(AtcalParser.NUMBER, 0); }
		public ArrayLValueContext(LvalueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitArrayLValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FieldLValueContext extends LvalueContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public FieldLValueContext(LvalueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitFieldLValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LvalueContext lvalue() throws RecognitionException {
		LvalueContext _localctx = new LvalueContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_lvalue);
		int _la;
		try {
			setState(260);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new VarLValueContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(252); match(ID);
				}
				break;
			case T__13:
				_localctx = new ArrayLValueContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(253); match(T__13);
				setState(255);
				_la = _input.LA(1);
				if (_la==NUMBER) {
					{
					setState(254); match(NUMBER);
					}
				}

				setState(257); match(T__9);
				}
				break;
			case T__21:
				_localctx = new FieldLValueContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(258); match(T__21);
				setState(259); match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WithRefContext extends ParserRuleContext {
		public List<LawRefinementContext> lawRefinement() {
			return getRuleContexts(LawRefinementContext.class);
		}
		public LawRefinementContext lawRefinement(int i) {
			return getRuleContext(LawRefinementContext.class,i);
		}
		public WithRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_withRef; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitWithRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WithRefContext withRef() throws RecognitionException {
		WithRefContext _localctx = new WithRefContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_withRef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262); match(T__23);
			setState(263); match(T__13);
			setState(264); lawRefinement();
			setState(269);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__22) {
				{
				{
				setState(265); match(T__22);
				setState(266); lawRefinement();
				}
				}
				setState(271);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(272); match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ZExprContext extends ParserRuleContext {
		public ZExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zExpr; }
	 
		public ZExprContext() { }
		public void copyFrom(ZExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GroupContext extends ZExprContext {
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public GroupContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitGroup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumModContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public TerminalNode MOD() { return getToken(AtcalParser.MOD, 0); }
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public NumModContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNumMod(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ProdProjContext extends ZExprContext {
		public TerminalNode TUPPROJ() { return getToken(AtcalParser.TUPPROJ, 0); }
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public ProdProjContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitProdProj(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentContext extends ZExprContext {
		public TerminalNode ID() { return getToken(AtcalParser.ID, 0); }
		public IdentContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitIdent(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumMulContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public TerminalNode MUL() { return getToken(AtcalParser.MUL, 0); }
		public NumMulContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNumMul(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetDiffContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public TerminalNode DIFF() { return getToken(AtcalParser.DIFF, 0); }
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public SetDiffContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetDiff(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StrConcatContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public StrConcatContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitStrConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ProdConsContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public ProdConsContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitProdCons(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetUnionContext extends ZExprContext {
		public TerminalNode UNION() { return getToken(AtcalParser.UNION, 0); }
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public SetUnionContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetUnion(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetDomContext extends ZExprContext {
		public TerminalNode DOM() { return getToken(AtcalParser.DOM, 0); }
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public SetDomContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetDom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ElemExprContext extends ZExprContext {
		public TerminalNode ELEM() { return getToken(AtcalParser.ELEM, 0); }
		public ElemExprContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitElemExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumLiteralContext extends ZExprContext {
		public TerminalNode NUMBER() { return getToken(AtcalParser.NUMBER, 0); }
		public NumLiteralContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNumLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumMinusContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public TerminalNode MINUS() { return getToken(AtcalParser.MINUS, 0); }
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public NumMinusContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNumMinus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StrLiteralContext extends ZExprContext {
		public TerminalNode STRING() { return getToken(AtcalParser.STRING, 0); }
		public StrLiteralContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitStrLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AutoExprContext extends ZExprContext {
		public TerminalNode AUTO() { return getToken(AtcalParser.AUTO, 0); }
		public AutoExprContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitAutoExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetConsContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public SetConsContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetCons(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumPlusContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(AtcalParser.PLUS, 0); }
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public NumPlusContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNumPlus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetCardContext extends ZExprContext {
		public TerminalNode CARD() { return getToken(AtcalParser.CARD, 0); }
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public SetCardContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetCard(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumDivContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public TerminalNode DIV() { return getToken(AtcalParser.DIV, 0); }
		public NumDivContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitNumDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetProjContext extends ZExprContext {
		public TerminalNode PROJ() { return getToken(AtcalParser.PROJ, 0); }
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public SetProjContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetProj(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetRanContext extends ZExprContext {
		public TerminalNode RAN() { return getToken(AtcalParser.RAN, 0); }
		public ZExprContext zExpr() {
			return getRuleContext(ZExprContext.class,0);
		}
		public SetRanContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetRan(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetInterContext extends ZExprContext {
		public ZExprContext zExpr(int i) {
			return getRuleContext(ZExprContext.class,i);
		}
		public TerminalNode INTER() { return getToken(AtcalParser.INTER, 0); }
		public List<ZExprContext> zExpr() {
			return getRuleContexts(ZExprContext.class);
		}
		public SetInterContext(ZExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AtcalVisitor ) return ((AtcalVisitor<? extends T>)visitor).visitSetInter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ZExprContext zExpr() throws RecognitionException {
		return zExpr(0);
	}

	private ZExprContext zExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ZExprContext _localctx = new ZExprContext(_ctx, _parentState);
		ZExprContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_zExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(306);
			switch (_input.LA(1)) {
			case ID:
				{
				_localctx = new IdentContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(275); match(ID);
				}
				break;
			case NUMBER:
				{
				_localctx = new NumLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(276); match(NUMBER);
				}
				break;
			case STRING:
				{
				_localctx = new StrLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(277); match(STRING);
				}
				break;
			case AUTO:
				{
				_localctx = new AutoExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(278); match(AUTO);
				}
				break;
			case ELEM:
				{
				_localctx = new ElemExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(279); match(ELEM);
				}
				break;
			case T__11:
				{
				_localctx = new ProdConsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(280); match(T__11);
				setState(281); zExpr(0);
				setState(286);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__22) {
					{
					{
					setState(282); match(T__22);
					setState(283); zExpr(0);
					}
					}
					setState(288);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(289); match(T__8);
				}
				break;
			case T__32:
				{
				_localctx = new SetConsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(291); match(T__32);
				setState(292); zExpr(0);
				setState(297);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__22) {
					{
					{
					setState(293); match(T__22);
					setState(294); zExpr(0);
					}
					}
					setState(299);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(300); match(T__29);
				}
				break;
			case T__24:
				{
				_localctx = new GroupContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(302); match(T__24);
				setState(303); zExpr(0);
				setState(304); match(T__1);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(352);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(350);
					switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
					case 1:
						{
						_localctx = new SetInterContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(308);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(309); match(INTER);
						setState(310); zExpr(13);
						}
						break;

					case 2:
						{
						_localctx = new SetUnionContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(311);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(312); match(UNION);
						setState(313); zExpr(12);
						}
						break;

					case 3:
						{
						_localctx = new SetDiffContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(314);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(315); match(DIFF);
						setState(316); zExpr(11);
						}
						break;

					case 4:
						{
						_localctx = new NumMulContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(317);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(318); match(MUL);
						setState(319); zExpr(8);
						}
						break;

					case 5:
						{
						_localctx = new NumDivContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(320);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(321); match(DIV);
						setState(322); zExpr(7);
						}
						break;

					case 6:
						{
						_localctx = new NumModContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(323);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(324); match(MOD);
						setState(325); zExpr(6);
						}
						break;

					case 7:
						{
						_localctx = new NumPlusContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(326);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(327); match(PLUS);
						setState(328); zExpr(5);
						}
						break;

					case 8:
						{
						_localctx = new NumMinusContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(329);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(330); match(MINUS);
						setState(331); zExpr(4);
						}
						break;

					case 9:
						{
						_localctx = new StrConcatContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(332);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(333); match(T__12);
						setState(334); zExpr(3);
						}
						break;

					case 10:
						{
						_localctx = new ProdProjContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(335);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(336); match(T__21);
						setState(337); match(TUPPROJ);
						}
						break;

					case 11:
						{
						_localctx = new SetDomContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(338);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(339); match(T__21);
						setState(340); match(DOM);
						}
						break;

					case 12:
						{
						_localctx = new SetRanContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(341);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(342); match(T__21);
						setState(343); match(RAN);
						}
						break;

					case 13:
						{
						_localctx = new SetProjContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(344);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(345); match(T__21);
						setState(346); match(PROJ);
						}
						break;

					case 14:
						{
						_localctx = new SetCardContext(new ZExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_zExpr);
						setState(347);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(348); match(T__21);
						setState(349); match(CARD);
						}
						break;
					}
					} 
				}
				setState(354);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 20: return zExpr_sempred((ZExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean zExpr_sempred(ZExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 12);

		case 1: return precpred(_ctx, 11);

		case 2: return precpred(_ctx, 10);

		case 3: return precpred(_ctx, 7);

		case 4: return precpred(_ctx, 6);

		case 5: return precpred(_ctx, 5);

		case 6: return precpred(_ctx, 4);

		case 7: return precpred(_ctx, 3);

		case 8: return precpred(_ctx, 2);

		case 9: return precpred(_ctx, 17);

		case 10: return precpred(_ctx, 15);

		case 11: return precpred(_ctx, 14);

		case 12: return precpred(_ctx, 13);

		case 13: return precpred(_ctx, 8);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3?\u0166\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\2\5\2\60\n\2\3\2\5"+
		"\2\63\n\2\3\2\3\2\5\2\67\n\2\3\2\3\2\5\2;\n\2\3\3\3\3\3\3\3\3\3\3\6\3"+
		"B\n\3\r\3\16\3C\3\4\3\4\6\4H\n\4\r\4\16\4I\3\5\3\5\3\5\3\5\3\5\3\5\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\7\6j\n\6\f\6\16\6m\13\6\3\6\3\6\3\6\3\6\5\6s\n"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\177\n\6\3\7\3\7\3\7\3\7"+
		"\7\7\u0085\n\7\f\7\16\7\u0088\13\7\5\7\u008a\n\7\3\7\3\7\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\7\b\u0096\n\b\f\b\16\b\u0099\13\b\5\b\u009b\n\b\3\b"+
		"\3\b\3\t\3\t\3\t\3\t\3\t\7\t\u00a4\n\t\f\t\16\t\u00a7\13\t\3\t\3\t\3\n"+
		"\3\n\3\n\3\n\3\13\3\13\3\13\3\13\7\13\u00b3\n\13\f\13\16\13\u00b6\13\13"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00c6\n\f"+
		"\3\r\3\r\3\r\3\r\6\r\u00cc\n\r\r\r\16\r\u00cd\3\16\3\16\5\16\u00d2\n\16"+
		"\3\16\3\16\3\16\5\16\u00d7\n\16\3\17\3\17\3\17\3\17\3\17\7\17\u00de\n"+
		"\17\f\17\16\17\u00e1\13\17\3\20\3\20\3\20\3\20\3\20\7\20\u00e8\n\20\f"+
		"\20\16\20\u00eb\13\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\5\22\u00f4\n"+
		"\22\3\23\3\23\3\23\3\23\5\23\u00fa\n\23\3\23\5\23\u00fd\n\23\3\24\3\24"+
		"\3\24\5\24\u0102\n\24\3\24\3\24\3\24\5\24\u0107\n\24\3\25\3\25\3\25\3"+
		"\25\3\25\7\25\u010e\n\25\f\25\16\25\u0111\13\25\3\25\3\25\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u011f\n\26\f\26\16\26\u0122"+
		"\13\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u012a\n\26\f\26\16\26\u012d"+
		"\13\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0135\n\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u0161\n\26"+
		"\f\26\16\26\u0164\13\26\3\26\2\3*\27\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(*\2\4\4\2-.>>\4\2++--\u018d\2,\3\2\2\2\4<\3\2\2\2\6E\3\2\2"+
		"\2\bK\3\2\2\2\n~\3\2\2\2\f\u0080\3\2\2\2\16\u008d\3\2\2\2\20\u009e\3\2"+
		"\2\2\22\u00aa\3\2\2\2\24\u00ae\3\2\2\2\26\u00c5\3\2\2\2\30\u00c7\3\2\2"+
		"\2\32\u00d1\3\2\2\2\34\u00d8\3\2\2\2\36\u00e2\3\2\2\2 \u00ec\3\2\2\2\""+
		"\u00f0\3\2\2\2$\u00f5\3\2\2\2&\u0106\3\2\2\2(\u0108\3\2\2\2*\u0134\3\2"+
		"\2\2,-\7\20\2\2-/\7-\2\2.\60\5\4\3\2/.\3\2\2\2/\60\3\2\2\2\60\62\3\2\2"+
		"\2\61\63\5\6\4\2\62\61\3\2\2\2\62\63\3\2\2\2\63\64\3\2\2\2\64\66\5\24"+
		"\13\2\65\67\7,\2\2\66\65\3\2\2\2\66\67\3\2\2\2\678\3\2\2\28:\5\26\f\2"+
		"9;\5\30\r\2:9\3\2\2\2:;\3\2\2\2;\3\3\2\2\2<A\7\17\2\2=B\7,\2\2>?\7-\2"+
		"\2?@\7\4\2\2@B\7*\2\2A=\3\2\2\2A>\3\2\2\2BC\3\2\2\2CA\3\2\2\2CD\3\2\2"+
		"\2D\5\3\2\2\2EG\7\'\2\2FH\5\b\5\2GF\3\2\2\2HI\3\2\2\2IG\3\2\2\2IJ\3\2"+
		"\2\2J\7\3\2\2\2KL\7\b\2\2LM\7-\2\2MN\7\r\2\2NO\5\n\6\2OP\7*\2\2P\t\3\2"+
		"\2\2Q\177\7-\2\2R\177\7\31\2\2S\177\7&\2\2T\177\7\25\2\2U\177\7#\2\2V"+
		"W\7\37\2\2WX\5\n\6\2XY\7\21\2\2YZ\7.\2\2Z[\7(\2\2[\177\3\2\2\2\\]\7)\2"+
		"\2]^\7-\2\2^\177\5\f\7\2_`\7\"\2\2`a\7-\2\2ab\7\21\2\2bc\7-\2\2cd\7\33"+
		"\2\2dk\5\n\6\2ef\7\23\2\2fg\7-\2\2gh\7\33\2\2hj\5\n\6\2ie\3\2\2\2jm\3"+
		"\2\2\2ki\3\2\2\2kl\3\2\2\2ln\3\2\2\2mk\3\2\2\2no\7(\2\2o\177\3\2\2\2p"+
		"q\7\7\2\2qs\7>\2\2rp\3\2\2\2rs\3\2\2\2st\3\2\2\2tu\7\16\2\2uv\7-\2\2v"+
		"w\5\16\b\2wx\7\6\2\2xy\7-\2\2yz\5\16\b\2z{\7\30\2\2{|\7-\2\2|}\5\16\b"+
		"\2}\177\3\2\2\2~Q\3\2\2\2~R\3\2\2\2~S\3\2\2\2~T\3\2\2\2~U\3\2\2\2~V\3"+
		"\2\2\2~\\\3\2\2\2~_\3\2\2\2~r\3\2\2\2\177\13\3\2\2\2\u0080\u0089\7\21"+
		"\2\2\u0081\u0086\7-\2\2\u0082\u0083\7\23\2\2\u0083\u0085\7-\2\2\u0084"+
		"\u0082\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2\u0086\u0087\3\2"+
		"\2\2\u0087\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0089\u0081\3\2\2\2\u0089"+
		"\u008a\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008c\7(\2\2\u008c\r\3\2\2\2"+
		"\u008d\u009a\7\21\2\2\u008e\u008f\7-\2\2\u008f\u0090\7\33\2\2\u0090\u0097"+
		"\5\n\6\2\u0091\u0092\7\23\2\2\u0092\u0093\7-\2\2\u0093\u0094\7\33\2\2"+
		"\u0094\u0096\5\n\6\2\u0095\u0091\3\2\2\2\u0096\u0099\3\2\2\2\u0097\u0095"+
		"\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009b\3\2\2\2\u0099\u0097\3\2\2\2\u009a"+
		"\u008e\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u009d\7("+
		"\2\2\u009d\17\3\2\2\2\u009e\u009f\7\27\2\2\u009f\u00a0\7\34\2\2\u00a0"+
		"\u00a5\5\22\n\2\u00a1\u00a2\7\23\2\2\u00a2\u00a4\5\22\n\2\u00a3\u00a1"+
		"\3\2\2\2\u00a4\u00a7\3\2\2\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6"+
		"\u00a8\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a8\u00a9\7 \2\2\u00a9\21\3\2\2\2"+
		"\u00aa\u00ab\7-\2\2\u00ab\u00ac\7\26\2\2\u00ac\u00ad\t\2\2\2\u00ad\23"+
		"\3\2\2\2\u00ae\u00b4\7+\2\2\u00af\u00b0\5\32\16\2\u00b0\u00b1\7*\2\2\u00b1"+
		"\u00b3\3\2\2\2\u00b2\u00af\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4\u00b2\3\2"+
		"\2\2\u00b4\u00b5\3\2\2\2\u00b5\25\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b7\u00b8"+
		"\7\32\2\2\u00b8\u00b9\7-\2\2\u00b9\u00ba\5\f\7\2\u00ba\u00bb\7*\2\2\u00bb"+
		"\u00c6\3\2\2\2\u00bc\u00bd\7-\2\2\u00bd\u00be\7\n\2\2\u00be\u00bf\7\32"+
		"\2\2\u00bf\u00c0\7-\2\2\u00c0\u00c1\5\f\7\2\u00c1\u00c2\7\5\2\2\u00c2"+
		"\u00c3\5\n\6\2\u00c3\u00c4\7*\2\2\u00c4\u00c6\3\2\2\2\u00c5\u00b7\3\2"+
		"\2\2\u00c5\u00bc\3\2\2\2\u00c6\27\3\2\2\2\u00c7\u00cb\7$\2\2\u00c8\u00cc"+
		"\7,\2\2\u00c9\u00ca\7-\2\2\u00ca\u00cc\7\3\2\2\u00cb\u00c8\3\2\2\2\u00cb"+
		"\u00c9\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce\3\2"+
		"\2\2\u00ce\31\3\2\2\2\u00cf\u00d0\7-\2\2\u00d0\u00d2\7\33\2\2\u00d1\u00cf"+
		"\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d6\3\2\2\2\u00d3\u00d7\5\36\20\2"+
		"\u00d4\u00d7\5\34\17\2\u00d5\u00d7\5 \21\2\u00d6\u00d3\3\2\2\2\u00d6\u00d4"+
		"\3\2\2\2\u00d6\u00d5\3\2\2\2\u00d7\33\3\2\2\2\u00d8\u00d9\7-\2\2\u00d9"+
		"\u00da\7%\2\2\u00da\u00df\5$\23\2\u00db\u00dc\7\23\2\2\u00dc\u00de\5$"+
		"\23\2\u00dd\u00db\3\2\2\2\u00de\u00e1\3\2\2\2\u00df\u00dd\3\2\2\2\u00df"+
		"\u00e0\3\2\2\2\u00e0\35\3\2\2\2\u00e1\u00df\3\2\2\2\u00e2\u00e3\5\"\22"+
		"\2\u00e3\u00e4\7\13\2\2\u00e4\u00e9\5$\23\2\u00e5\u00e6\7\23\2\2\u00e6"+
		"\u00e8\5$\23\2\u00e7\u00e5\3\2\2\2\u00e8\u00eb\3\2\2\2\u00e9\u00e7\3\2"+
		"\2\2\u00e9\u00ea\3\2\2\2\u00ea\37\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec\u00ed"+
		"\7-\2\2\u00ed\u00ee\7\24\2\2\u00ee\u00ef\t\3\2\2\u00ef!\3\2\2\2\u00f0"+
		"\u00f3\5*\26\2\u00f1\u00f2\7\13\2\2\u00f2\u00f4\5\"\22\2\u00f3\u00f1\3"+
		"\2\2\2\u00f3\u00f4\3\2\2\2\u00f4#\3\2\2\2\u00f5\u00f6\5&\24\2\u00f6\u00f7"+
		"\7\5\2\2\u00f7\u00f9\5\n\6\2\u00f8\u00fa\5\20\t\2\u00f9\u00f8\3\2\2\2"+
		"\u00f9\u00fa\3\2\2\2\u00fa\u00fc\3\2\2\2\u00fb\u00fd\5(\25\2\u00fc\u00fb"+
		"\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd%\3\2\2\2\u00fe\u0107\7-\2\2\u00ff\u0101"+
		"\7\34\2\2\u0100\u0102\7.\2\2\u0101\u0100\3\2\2\2\u0101\u0102\3\2\2\2\u0102"+
		"\u0103\3\2\2\2\u0103\u0107\7 \2\2\u0104\u0105\7\24\2\2\u0105\u0107\7-"+
		"\2\2\u0106\u00fe\3\2\2\2\u0106\u00ff\3\2\2\2\u0106\u0104\3\2\2\2\u0107"+
		"\'\3\2\2\2\u0108\u0109\7\22\2\2\u0109\u010a\7\34\2\2\u010a\u010f\5\36"+
		"\20\2\u010b\u010c\7\23\2\2\u010c\u010e\5\36\20\2\u010d\u010b\3\2\2\2\u010e"+
		"\u0111\3\2\2\2\u010f\u010d\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0112\3\2"+
		"\2\2\u0111\u010f\3\2\2\2\u0112\u0113\7 \2\2\u0113)\3\2\2\2\u0114\u0115"+
		"\b\26\1\2\u0115\u0135\7-\2\2\u0116\u0135\7.\2\2\u0117\u0135\7>\2\2\u0118"+
		"\u0135\7<\2\2\u0119\u0135\7;\2\2\u011a\u011b\7\36\2\2\u011b\u0120\5*\26"+
		"\2\u011c\u011d\7\23\2\2\u011d\u011f\5*\26\2\u011e\u011c\3\2\2\2\u011f"+
		"\u0122\3\2\2\2\u0120\u011e\3\2\2\2\u0120\u0121\3\2\2\2\u0121\u0123\3\2"+
		"\2\2\u0122\u0120\3\2\2\2\u0123\u0124\7!\2\2\u0124\u0135\3\2\2\2\u0125"+
		"\u0126\7\t\2\2\u0126\u012b\5*\26\2\u0127\u0128\7\23\2\2\u0128\u012a\5"+
		"*\26\2\u0129\u0127\3\2\2\2\u012a\u012d\3\2\2\2\u012b\u0129\3\2\2\2\u012b"+
		"\u012c\3\2\2\2\u012c\u012e\3\2\2\2\u012d\u012b\3\2\2\2\u012e\u012f\7\f"+
		"\2\2\u012f\u0135\3\2\2\2\u0130\u0131\7\21\2\2\u0131\u0132\5*\26\2\u0132"+
		"\u0133\7(\2\2\u0133\u0135\3\2\2\2\u0134\u0114\3\2\2\2\u0134\u0116\3\2"+
		"\2\2\u0134\u0117\3\2\2\2\u0134\u0118\3\2\2\2\u0134\u0119\3\2\2\2\u0134"+
		"\u011a\3\2\2\2\u0134\u0125\3\2\2\2\u0134\u0130\3\2\2\2\u0135\u0162\3\2"+
		"\2\2\u0136\u0137\f\16\2\2\u0137\u0138\78\2\2\u0138\u0161\5*\26\17\u0139"+
		"\u013a\f\r\2\2\u013a\u013b\79\2\2\u013b\u0161\5*\26\16\u013c\u013d\f\f"+
		"\2\2\u013d\u013e\7:\2\2\u013e\u0161\5*\26\r\u013f\u0140\f\t\2\2\u0140"+
		"\u0141\7\61\2\2\u0141\u0161\5*\26\n\u0142\u0143\f\b\2\2\u0143\u0144\7"+
		"\62\2\2\u0144\u0161\5*\26\t\u0145\u0146\f\7\2\2\u0146\u0147\7\63\2\2\u0147"+
		"\u0161\5*\26\b\u0148\u0149\f\6\2\2\u0149\u014a\7/\2\2\u014a\u0161\5*\26"+
		"\7\u014b\u014c\f\5\2\2\u014c\u014d\7\60\2\2\u014d\u0161\5*\26\6\u014e"+
		"\u014f\f\4\2\2\u014f\u0150\7\35\2\2\u0150\u0161\5*\26\5\u0151\u0152\f"+
		"\23\2\2\u0152\u0153\7\24\2\2\u0153\u0161\7\67\2\2\u0154\u0155\f\21\2\2"+
		"\u0155\u0156\7\24\2\2\u0156\u0161\7\64\2\2\u0157\u0158\f\20\2\2\u0158"+
		"\u0159\7\24\2\2\u0159\u0161\7\65\2\2\u015a\u015b\f\17\2\2\u015b\u015c"+
		"\7\24\2\2\u015c\u0161\7\66\2\2\u015d\u015e\f\n\2\2\u015e\u015f\7\24\2"+
		"\2\u015f\u0161\7=\2\2\u0160\u0136\3\2\2\2\u0160\u0139\3\2\2\2\u0160\u013c"+
		"\3\2\2\2\u0160\u013f\3\2\2\2\u0160\u0142\3\2\2\2\u0160\u0145\3\2\2\2\u0160"+
		"\u0148\3\2\2\2\u0160\u014b\3\2\2\2\u0160\u014e\3\2\2\2\u0160\u0151\3\2"+
		"\2\2\u0160\u0154\3\2\2\2\u0160\u0157\3\2\2\2\u0160\u015a\3\2\2\2\u0160"+
		"\u015d\3\2\2\2\u0161\u0164\3\2\2\2\u0162\u0160\3\2\2\2\u0162\u0163\3\2"+
		"\2\2\u0163+\3\2\2\2\u0164\u0162\3\2\2\2$/\62\66:ACIkr~\u0086\u0089\u0097"+
		"\u009a\u00a5\u00b4\u00c5\u00cb\u00cd\u00d1\u00d6\u00df\u00e9\u00f3\u00f9"+
		"\u00fc\u0101\u0106\u010f\u0120\u012b\u0134\u0160\u0162";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}