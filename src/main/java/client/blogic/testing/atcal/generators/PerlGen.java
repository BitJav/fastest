package client.blogic.testing.atcal.generators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.ArrayType;
import client.blogic.testing.atcal.ContractType;
import client.blogic.testing.atcal.FloatType;
import client.blogic.testing.atcal.IntType;
import client.blogic.testing.atcal.RecordType;
import client.blogic.testing.atcal.StringType;
import client.blogic.testing.atcal.apl.APLLValue;
import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.apl.expressions.APLExprList;
import client.blogic.testing.atcal.apl.expressions.CallExpr;
import client.blogic.testing.atcal.apl.expressions.ConsExpr;
import client.blogic.testing.atcal.apl.expressions.LongExpr;
import client.blogic.testing.atcal.apl.expressions.StringExpr;
import client.blogic.testing.atcal.apl.statements.APLStmt;
import client.blogic.testing.atcal.apl.statements.ArrayDeclStmt;
import client.blogic.testing.atcal.apl.statements.AssignStmt;
import client.blogic.testing.atcal.apl.statements.ConstructorCallStmt;
import client.blogic.testing.atcal.apl.statements.RecordStmt;
import client.blogic.testing.atcal.apl.statements.SetterCallStmt;

/**
 * The Perl generator translates the APL code into a Perl program.
 * Created by Cristian on 26/06/15.
 */
public class PerlGen implements Generator {

	private static boolean isScalarType(ATCALType type) {
		return type instanceof IntType || type instanceof FloatType || type instanceof StringType;
	}

	@Override
	public String getTargetLanguage() {
		return "perl";
	}

	@Override
	public String getDumpCode(Collection<APLLValue> lvaluesSet) {
		String dumpCode = lvaluesSet.stream()
		    .map(lvalue -> "$state->{'" + lvalue
		        .getName() + "'} = " + (isScalarType(lvalue.getType()) ? "" : " \\") + generate(lvalue) + ";")
		    .collect(Collectors.joining("\n"));

		dumpCode += "\n__fastest_dump($state);";

		return dumpCode;
	}

	@Override
	public String generate(APLStmt aplStmt) {
		if (aplStmt instanceof AssignStmt) {
			return generate((AssignStmt) aplStmt);
		} else if (aplStmt instanceof CallExpr) {
			return generate((CallExpr) aplStmt) + ";";
		} else if (aplStmt instanceof SetterCallStmt) {
			return generate((SetterCallStmt) aplStmt);
		} else if (aplStmt instanceof ConstructorCallStmt) {
			return generate((ConstructorCallStmt) aplStmt);
		} else if (aplStmt instanceof RecordStmt) {
			return generate((RecordStmt) aplStmt);
		} else if (aplStmt instanceof ArrayDeclStmt) {
			// Perl does not require array declaration,
			// but it initialize the array to an empty array
			// for simplicity
			return generate((ArrayDeclStmt) aplStmt);
		} else {
			throw new RuntimeException("Unsupported APL statement.");
		}
	}

	private String generate(AssignStmt assignStmt) {
		String  lvalueStr = PerlGen.generate(assignStmt.getLvalue());
		String  exprStr   = null;
		APLExpr expr      = assignStmt.getExpr();
		if (expr instanceof APLVar) {
			exprStr = PerlGen.generate((APLVar) expr);
		} else if (expr instanceof APLArray) {
			exprStr = PerlGen.generate((APLArray) expr);
		} else if (expr instanceof CallExpr) {
			exprStr = PerlGen.generate((CallExpr) expr);
		} else if (expr instanceof ConsExpr) {
			exprStr = PerlGen.generate((ConsExpr) expr);
		} else if (expr instanceof LongExpr) {
			exprStr = PerlGen.generate((LongExpr) expr);
		} else if (expr instanceof APLExprList) {
			exprStr = PerlGen.generate((APLExprList) expr);
		} else if (expr instanceof StringExpr) {
			exprStr = PerlGen.generate((StringExpr) expr);
		} else {
			throw new RuntimeException("Unsupported expression in APL statement.");
		}

		return lvalueStr + " = " + exprStr + ";";
	}

	private static String generate(APLLValue lvalue) {
		String result = "";
		if (lvalue instanceof APLVar) {
			result = "$" + lvalue.getName();
		} else if (lvalue instanceof APLArray) {
			if (((APLArray) lvalue).useAsReference()) {
				result = "\\@" + lvalue.getName();
			} else {
				result = "@" + lvalue.getName();
			}
		} else if (lvalue instanceof APLArray.APLArrayIndex) {
			APLArray.APLArrayIndex varIdx = (APLArray.APLArrayIndex) lvalue;
			result = "$" + varIdx.getParent().getName() + "[" + varIdx.getIndex() + "]";
		} else {
			throw new RuntimeException("Unsupported APL lvalue type:" + lvalue);
		}
		return result;
	}

	private static String generate(StringExpr stringExpr) {
		return "\"" + stringExpr.getValue() + "\"";
	}

	private static String generate(LongExpr longExpr) {
		return Long.toString(longExpr.getValue());
	}

	private static String generate(ConsExpr consExpr) {
		return consExpr.getValue();
	}

	private static String generate(APLExprList listExpr) {
		String result = "(" + listExpr.getAplExprList().stream().map(APLExpr::toString).collect(Collectors.joining(",")) + ")";
		return result;
	}

	private static String generate(String s) {
		return "$" + s;
	}

	private static String generate(CallExpr callExpr) {
		return callExpr.getFunName() + "(" +
		       callExpr.getArgs().stream().map(PerlGen::generate).collect(Collectors.joining(",")) +
		       ")";
	}

	private static String generate(ArrayDeclStmt arrayDeclStmt) {
		String arrayName = arrayDeclStmt.getName();
		return "@" + arrayName + " = ();";
	}

	private static String generate(RecordStmt recordStmt) {
		RecordType             recordType            = (RecordType) recordStmt.getLvalue().getType();
		List<String>           members               = recordType.getMembers();
		List<APLLValue>        modifiedFields        = getModifiedMembers(recordType.getMembersMap(), recordStmt.getFields());
		Map<String, APLLValue> modifiedFieldByMember = new HashMap<>();
		for (int i = 0; i < members.size(); i++) {
			modifiedFieldByMember.put(members.get(i), modifiedFields.get(i));
		}
		String recordName         = generate(recordStmt.getLvalue());
		String fieldConcatenation = members.stream().map(f -> "\"" + f + "\" => " + generate(modifiedFieldByMember.get(f)))
		    .collect(Collectors.joining(", "));
		return (recordName + " = {" + fieldConcatenation + "};");
	}

	private static String generate(SetterCallStmt setterCallStmt) {
		APLLValue    setterLValue = setterCallStmt.getLvalue();
		ContractType contractType = (ContractType) setterLValue.getType();
		String       setterMethod = contractType.getSetter();
		String       lvalueName   = setterLValue.getName();
		if (contractType.getConstructor().equals("HASH")) {
			return generate(setterCallStmt.getLvalue().getName()) + "->{" +
			       generate(setterCallStmt.getExprs().get(0)) +
			       "} = " +
			       generate(setterCallStmt.getExprs().get(1)) +
			       ";";
		} else {
			List<APLLValue> modifiedSetterArgs = getModifiedMembers(contractType.getSetterArgs(), setterCallStmt.getExprs());

			String setterCall = setterMethod + "(" +
			                    modifiedSetterArgs.stream().map(PerlGen::generate).collect(Collectors.joining(",")) +
			                    ")";
			return "$" + lvalueName + "->" + setterCall + ";";
		}
	}

	private static String generate(ConstructorCallStmt constructorCallStmt) {
		APLLValue    constructorLValue = constructorCallStmt.getLvalue();
		ContractType contractType      = (ContractType) constructorLValue.getType();
		if (contractType.getConstructor().equals("HASH"))
			return "";

		String module       = contractType.getModule();
		String modulePrefix = "";
		if (module != null) {
			modulePrefix = module + "->";
		}

		List<APLLValue> modifiedConstructorArgs = getModifiedMembers(contractType.getConstArgs(), constructorCallStmt.getExprs());

		String constructor     = contractType.getConstructor();
		String constructorCall = modulePrefix + constructor +
		                         "(" +
		                         modifiedConstructorArgs.stream().map(PerlGen::generate).collect(Collectors.joining(",")) +
		                         ")";
		return "$" + constructorLValue.getName() + " = " + constructorCall + ";";
	}

	private static List<APLLValue> getModifiedMembers(Map<String, ATCALType> argumentsMap, List<String> stmtArguments) {
		int             index             = 0;
		List<APLLValue> modifiedArguments = new ArrayList<>();
		ATCALType       memberType;
		APLLValue       modifiedMemberVar;
		String          modifiedMemberName;
		// It's considered that the constructorCallStmt arguments' order is the
		// same as the corresponding constructor's arguments
		for (String setterArg : argumentsMap.keySet()) {
			memberType         = argumentsMap.get(setterArg);
			modifiedMemberName = stmtArguments.get(index);
			if (memberType instanceof ArrayType) {
				modifiedMemberVar = new APLArray(modifiedMemberName, (ArrayType) memberType, true);
			} else {
				modifiedMemberVar = new APLVar(modifiedMemberName, memberType);
			}
			modifiedArguments.add(modifiedMemberVar);
			index++;
		}
		return modifiedArguments;
	}
}
