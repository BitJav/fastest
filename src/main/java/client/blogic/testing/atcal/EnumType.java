package client.blogic.testing.atcal;

import java.util.Collection;
import java.util.LinkedHashMap;

import com.google.common.collect.Maps;
import com.google.common.collect.Range;

import client.blogic.testing.atcal.apl.expressions.ConsExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprNum;

/**
 * Created by Cristian on 05/05/15.
 */
public class EnumType extends ATCALType {

	private final String                          name;
	private final LinkedHashMap<String, ConsExpr> elements;

	public EnumType(String name, Collection<String> elements) {
		this.name     = name;
		this.elements = Maps.newLinkedHashMap();
		for (String e : elements)
			this.elements.put(e, new ConsExpr(e));
	}

	public ConsExpr getElemByName(String e) {
		return elements.get(e);
	}

	public ConsExpr getElemByIndex(int index) {
		return elements.values().toArray(new ConsExpr[elements.values().size()])[index];
	}

	public int getElemCount() {
		return elements.size();
	}

	// Conversion to enum constants from Z types without bijection maps.
	@Override
	public ConsExpr fromZExpr(ZExpr zExpr) {

		// Convert from Z numeric expression
		if (zExpr instanceof ZExprNum) {
			ZExprNum zExprNum = (ZExprNum) zExpr;

			// Check if the numeric expression value is between the bounds of the array.
			int value = (int) zExprNum.getNum();     // TODO: what happens if the value is beyond the int range?
			if (!Range.closed(0, getElemCount()).contains(value))
				throw new RuntimeException("Z integer value is outside ATCAL enumeration range.");

			return getElemByIndex(value);

			// Convert from Z constant value
		} else if (zExpr instanceof ZExprAuto) {
			// AUTOFILL expressions get the first value in the enumeration
			return elements.values().iterator().next();
		}

		// Unsupported conversion
		throw new RuntimeException("Unsupported operation.");
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		boolean result = false;
		if (expr instanceof ZExprNum || expr instanceof ZExprAuto) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public String typeCategory() {
		return "Enum";
	}

	@Override
	public boolean contains(String member) {
		return false;
	}

	@Override
	public String toString() {
		return "Enum " + name + "(" + elements.values() + ")";
	}

	@Override
	public int hashCode() {
		final int prime  = 31;
		int       result = 1;
		result = prime * result + ((elements == null) ? 0 : elements.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnumType other = (EnumType) obj;
		if (elements == null) {
			if (other.elements != null)
				return false;
		} else if (!elements.equals(other.elements))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
}
