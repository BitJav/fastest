package client.blogic.testing.atcal;

import client.blogic.testing.atcal.apl.APLLValue;

public class OuterAPLScopeInfo {
	/**
	 * Used to know the iteration counter
	 */
	private Integer   counter;
	/**
	 * Nesting depth
	 */
	private Integer   depth;
	/**
	 * Array size if {@link #aplScope} is an array, 0 otherwise
	 */
	private Integer   arraySize;
	/**
	 * Above level ALP scope
	 */
	private APLLValue aplScope;
	/**
	 * ALP scope above of {@link #aplScope}
	 */
	private APLLValue upperAPLScope;
	/**
	 * If {@link #aplScope}'s type is {@link ContractType}, this flag tells if
	 * it is refining a set or a list
	 */
	private boolean   isContractRefiningSetOrList;

	public OuterAPLScopeInfo(APLLValue aplScope,
	                         APLLValue upperAPLScope,
	                         int counter,
	                         int depth,
	                         int arraySize,
	                         boolean isContractRefiningSetOrList) {
		this.counter                     = counter;
		this.depth                       = depth;
		this.arraySize                   = arraySize;
		this.aplScope                    = aplScope;
		this.upperAPLScope               = upperAPLScope;
		this.isContractRefiningSetOrList = isContractRefiningSetOrList;
	}
	
	public OuterAPLScopeInfo(APLLValue aplScope,
	                         boolean isContractRefiningSetOrList,
	                         OuterAPLScopeInfo outerAPLScopeInfo) {
		boolean   existOuterAPLScope = outerAPLScopeInfo != null ? true : false;
		int       counter            = existOuterAPLScope ? outerAPLScopeInfo.getCounter() : 0;
		int       depth              = existOuterAPLScope ? outerAPLScopeInfo.getDepth() + 1 : 1;
		APLLValue upperAPLScope      = existOuterAPLScope ? outerAPLScopeInfo.getAplScope() : null;
		this.counter                     = counter;
		this.depth                       = depth;
		this.arraySize                   = 0;
		this.aplScope                    = aplScope;
		this.upperAPLScope               = upperAPLScope;
		this.isContractRefiningSetOrList = isContractRefiningSetOrList;
	}
	
	
	public OuterAPLScopeInfo(OuterAPLScopeInfo outerAPLScopeInfo) {
		this.counter                     = outerAPLScopeInfo.counter;
		this.depth                       = outerAPLScopeInfo.depth;
		this.arraySize                   = outerAPLScopeInfo.arraySize;
		this.aplScope                    = outerAPLScopeInfo.aplScope;
		this.upperAPLScope               = outerAPLScopeInfo.upperAPLScope;
		this.isContractRefiningSetOrList = outerAPLScopeInfo.isContractRefiningSetOrList;
	}

	public String getName() {
		return aplScope.getName();
	}

	public Integer getCounter() {
		return counter;
	}

	public Integer getDepth() {
		return depth;
	}

	public Integer getArraySize() {
		return arraySize;
	}

	public APLLValue getAplScope() {
		return aplScope;
	}

	public APLLValue getUpperAPLScope() {
		return upperAPLScope;
	}

	public boolean isContractRefiningSetOrList() {
		return isContractRefiningSetOrList;
	}

	public void setAplScope(APLLValue aplScope) {
		this.aplScope = aplScope;
	}

	public void setUpperAPLScope(APLLValue upperAPLScope) {
		this.upperAPLScope = upperAPLScope;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}

	public void setArraySize(Integer arraySize) {
		this.arraySize = arraySize;
	}

	public void incrementCounter() {
		counter++;
	}

	public void setContractRefiningSetOrList(boolean isContractRefiningSetOrList) {
		this.isContractRefiningSetOrList = isContractRefiningSetOrList;
	}

	@Override
	public String toString() {
		return "[upperAPLScope = " + upperAPLScope +
		       ",\naplScope = " +
		       aplScope +
		       ",\ncounter = " +
		       counter +
		       ",\ndepth = " +
		       depth +
		       ",\narray size = " +
		       arraySize +
		       ",\nisContractRefiningSetOrList = " +
		       isContractRefiningSetOrList +
		       "]";
	}
}
