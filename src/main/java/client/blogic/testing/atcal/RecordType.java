package client.blogic.testing.atcal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.apl.expressions.RecordExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprProd;

/**
 * Represents a record type. It's implemented as a sorted map because it's required
 * when a Z product is refined as a record. For example:
 * zProdVar ==> record1 AS RECORD Pair (fst : STRING, snd: INT)
 * and in an ATC zProdVar = <4,33>
 * then, what is meant to do is to map 4 to record1.fst and 33 to record1.snd
 * 
 * Created by Cristian on 06/05/15.
 */
public class RecordType extends ATCALType {

	private final LinkedHashMap<String, ATCALType> fields;

	public RecordType(LinkedHashMap<String, ATCALType> fields) {
		this.fields = fields;
	}

	public Optional<ATCALType> getFieldType(String field) {
		return Optional.ofNullable(fields.get(field));
	}

	public List<String> getMembers() {
		List<String> members = new ArrayList<>();
		for (String member : fields.keySet()) {
			members.add(member);
		}
		return members;
	}

	public Map<String, ATCALType> getMembersMap() {
		return fields;
	}

	@Override
	public APLExpr fromZExpr(ZExpr expr) {
		if (expr instanceof ZExprProd) {
			ZExprProd                        prod                = (ZExprProd) expr;
			ImmutableList<ZExpr>             prodValues          = prod.getValues();
			LinkedHashMap<String, ATCALType> recordFieldsMapping = this.fields;
			if (prodValues.size() == recordFieldsMapping.size()) {
				LinkedHashMap<String, APLExpr> recordMap = new LinkedHashMap<>();
				Iterator<String>               iterator  = recordFieldsMapping.keySet().iterator();
				for (ZExpr zExpr : prodValues) {
					if (iterator.hasNext()) {
						String    member     = iterator.next();
						ATCALType memberType = recordFieldsMapping.get(member);
						APLExpr   aplExpr    = memberType.fromZExpr(zExpr);
						recordMap.put(member, aplExpr);
					}
				}
				return new RecordExpr(recordMap);
			} else {
				throw new RuntimeException("Z product expression " + prod +
				                           " and record type " +
				                           this +
				                           " have different quantity of members");
			}
		} else {
			throw new RuntimeException(expr + " must be a Z product expressions in order to be translated to record of type " +
			                           this);
		}
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		boolean result = false;
		if (expr instanceof ZExprProd) {
			ZExprProd                        prod                = (ZExprProd) expr;
			ImmutableList<ZExpr>             prodValues          = prod.getValues();
			LinkedHashMap<String, ATCALType> recordFieldsMapping = this.fields;
			if (prodValues.size() == recordFieldsMapping.size()) {
				Iterator<String> iterator = recordFieldsMapping.keySet().iterator();
				for (ZExpr zExpr : prodValues) {
					if (iterator.hasNext()) {
						String    member     = iterator.next();
						ATCALType memberType = recordFieldsMapping.get(member);
						if (!memberType.canImplementDirectly(zExpr)) {
							return false;
						}
					}
				}
				result = true;
			} else {
				result = false;
			}
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public String typeCategory() {
		return "Record";
	}

	@Override
	public boolean contains(String member) {
		return fields.containsKey(member);
	}

	@Override
	public String toString() {
		String resultString = "";
		resultString += "RecordType{\n";
		for (String field : fields.keySet()) {
			resultString += "\t" + field + " -> " + fields.get(field).toString() + ",\n";
		}
		resultString += "}";
		return resultString;
	}

	@Override
	public int hashCode() {
		final int prime  = 31;
		int       result = 1;
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecordType other = (RecordType) obj;
		if (fields == null) {
			if (other.fields != null)
				return false;
		} else if (!fields.equals(other.fields))
			return false;
		return true;
	}

}
