package client.blogic.testing.atcal;

import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;

/**
 * Created by Cristian on 4/21/15.
 */
public abstract class ATCALType {
	/**
	 * Convert a Z expression into an APL
	 *
	 * @param  expr The Z expression
	 * @return      An APL expression or a runtime exception if conversion is not supported.
	 */
	public APLExpr fromZExpr(ZExpr expr) {
		throw new RuntimeException("Unsupported operation.");
	}

	/**
	 * Tells if this ATCALType can implement {@code expr} directly (without a with-clause)
	 * 
	 * @param  expr Expression that might want to be translated
	 * @return
	 */
	public abstract boolean canImplementDirectly(ZExpr expr);

	/**
	 * Returns the category that this type represents
	 * 
	 * @return The category that this type represents
	 */
	public abstract String typeCategory();

	/**
	 * Tells if the current ATCAL type contains {@code member}
	 * 
	 * @param  member
	 * @return        True if {@code member} is in current type
	 */
	public abstract boolean contains(String member);

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);
	
	
}
