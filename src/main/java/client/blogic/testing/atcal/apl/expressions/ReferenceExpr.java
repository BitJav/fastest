package client.blogic.testing.atcal.apl.expressions;

import com.google.common.base.Objects;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.visitors.APLExprVisitor;

public class ReferenceExpr implements APLExpr {
	private final long value;

	public ReferenceExpr(long value) {
		this.value = value;
	}

	/**
	 * Get the value of the expression
	 * 
	 * @return the long value
	 */
	public long getValue() {
		return value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof LongExpr))
			return false;
		ReferenceExpr referenceExpr = (ReferenceExpr) o;
		return Objects.equal(value, referenceExpr.value);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(value);
	}

	@Override
	public String toString() {
		return "" + value;
	}

	@Override
	public ATCALType accept(APLExprVisitor<ATCALType> typeSolver) {
		return typeSolver.visit(this);
	}
}
