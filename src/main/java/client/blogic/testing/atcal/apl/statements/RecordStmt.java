package client.blogic.testing.atcal.apl.statements;

import java.util.List;
import java.util.stream.Collectors;

import client.blogic.testing.atcal.apl.APLLValue;

public class RecordStmt implements APLStmt {

	private final APLLValue    lvalue;
	private final List<String> fields;

	public RecordStmt(APLLValue lvalue, List<String> fields) {
		this.lvalue = lvalue;
		this.fields = fields;
	}

	/**
	 * Get the lvalue of the record assignment
	 * 
	 * @return the lvalue
	 */
	public APLLValue getLvalue() {
		return lvalue;
	}

	/**
	 * Get the expression of the assignment
	 * 
	 * @return the expression
	 */
	public List<String> getFields() {
		return fields;
	}

	@Override
	public String toString() {
		return lvalue.toString() + "= (" + fields.stream().collect(Collectors.joining(", ")) + ")";
	}
}
