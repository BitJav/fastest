package client.blogic.testing.atcal.apl.expressions;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.visitors.APLExprVisitor;
import client.blogic.testing.atcal.visitors.TypeSolverVisitor;

/**
 * Represents an APL record
 * 
 * @author Javier Bonet on 18/10/2019
 * 
 */
public class RecordExpr implements APLExpr {

	private LinkedHashMap<String, APLExpr>   recordMap;
	private LinkedHashMap<String, ATCALType> types;

	public RecordExpr(LinkedHashMap<String, APLExpr> recordMap) {
		this.recordMap = recordMap;
		setTypes(recordMap);
	}

	public LinkedHashMap<String, APLExpr> getRecordMap() {
		return recordMap;
	}

	private void setTypes(LinkedHashMap<String, APLExpr> recordMap) {
		types = new LinkedHashMap<>();
		for (Entry<String, APLExpr> recordEntry : recordMap.entrySet()) {
			String  memberName = recordEntry.getKey();
			APLExpr value      = recordEntry.getValue();
			types.put(memberName, getType(value));
		}
	}

	private ATCALType getType(APLExpr value) {
		APLExprVisitor<ATCALType> typeSolverVisitor = new TypeSolverVisitor();
		ATCALType                 type              = value.accept(typeSolverVisitor);
		return type;
	}

	@Override
	public ATCALType accept(APLExprVisitor<ATCALType> typeSolver) {
		return typeSolver.visit(this);
	}
}
