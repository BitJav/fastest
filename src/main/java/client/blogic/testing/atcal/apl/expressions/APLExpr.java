package client.blogic.testing.atcal.apl.expressions;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.visitors.APLExprVisitor;

/**
 * Created by Cristian on 4/20/15.
 */
public interface APLExpr {
	ATCALType accept(APLExprVisitor<ATCALType> typeSolver);
}
