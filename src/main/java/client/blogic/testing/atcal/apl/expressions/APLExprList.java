package client.blogic.testing.atcal.apl.expressions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.visitors.APLExprVisitor;

/**
 * This class must be used only when translating Z expressions to APL ones in the ArrayType's fromZExpr method
 * 
 * @author Javier Bonet on 13/10/2019
 */
public class APLExprList implements APLExpr, Iterable<APLExpr> {
	private List<APLExpr> aplExprList;
	/**
	 * Inner type
	 */
	private ATCALType     type;

	public APLExprList(ATCALType type) {
		this.aplExprList = new ArrayList<>();
		this.type        = type;
	}

	public List<APLExpr> getAplExprList() {
		return aplExprList;
	}

	public void setAplExprList(List<APLExpr> aplValuesList) {
		this.aplExprList = aplValuesList;
	}

	public ATCALType getType() {
		return type;
	}

	public void add(APLExpr expr) {
		this.aplExprList.add(expr);
	}

	@Override
	public Iterator<APLExpr> iterator() {
		return this.aplExprList.iterator();
	}

	@Override
	public ATCALType accept(APLExprVisitor<ATCALType> typeSolver) {
		return typeSolver.visit(this);
	}
}
