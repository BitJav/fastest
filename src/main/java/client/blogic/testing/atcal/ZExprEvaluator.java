package client.blogic.testing.atcal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import org.antlr.v4.runtime.misc.NotNull;

import com.google.common.base.CharMatcher;

import client.blogic.testing.atcal.exceptions.ElemOperatorMustBeSettedPreviouslyException;
import client.blogic.testing.atcal.exceptions.IndexedElementDoesntExistException;
import client.blogic.testing.atcal.exceptions.TryingToRefineBasicStructureWithElemOperatorException;
import client.blogic.testing.atcal.exceptions.WrongUseOfElemOperatorException;
import client.blogic.testing.atcal.exceptions.WrongZExprTypeException;
import client.blogic.testing.atcal.exceptions.ZExprMustBeSetOrListException;
import client.blogic.testing.atcal.exceptions.ZVariableNotFoundInAbstractTestCaseException;
import client.blogic.testing.atcal.helpers.RefinementHelper;
import client.blogic.testing.atcal.parser.AtcalBaseVisitor;
import client.blogic.testing.atcal.parser.AtcalParser;
import client.blogic.testing.atcal.parser.AtcalParser.SetDomContext;
import client.blogic.testing.atcal.parser.AtcalParser.SetRanContext;
import client.blogic.testing.atcal.parser.AtcalParser.ZExprContext;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprConst;
import client.blogic.testing.atcal.z.ast.ZExprList;
import client.blogic.testing.atcal.z.ast.ZExprNum;
import client.blogic.testing.atcal.z.ast.ZExprProd;
import client.blogic.testing.atcal.z.ast.ZExprSchema;
import client.blogic.testing.atcal.z.ast.ZExprSet;
import client.blogic.testing.atcal.z.ast.ZExprSetOrList;
import client.blogic.testing.atcal.z.ast.ZExprString;
import client.blogic.testing.atcal.z.ast.ZVar;

/**
 * Created by Cristian on 3/31/15.
 */
public class ZExprEvaluator extends AtcalBaseVisitor<ZExpr> {

	/**
	 * This scope should only be modified when processing zExprs of the form
	 * zExpr1 ==> zExpr2 ==> ...
	 * So, in this case the scope would start having zExpr1 and then zExpr2
	 * and the rest will be added when being processed
	 */
	private ZExprSchema scope;

	private Map<String, Integer> scopeNameToCurrentPositionMap;

	private boolean refiningSetCardinality;

	private boolean allValuesRequired = false;

	private Map<String, ZExpr> processedElements;

	private final String ELEMENT = "@ELEM";

	public ZExprEvaluator(ZExprSchema scope) {
		this.scope                         = scope;
		this.scopeNameToCurrentPositionMap = new HashMap<>();
		this.refiningSetCardinality        = false;
		this.processedElements             = new HashMap<>();
	}

	public ZExprEvaluator(ZExprSchema scope,
	                      Map<String, Integer> scopeNameToCurrentPositionMap,
	                      Map<String, ZExpr> processedElements) {
		this.scope                         = scope;
		this.scopeNameToCurrentPositionMap = scopeNameToCurrentPositionMap;
		this.refiningSetCardinality        = false;
		this.processedElements             = processedElements;
	}

	@Override
	public ZExpr visitZExprs(@NotNull AtcalParser.ZExprsContext ctx) {
		if (ctx.zExpr().getText().equals(ELEMENT)) {
			allValuesRequired = true;
		}

		ZExpr zExpr = visit(ctx.zExpr());
		// if there is no more nested Z expressions return the evaluation result, otherwise
		// create a nested scope and recursively evaluate the nested Z expressions.
		if (ctx.zExprs() == null) {
			return zExpr;
		} else {
			ZExprSchema    newZScope    = ZExprSchema.add(scope, new ZVar(RefinementHelper.zScopeVarName, zExpr));
			ZExprEvaluator newZExprEval = new ZExprEvaluator(newZScope, scopeNameToCurrentPositionMap, processedElements);
			return newZExprEval.visit(ctx.zExprs());
		}
	}

	@Override
	public ZExpr visitIdent(@NotNull AtcalParser.IdentContext ctx) {
		Optional<ZVar> var = scope.getVar(ctx.ID().getText());
		if (var.isPresent()) {
			return var.get().getValue();
		} else {
			throw new ZVariableNotFoundInAbstractTestCaseException(ctx.ID().getText());
		}
	}

	@Override
	public ZExprNum visitNumLiteral(@NotNull AtcalParser.NumLiteralContext ctx) {
		return new ZExprNum(Long.parseLong(ctx.NUMBER().getText()));
	}

	@Override
	public ZExprString visitStrLiteral(@NotNull AtcalParser.StrLiteralContext ctx) {
		return new ZExprString(CharMatcher.is('\"').trimFrom(ctx.STRING().getText()));
	}

	@Override
	public ZExpr visitAutoExpr(@NotNull AtcalParser.AutoExprContext ctx) {
		return new ZExprAuto();
	}

	@Override
	public ZExpr visitElemExpr(@NotNull AtcalParser.ElemExprContext ctx) {
		ZExpr zScope = null;

		if (allValuesRequired) {
			if (processedElements.get(ELEMENT) == null) {
				// This case could happen only at the beginning of the process
				zScope = scope.getVar(RefinementHelper.zScopeVarName).get().getValue();
			} else {
				ZExpr elementExpr = processedElements.get(ELEMENT);
				if (elementExpr instanceof ZExprSetOrList) {
					ZExprSetOrList setOrList = (ZExprSetOrList) elementExpr;
					zScope = new ZExprSet(setOrList.iterator());
				} else {
					throw new WrongUseOfElemOperatorException(ctx.getText());
				}
			}
			if (!(zScope instanceof ZExprSet)) {
				throw new WrongUseOfElemOperatorException(zScope.toString());
			}
			// @ELEM operator should be only used whit non-basic inner elements
			// or when refining a set cardinality
			ZExprSet set = (ZExprSet) zScope;
			if (set.getCard() != 0 && isBasicSet(set) && !refiningSetCardinality) {
				throw new TryingToRefineBasicStructureWithElemOperatorException(set.toString());
			}
			String contextNamePrefix;
			String contextName = zScope.getContextName();
			if (contextName != "") {
				contextNamePrefix = zScope.getContextName() + ".";
			} else {
				contextNamePrefix = "";
			}
			ZExprList zList = new ZExprList(set, contextNamePrefix + ctx.getText());
			refiningSetCardinality = false;
			return zList;
		} else {
			// This case represents the use of @ELEM operator along
			// with other operator. For example: @ELEM.#1
			ZExpr elemExpr = processedElements.get(ELEMENT);
			zScope = elemExpr;
			if (!(zScope instanceof ZExprProd)) {
				String scopeName = zScope != null ? zScope.toString() : ctx.getText();
				throw new WrongUseOfElemOperatorException(scopeName);
			}

			return zScope;
		}
	}

	@Override
	public ZExpr visitGroup(@NotNull AtcalParser.GroupContext ctx) {
		return visit(ctx.zExpr());
	}

	// Cartessian product operations
	@Override
	public ZExpr visitProdProj(@NotNull AtcalParser.ProdProjContext ctx) {
		ZExprContext zExprContext = ctx.zExpr();
		ZExpr        zExpr;
		if (processedElements.containsKey(zExprContext.getText()) && isDomOrRanExpression(zExprContext)) {
			String zExprText = zExprContext.getText();
			zExpr = getElementByIndex(processedElements.get(zExprText), scopeNameToCurrentPositionMap.get(zExprText));
		} else {
			zExpr = visit(zExprContext);
		}
		if (!(zExpr instanceof ZExprProd)) {
			throw new WrongZExprTypeException(ctx.zExpr().getText(), "product");
		}
		ZExprProd prod = (ZExprProd) zExpr;
		return prod.getValue(Integer.valueOf(ctx.TUPPROJ().getText().substring(1)) - 1);
	}

	@Override
	public ZExpr visitProdCons(@NotNull AtcalParser.ProdConsContext ctx) {
		ArrayList<ZExpr> exprs = new ArrayList<ZExpr>(ctx.zExpr().size());
		for (AtcalParser.ZExprContext context : ctx.zExpr()) {
			exprs.add(visit(context));
		}
		return new ZExprProd(exprs.toArray(new ZExpr[] {}));
	}

	// Set operations
	@Override
	public ZExpr visitSetDom(@NotNull AtcalParser.SetDomContext ctx) {
		allValuesRequired = true;
		ZExpr zExpr = visit(ctx.zExpr());
		ZExpr expr  = zExpr;
		if (expr instanceof ZExprList) {
			ZExprList listExpr = (ZExprList) expr;
			expr = new ZExprSet(listExpr.iterator());
		}

		if (!(expr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr().getText(), "set");
		}
		ZExprSet set     = (ZExprSet) expr;
		int      setCard = set.getCard();
		if (setCard > 0 && !(set.get(0) instanceof ZExprProd)) {
			throw new WrongZExprTypeException(set.get(0).toString(), "product");
		}
		ArrayList<ZExpr> setDom = new ArrayList<ZExpr>();
		for (ZExpr elem : set) {
			setDom.add(((ZExprProd) elem).getValue(0));
		}
		return new ZExprSet(setDom, ctx.getText());
	}

	@Override
	public ZExpr visitSetDiff(@NotNull AtcalParser.SetDiffContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "set");
		}
		if (!(rightZExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "set");
		}
		ZExprSet leftOp  = (ZExprSet) leftZExpr;
		ZExprSet rightOp = (ZExprSet) rightZExpr;
		return leftOp.difference(rightOp);
	}

	@Override
	public ZExpr visitSetInter(@NotNull AtcalParser.SetInterContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "set");
		}
		if (!(rightZExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "set");
		}
		ZExprSet leftOp  = (ZExprSet) leftZExpr;
		ZExprSet rightOp = (ZExprSet) rightZExpr;
		return leftOp.intersection(rightOp);
	}

	@Override
	public ZExpr visitSetRan(@NotNull AtcalParser.SetRanContext ctx) {
		allValuesRequired = true;
		ZExpr zExpr = visit(ctx.zExpr());
		ZExpr expr  = zExpr;
		if (expr instanceof ZExprList) {
			ZExprList listExpr = (ZExprList) expr;
			expr = new ZExprSet(listExpr.iterator());
		}

		if (!(expr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr().getText(), "set");
		}
		ZExprSet set     = (ZExprSet) expr;
		int      setCard = set.getCard();
		if (setCard > 0 && !(set.get(0) instanceof ZExprProd)) {
			throw new WrongZExprTypeException(set.get(0).toString(), "product");
		}
		ArrayList<ZExpr> setRan = new ArrayList<ZExpr>();
		for (ZExpr elem : set) {
			setRan.add(((ZExprProd) elem).getValue(1));
		}
		return new ZExprSet(setRan, ctx.getText());
	}

	@Override
	public ZExpr visitSetProj(@NotNull AtcalParser.SetProjContext ctx) {
		allValuesRequired = false;
		ZExpr zExpr = visit(ctx.zExpr());
		if (!(zExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr().getText(), "set");
		}
		ZExprSet set     = (ZExprSet) zExpr;
		int      setCard = set.getCard();
		if (setCard > 0 && !(set.get(0) instanceof ZExprProd)) {
			throw new WrongZExprTypeException(set.get(0).toString(), "product");
		}
		ArrayList<ZExpr> setProj = new ArrayList<ZExpr>();
		for (ZExpr elem : set) {
			setProj.add(((ZExprProd) elem).getValue(Integer.valueOf(ctx.PROJ().getText().substring(1)) - 1));
		}
		return new ZExprSet(setProj, ctx.getText());
	}

	@Override
	public ZExpr visitSetCons(@NotNull AtcalParser.SetConsContext ctx) {
		ArrayList<ZExpr> exprs = new ArrayList<ZExpr>(ctx.zExpr().size());
		for (AtcalParser.ZExprContext context : ctx.zExpr()) {
			exprs.add(visit(context));
		}
		return new ZExprSet(exprs);
	}

	@Override
	public ZExpr visitSetUnion(@NotNull AtcalParser.SetUnionContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "set");
		}
		if (!(rightZExpr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "set");
		}
		ZExprSet leftOp  = (ZExprSet) leftZExpr;
		ZExprSet rightOp = (ZExprSet) rightZExpr;
		return leftOp.union(rightOp);
	}

	// Numerical operations

	@Override
	public ZExprNum visitNumMod(@NotNull AtcalParser.NumModContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "number");
		}
		if (!(rightZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "number");
		}
		ZExprNum leftOp  = (ZExprNum) leftZExpr;
		ZExprNum rightOp = (ZExprNum) rightZExpr;
		return new ZExprNum(leftOp.getNum() % rightOp.getNum());
	}

	@Override
	public ZExpr visitNumMul(@NotNull AtcalParser.NumMulContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "number");
		}
		if (!(rightZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "number");
		}
		ZExprNum leftOp  = (ZExprNum) leftZExpr;
		ZExprNum rightOp = (ZExprNum) rightZExpr;
		return new ZExprNum(leftOp.getNum() * rightOp.getNum());
	}

	@Override
	public ZExpr visitNumPlus(@NotNull AtcalParser.NumPlusContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "number");
		}
		if (!(rightZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "number");
		}
		ZExprNum leftOp  = (ZExprNum) leftZExpr;
		ZExprNum rightOp = (ZExprNum) rightZExpr;
		return new ZExprNum(leftOp.getNum() + rightOp.getNum());
	}

	@Override
	public ZExpr visitNumDiv(@NotNull AtcalParser.NumDivContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "number");
		}
		if (!(rightZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "number");
		}
		ZExprNum leftOp  = (ZExprNum) leftZExpr;
		ZExprNum rightOp = (ZExprNum) rightZExpr;
		return new ZExprNum(leftOp.getNum() / rightOp.getNum());
	}

	@Override
	public ZExpr visitNumMinus(@NotNull AtcalParser.NumMinusContext ctx) {
		ZExpr leftZExpr  = visit(ctx.zExpr(0));
		ZExpr rightZExpr = visit(ctx.zExpr(1));
		if (!(leftZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(0).getText(), "number");
		}
		if (!(rightZExpr instanceof ZExprNum)) {
			throw new WrongZExprTypeException(ctx.zExpr(1).getText(), "number");
		}
		ZExprNum leftOp  = (ZExprNum) leftZExpr;
		ZExprNum rightOp = (ZExprNum) rightZExpr;
		return new ZExprNum(leftOp.getNum() - rightOp.getNum());
	}

	@Override
	public ZExprNum visitSetCard(@NotNull AtcalParser.SetCardContext ctx) {
		refiningSetCardinality = true;
		ZExpr zExpr = visit(ctx.zExpr());
		ZExpr expr  = zExpr;
		if (expr instanceof ZExprList) {
			ZExprList listExpr = (ZExprList) expr;
			expr = new ZExprSet(listExpr.iterator());
		}

		if (!(expr instanceof ZExprSet)) {
			throw new WrongZExprTypeException(ctx.zExpr().getText(), "set");
		}
		ZExprSet set = (ZExprSet) expr;
		return new ZExprNum(set.getCard());
	}

	// Strings operations
	@Override
	public ZExprString visitStrConcat(@NotNull AtcalParser.StrConcatContext ctx) {
		ZExprSchema newZScope;
		String      leftOpStr   = ZExprString.fromZExpr(visit(ctx.zExpr(0))).getStr();
		String      rightOpStr  = ZExprString.fromZExpr(visit(ctx.zExpr(1))).getStr();
		String      leftOpName  = ctx.zExpr(0).getText();
		String      rightOpName = ctx.zExpr(1).getText();
		newZScope = ZExprSchema.add(scope, new ZVar(leftOpName, new ZExprString(leftOpStr)));
		newZScope = ZExprSchema.add(newZScope, new ZVar(rightOpName, new ZExprString(rightOpStr)));
		scope     = newZScope;
		return new ZExprString(leftOpStr + rightOpStr);
	}

	private ZExpr getElementByIndex(ZExpr zExpr, int currentElementIndex) {
		ZExpr           resultZExpr = null;
		int             counter     = 0;
		Iterator<ZExpr> iterator;
		ZExpr           element;
		if (zExpr instanceof ZExprList || zExpr instanceof ZExprSet) {
			iterator = RefinementHelper.getIterator(zExpr);
			while (iterator.hasNext()) {
				element = iterator.next();
				if (counter == currentElementIndex) {
					resultZExpr = element;
					break;
				}
				counter++;
			}
		} else {
			throw new ZExprMustBeSetOrListException(zExpr.toString());
		}

		if (resultZExpr == null) {
			throw new IndexedElementDoesntExistException(zExpr.toString(), currentElementIndex);
		}

		return resultZExpr;
	}

	private boolean isBasicSet(ZExprSet set) {
		boolean         result   = true;
		Iterator<ZExpr> iterator = set.iterator();
		if (iterator.hasNext()) {
			result = isBasicZExpr(iterator.next());
		}
		return result;
	}

	private boolean isBasicZExpr(ZExpr expr) {
		boolean result = false;
		if (expr instanceof ZExprAuto || expr instanceof ZExprConst || expr instanceof ZExprNum || expr instanceof ZExprString) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	private boolean isDomOrRanExpression(ZExprContext zExpr) {
		boolean result = false;
		if (zExpr instanceof SetDomContext || zExpr instanceof SetRanContext) {
			result = true;
		}
		return result;
	}
}
