package client.blogic.testing.atcal;

import java.util.List;
import java.util.Map;

import client.blogic.testing.atcal.parser.AtcalParser;
import client.blogic.testing.atcal.parser.AtcalParser.LawContext;

/**
 * Created by Cristian on 05/08/15.
 */
public class RefinementRule {

	private final AtcalParser.RefinementRuleContext context;      // The parsed rule context
	/**
	 * Holds the refinement laws that have an associated ID
	 */
	private Map<String, LawContext>                 lawsById;
	/**
	 * Holds the refinement laws that doesn't have an associated ID
	 */
	private List<LawContext>                        lawsWithoutId;

	public RefinementRule(AtcalParser.RefinementRuleContext context,
	                      Map<String, LawContext> lawsById,
	                      List<LawContext> lawsWithoutId) {
		this.context       = context;
		this.lawsById      = lawsById;
		this.lawsWithoutId = lawsWithoutId;
	}

	public AtcalParser.RefinementRuleContext getContext() {
		return context;
	}

	public Map<String, LawContext> getLawsById() {
		return lawsById;
	}

	public List<LawContext> getLawsWithoutId() {
		return lawsWithoutId;
	}
}
