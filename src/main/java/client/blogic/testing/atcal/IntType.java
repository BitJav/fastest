package client.blogic.testing.atcal;

import client.blogic.testing.atcal.apl.expressions.LongExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprNum;

/**
 * Created by Cristian on 06/05/15.
 */
public final class IntType extends ATCALType {

	private static final IntType INSTANCE = new IntType();

	private IntType() {
		if (INSTANCE != null) {
			throw new IllegalStateException("Already instantiated");
		}
	}

	public static IntType getInstance() {
		return INSTANCE;
	}

	// Conversion to integer expression
	@Override
	public LongExpr fromZExpr(ZExpr zExpr) {
		if (zExpr instanceof ZExprNum) {
			return new LongExpr(((ZExprNum) zExpr).getNum());
		} else if (zExpr instanceof ZExprAuto) {
			return new LongExpr(0xDEADBEEF);
		}
		// Unsupported conversion
		throw new RuntimeException("Unsupported operation.");
	}

	@Override
	public String toString() {
		return "Int";
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		boolean result = false;
		if (expr instanceof ZExprNum || expr instanceof ZExprAuto) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public boolean contains(String member) {
		return false;
	}

	@Override
	public String typeCategory() {
		return "Int";
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
