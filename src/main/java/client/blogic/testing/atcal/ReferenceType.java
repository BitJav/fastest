package client.blogic.testing.atcal;

import client.blogic.testing.atcal.apl.expressions.LongExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprNum;

public class ReferenceType extends ATCALType {

	@Override
	public LongExpr fromZExpr(ZExpr zExpr) {
		if (zExpr instanceof ZExprNum) {
			return new LongExpr(((ZExprNum) zExpr).getNum());
		} else if (zExpr instanceof ZExprAuto) {
			return new LongExpr(0xDEADBEEF);
		}
		// Unsupported conversion
		throw new RuntimeException("Unsupported operation.");
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		return true;
	}

	@Override
	public String typeCategory() {
		return "Reference";
	}

	@Override
	public boolean contains(String member) {
		return false;
	}

	@Override
	public String toString() {
		return "ReferenceType";
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
