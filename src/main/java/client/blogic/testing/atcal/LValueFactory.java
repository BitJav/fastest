package client.blogic.testing.atcal;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import client.blogic.testing.atcal.apl.APLLValue;
import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.helpers.RefinementHelper.ContextType;

/**
 * Created by Cristian on 25/06/15.
 */
public class LValueFactory {

	private final Map<String, APLLValue> lvalues            = Maps.newHashMap();
	private final Set<APLLValue>         effectiveVariables = new HashSet<>();

	public APLLValue newVar(String name, ATCALType type, boolean insideAnAPLScope, ContextType zScopeType) {
		APLLValue var;
		if (insideAnAPLScope) {
			var = new APLVar(name, type);
			lvalues.put(name, var);
		} else {
			if (lvalues.containsKey(name)) {
				var = lvalues.get(name);
			} else {
				var = new APLVar(name, type);
				lvalues.put(name, var);
			}
		}
		if (!insideAnAPLScope || zScopeType.equals(ContextType.SET_CARDINALITY)) {
			effectiveVariables.add(var);
		}
		return var;
	}

	public APLLValue newArray(String name, ATCALType type, boolean insideAnAPLScope) {
		APLLValue var;
		if (insideAnAPLScope) {
			var = new APLArray(name, (ArrayType) type);
			lvalues.put(name, var);
		} else {
			if (lvalues.containsKey(name)) {
				var = lvalues.get(name);
			} else {
				var = new APLArray(name, (ArrayType) type);
				lvalues.put(name, var);
			}
			effectiveVariables.add(var);
		}
		return var;
	}

	public APLLValue getLValue(String name) {
		return lvalues.get(name);
	}

	public Collection<APLLValue> getEffectiveVariables() {
		return effectiveVariables;
	}
}
