package client.blogic.testing.atcal;

import java.util.Iterator;

import client.blogic.testing.atcal.apl.expressions.StringExpr;
import client.blogic.testing.atcal.helpers.RefinementHelper;
import client.blogic.testing.atcal.helpers.TypeHelper;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprAuto;
import client.blogic.testing.atcal.z.ast.ZExprConst;
import client.blogic.testing.atcal.z.ast.ZExprList;
import client.blogic.testing.atcal.z.ast.ZExprNum;
import client.blogic.testing.atcal.z.ast.ZExprSet;
import client.blogic.testing.atcal.z.ast.ZExprString;

/**
 * Created by Cristian on 06/05/15.
 */
public class StringType extends ATCALType {

	// Conversion to string expression
	@Override
	public StringExpr fromZExpr(ZExpr zExpr) {
		if (zExpr instanceof ZExprNum) {
			return new StringExpr(String.valueOf(((ZExprNum) zExpr).getNum()));
		} else if (zExpr instanceof ZExprString) {
			return new StringExpr(((ZExprString) zExpr).getStr());
		} else if (zExpr instanceof ZExprConst) {
			return new StringExpr(((ZExprConst) zExpr).getValue());
		} else if (zExpr instanceof ZExprAuto) {
			return new StringExpr("AUTOFILL");
		} else if (zExpr instanceof ZExprSet) {
			ZExprSet        setExpr  = (ZExprSet) zExpr;
			Iterator<ZExpr> iterator = setExpr.iterator();
			return translateContainerToString(iterator);
		} else if (zExpr instanceof ZExprList) {
			ZExprList       setList  = (ZExprList) zExpr;
			Iterator<ZExpr> iterator = setList.iterator();
			return translateContainerToString(iterator);
		}
		// Unsupported conversion
		throw new RuntimeException("Unsupported operation.");
	}

	private StringExpr translateContainerToString(Iterator<ZExpr> iterator) {
		String result = "";
		switch (TypeHelper.getInnerAbstractType(iterator)) {
		case BASIC:
			while (iterator.hasNext()) {
				ZExpr element = iterator.next();
				result = result + element.toString();
			}
			break;
		case NO_ELEMENTS:
			break;
		default:
			throw new RuntimeException("Sets with non-basic elements cannot be translated");
		}
		return new StringExpr(result);
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		boolean result = false;
		if (expr instanceof ZExprNum || expr instanceof ZExprString || expr instanceof ZExprAuto) {
			result = true;
		} else if (expr instanceof ZExprSet || expr instanceof ZExprList) {
			Iterator<ZExpr> iterator = RefinementHelper.getIterator(expr);
			if (iterator.hasNext()) {
				ZExpr element = iterator.next();
				if (element instanceof ZExprNum || element instanceof ZExprString || element instanceof ZExprAuto) {
					result = true;
				}
			} else {
				// It's an empty set or list, I consider it's ok
				result = true;
			}
		}
		return result;
	}

	@Override
	public String typeCategory() {
		return "String";
	}

	@Override
	public boolean contains(String member) {
		return false;
	}

	@Override
	public String toString() {
		return "String";
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
