package client.blogic.testing.atcal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import client.blogic.testing.atcal.apl.APLLValue;
import client.blogic.testing.atcal.apl.APLVar;
import client.blogic.testing.atcal.apl.CodeBlock;
import client.blogic.testing.atcal.apl.expressions.APLArray;
import client.blogic.testing.atcal.apl.expressions.CallExpr;
import client.blogic.testing.atcal.apl.statements.ArrayDeclStmt;
import client.blogic.testing.atcal.apl.statements.AssignStmt;
import client.blogic.testing.atcal.apl.statements.ConstructorCallStmt;
import client.blogic.testing.atcal.apl.statements.RecordStmt;
import client.blogic.testing.atcal.apl.statements.SetterCallStmt;
import client.blogic.testing.atcal.exceptions.CheckedException;
import client.blogic.testing.atcal.exceptions.InexistingLawException;
import client.blogic.testing.atcal.exceptions.InexistingRefinemetRuleException;
import client.blogic.testing.atcal.exceptions.InnerArrayTypeMustBeEnumException;
import client.blogic.testing.atcal.exceptions.InnerContractTypeMusBeEnumException;
import client.blogic.testing.atcal.exceptions.LValueTypeDoesntMatchWithGivenTypeException;
import client.blogic.testing.atcal.exceptions.RefinementException;
import client.blogic.testing.atcal.exceptions.RefinementToNotImplementedTypeException;
import client.blogic.testing.atcal.exceptions.UUTArgumentNotRefinedPreviously;
import client.blogic.testing.atcal.exceptions.WrongNumberOfRefinementsInWithClauseException;
import client.blogic.testing.atcal.exceptions.ZExprMustBeConstantException;
import client.blogic.testing.atcal.exceptions.ZVariableNotFoundInAbstractTestCaseException;
import client.blogic.testing.atcal.helpers.MappingHelper;
import client.blogic.testing.atcal.helpers.RefinementHelper;
import client.blogic.testing.atcal.helpers.RefinementHelper.ContextType;
import client.blogic.testing.atcal.helpers.StatementsHelper;
import client.blogic.testing.atcal.helpers.VariableNamingHelper;
import client.blogic.testing.atcal.mappingInfo.ElementsInfo;
import client.blogic.testing.atcal.parser.AtcalBaseVisitor;
import client.blogic.testing.atcal.parser.AtcalParser.ArrayLValueContext;
import client.blogic.testing.atcal.parser.AtcalParser.BiRefLawContext;
import client.blogic.testing.atcal.parser.AtcalParser.ConstMappingContext;
import client.blogic.testing.atcal.parser.AtcalParser.FieldLValueContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawReferenceContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawRefinementContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawsContext;
import client.blogic.testing.atcal.parser.AtcalParser.RefinementContext;
import client.blogic.testing.atcal.parser.AtcalParser.TypeContext;
import client.blogic.testing.atcal.parser.AtcalParser.UUTNoRetValContext;
import client.blogic.testing.atcal.parser.AtcalParser.UUTRetValContext;
import client.blogic.testing.atcal.parser.AtcalParser.VarLValueContext;
import client.blogic.testing.atcal.parser.AtcalParser.WithRefContext;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprConst;
import client.blogic.testing.atcal.z.ast.ZExprList;
import client.blogic.testing.atcal.z.ast.ZExprSchema;
import client.blogic.testing.atcal.z.ast.ZExprSet;
import client.blogic.testing.atcal.z.ast.ZVar;

/**
 * Created by Cristian on 4/16/15.
 */
public class RefinementLawEvaluator extends AtcalBaseVisitor<CodeBlock> {

	/**
	 * Scope for z expressions
	 */
	private final ZExprSchema                 zScope;
	/**
	 * Scope for APL code
	 */
	private final APLLValue                   aplScope;
	/**
	 * Declared types
	 */
	private final Map<String, ATCALType>      types;
	/**
	 * LValue manager
	 */
	private LValueFactory                     lValueFactory;
	/**
	 * The constant mapper manages the mappings from Z constants to APL expressions
	 */
	private final Map<String, ConstantMapper> zVarConstantMaps;
	private final BiMap<ZVar, String>         zVarToImpVarName;
	/**
	 * Holds the structures needed to be filled while processing. The information in
	 * this structures is then used to generate the corresponding statements
	 */
	private ElementsInfo                      elementsInfo;
	/**
	 * Used to identify the Z scope that it's currently being refined
	 */
	private RefinementHelper.ContextType      zScopeType;
	/**
	 * This variable should only be used when processing elements inside a ZScope.
	 * Holds the index of the ZScope element currently being processed
	 */
	private Map<String, Integer>              scopeNameToCurrentPositionMap;
	/**
	 * Used when processing elements inside an APL scope,
	 * in other words, when processing a WITH clause
	 */
	private OuterAPLScopeInfo                 outerAPLScopeInfo;
	/**
	 * True if the APL scope is of contract type and is refining a Set/List ZScope
	 */
	private boolean                           contractRefiningSetOrList;
	/**
	 * True if the APL scope is of array type and the upper APL scope is of array type
	 */
	private boolean                           refiningArrayInsideArray;

	/**
	 * Map that holds the list of values for each lawRefinement processed
	 * The key is the specification variable name or operators like @ELEM,
	 * @DOM, @ELEM.@RAN, etc.
	 */
	private Map<String, ZExpr> processedElements;

	public RefinementLawEvaluator(ZExprSchema zScope,
	                              APLLValue aplScope,
	                              Map<String, ATCALType> types,
	                              LValueFactory lValueFactory,
	                              Map<String, ConstantMapper> zVarConstantMaps,
	                              BiMap<ZVar, String> zVarToImpVarName,
	                              ElementsInfo elementsInfo,
	                              Map<String, ZExpr> processedElements) {
		this.zScope                        = zScope;
		this.aplScope                      = aplScope;
		this.types                         = types;
		this.lValueFactory                 = lValueFactory;
		this.zVarConstantMaps              = zVarConstantMaps;
		this.zVarToImpVarName              = zVarToImpVarName;
		this.elementsInfo                  = elementsInfo;
		this.zScopeType                    = ContextType.NON_SPECIAL_CASE;
		this.scopeNameToCurrentPositionMap = new HashMap<>();
		this.outerAPLScopeInfo             = null;
		this.contractRefiningSetOrList     = false;
		this.refiningArrayInsideArray      = false;
		this.processedElements             = processedElements;
	}

	public RefinementLawEvaluator(ZExprSchema zScope,
	                              APLLValue aplScope,
	                              Map<String, ATCALType> types,
	                              LValueFactory lValueFactory,
	                              Map<String, ConstantMapper> zVarConstantMaps,
	                              BiMap<ZVar, String> zVarToImpVarName,
	                              ElementsInfo elementsInfo,
	                              boolean contractRefiningSetOrList,
	                              Map<String, ZExpr> processedElements) {
		this.zScope                        = zScope;
		this.aplScope                      = aplScope;
		this.types                         = types;
		this.lValueFactory                 = lValueFactory;
		this.zVarConstantMaps              = zVarConstantMaps;
		this.zVarToImpVarName              = zVarToImpVarName;
		this.elementsInfo                  = elementsInfo;
		this.zScopeType                    = ContextType.NON_SPECIAL_CASE;
		this.scopeNameToCurrentPositionMap = new HashMap<>();
		this.outerAPLScopeInfo             = null;
		this.contractRefiningSetOrList     = contractRefiningSetOrList;
		this.refiningArrayInsideArray      = false;
		this.processedElements             = processedElements;
	}

	@Override
	public CodeBlock visitLaws(@NotNull LawsContext ctx) {
		CodeBlock codeBlock = new CodeBlock();
		for (LawContext lawCtx : ctx.law()) {
			codeBlock.join(processLawContext(lawCtx));
			// Reset the evaluator's state in order to avoid repeating
			// previous assignments
			resetState();
		}
		return codeBlock;
	}

	
	private CodeBlock processLawContext(LawContext lawCtx) {
		CodeBlock codeBlock = new CodeBlock();
		if (lawCtx.lawRefinement() != null) {
			codeBlock = visit(lawCtx.lawRefinement());
		} else if (lawCtx.biRefLaw() != null) {
			codeBlock = visit(lawCtx.biRefLaw());
		} else {
			codeBlock = visit(lawCtx.lawReference());
		}

		return codeBlock;
	}

	@Override
	public CodeBlock visitBiRefLaw(BiRefLawContext ctx) {
		// Get the ZVar and associate it with the implementation var name for the abstraction phase.
		Optional<ZVar> var = zScope.getVar(ctx.ID().getText());
		if (var.isPresent())
			zVarToImpVarName.put(var.get(), ctx.refinement(0).lvalue().getText());
		else
			throw new RuntimeException(String.format("Unknown identifier %s.", ctx.ID().getText()));

		// Evaluate the Z expressions of the law
		ZExprEvaluator zExprEvaluator = new ZExprEvaluator(zScope, scopeNameToCurrentPositionMap, processedElements);
		ZExpr          zExpr          = var.get().getValue();

		// Evaluate the refinement using the list of Z expressions as contexts
		CodeBlock codeBlock = new CodeBlock();

		// Create a new zScope that includes the result of evaluating the Z expressions of the law
		ZExprSchema newScope = ZExprSchema.add(zScope, new ZVar(RefinementHelper.zScopeVarName, zExpr));

		// Recursively evaluate the refinements of the law with the new Z scope and the current APL scope
		// The evaluation of each refinement produces a block of intermediate code that is collected to produce the output.
		RefinementLawEvaluator lawEvaluator = new RefinementLawEvaluator(newScope, aplScope, types, lValueFactory,
		    zVarConstantMaps, zVarToImpVarName, elementsInfo, contractRefiningSetOrList, processedElements);
		lawEvaluator.setScopeNameToPositionMap(scopeNameToCurrentPositionMap);
		for (RefinementContext context : ctx.refinement())
			codeBlock.join(lawEvaluator.visit(context));

		return codeBlock;
	}

	@Override
	public CodeBlock visitLawRefinement(LawRefinementContext ctx) {
		CodeBlock codeBlock = new CodeBlock();
		// Evaluate the Z expressions of the law
		ZExprList      zExprList      = null;
		ZExprEvaluator zExprEvaluator = new ZExprEvaluator(zScope, scopeNameToCurrentPositionMap, processedElements);
		try {
			ZExpr zExpr = zExprEvaluator.visit(ctx.zExprs());
			// If the evaluation of the Z expression returns a single Z expression, package it in a single element list to
			// factorize the rest of the code.
			if (zExpr instanceof ZExprList) {
				zExprList = (ZExprList) zExpr;
			} else {
				zExprList = new ZExprList(ImmutableList.of(zExpr), ctx.zExprs().getText());
			}
		} catch (ZVariableNotFoundInAbstractTestCaseException e) {
			// If there is a Z variable not included in the ATC, then don't
			// process the current refinement rule and returns an empty code block
			return codeBlock;
		} catch (CheckedException e) {
			throw new RefinementException("Processing " + ctx.getText(), e);
		}

		// Evaluate the refinement using the list of Z expressions as contexts
		for (ZExpr expr : zExprList) {
			processedElements.put(zExprList.getContextName(), expr);
			// Create a new zScope that includes the result of evaluating the Z expressions of the law
			ZExprSchema newScope   = ZExprSchema.add(zScope, new ZVar(RefinementHelper.zScopeVarName, expr));
			ContextType zScopeType = RefinementHelper.zExprsToContextType(ctx.zExprs());
			// Recursively evaluate the refinements of the law with the new Z scope and the current APL scope
			// The evaluation of each refinement produces a block of intermediate code that is collected to produce the output.
			RefinementLawEvaluator lawEvaluator = new RefinementLawEvaluator(newScope, aplScope, types, lValueFactory,
			    zVarConstantMaps, zVarToImpVarName, elementsInfo, contractRefiningSetOrList, processedElements);
			lawEvaluator.setScopeNameToPositionMap(scopeNameToCurrentPositionMap);
			lawEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
			lawEvaluator.setZScopeType(zScopeType);
			if (RefinementHelper.isElemOperator(ctx.zExprs())) {
				scopeNameToCurrentPositionMap.put(zExprList.getContextName(), outerAPLScopeInfo.getCounter());
			}
			for (RefinementContext context : ctx.refinement()) {
				codeBlock.join(lawEvaluator.visit(context));
			}
			if (outerAPLScopeInfo != null && zScopeType.equals(ContextType.POWER_SET_ELEMENT)) {
				outerAPLScopeInfo.incrementCounter();
			}
			if (outerAPLScopeInfo != null && outerAPLScopeInfo.getAplScope().getType() instanceof ArrayType
			    && outerAPLScopeInfo.getCounter().equals(outerAPLScopeInfo.getArraySize())) {
				break;
			}
		}
		return codeBlock;
	}

	@Override
	public CodeBlock visitLawReference(LawReferenceContext ctx) {
		String         ruleName = ctx.ID(0).getText();
		RefinementRule rule     = RuleManager.getInstance().getRule(ruleName);
		if (rule == null) {
			throw new InexistingRefinemetRuleException(ruleName);
		}
		boolean                 includeOtherRuleLaws = ctx.LAWS() != null;
		Map<String, LawContext> lawsById             = rule.getLawsById();
		CodeBlock               codeBlock            = new CodeBlock();
		if (includeOtherRuleLaws) {
			List<LawContext> lawsWithoutId = rule.getLawsWithoutId();
			List<LawContext> allLaws       = new ArrayList<>();
			allLaws.addAll(lawsById.values());
			allLaws.addAll(lawsWithoutId);
			for (LawContext lawCtx : allLaws) {
				codeBlock.join(processLawContext(lawCtx));
			}
		} else {
			String     lawName      = ctx.ID(1).getText();
			LawContext lawToProcess = lawsById.get(lawName);
			if(lawToProcess == null) {
				throw new InexistingLawException(ruleName, lawName);
			}
			codeBlock = processLawContext(lawToProcess);
		}
		return codeBlock;
	}
	
	@Override
	public CodeBlock visitRefinement(@NotNull RefinementContext ctx) {
		CodeBlock codeBlock = new CodeBlock();
		// Get the ATCAL lvalue of the refinement and use it as the new APL scope.
		LValueEvaluator lValueEval      = new LValueEvaluator(ctx.type());
		APLLValue       newAPLScope     = lValueEval.visit(ctx.lvalue());
		ATCALType       newAPLScopeType = newAPLScope.getType();
		boolean         insideAPLScope  = aplScope != null ? true : false;

		if (!newAPLScopeType.equals(lValueEval.type)) {
			throw new LValueTypeDoesntMatchWithGivenTypeException(newAPLScope.getName(), lValueEval.type.toString(),
			    newAPLScopeType.toString());
		}

		if (newAPLScopeType instanceof StringType) {
			codeBlock = RefinementHelper.refineAsString(ctx, this, newAPLScope);
		} else if (newAPLScopeType instanceof ContractType) {
			codeBlock = RefinementHelper.refineAsContract(ctx, this, newAPLScope);
		} else if (newAPLScopeType instanceof ArrayType) {
			if (insideAPLScope && aplScope.getType() instanceof ArrayType) {
				this.setRefiningArrayInsideArray(true);
			}
			codeBlock = RefinementHelper.refineAsArray(ctx, this, newAPLScope, insideAPLScope);
		} else if (newAPLScopeType instanceof RecordType) {
			codeBlock = RefinementHelper.refineAsRecord(ctx, this, newAPLScope);
		} else if (newAPLScopeType instanceof IntType || newAPLScopeType instanceof FloatType) {
			codeBlock = RefinementHelper.refineAsIntOrFloat(ctx, this, newAPLScope);
		} else if (newAPLScopeType instanceof EnumType) {
			codeBlock = RefinementHelper.refineAsEnum(ctx, this, newAPLScope);
		} else if (newAPLScopeType instanceof ReferenceType) {
			codeBlock = RefinementHelper.refineAsReference(ctx, this, newAPLScope);
		} else {
			throw new RefinementToNotImplementedTypeException(newAPLScopeType.toString());
		}

		return codeBlock;
	}

	@Override
	public CodeBlock visitWithRef(@NotNull WithRefContext ctx) {
		RefinementHelper.checkElemOperatorAppearsOnlyOnce(ctx);
		CodeBlock         codeBlock            = new CodeBlock();
		APLLValue         modifiedAPLScope     = RefinementHelper.generateNewAPLScopeIfNeeded(aplScope);
		OuterAPLScopeInfo newOuterAPLScopeInfo = new OuterAPLScopeInfo(modifiedAPLScope, contractRefiningSetOrList,
		    outerAPLScopeInfo);
		boolean           existOuterAPLScope   = outerAPLScopeInfo != null ? true : false;
		this.setOuterAPLScopeInfo(newOuterAPLScopeInfo);
		ATCALType aplScopeType = aplScope.getType();
		// Generate code according to the type of the refinement variable.
		if (aplScopeType instanceof ContractType) {
			// Contract types are used to create complex data structures that have a contract (an interface).
			// They have a constructor, a getter and a setter function that instantiate, extract or insert values.
			ContractType contractType = (ContractType) aplScopeType;

			// Create a new temporal variable to hold the data structure under construction.
			String contractVarName;
			String aplVarName;
			if (existOuterAPLScope) {
				contractVarName = VariableNamingHelper
				    .buildTemporalContractConstructorVariableName(outerAPLScopeInfo.getUpperAPLScope().getName(),
				                                                  outerAPLScopeInfo.getName(), outerAPLScopeInfo);
				aplVarName      = VariableNamingHelper
				    .buildDefinitiveContractMemberVariableNameWithUpperScope(modifiedAPLScope.getName(), outerAPLScopeInfo);
			} else {
				contractVarName = VariableNamingHelper.buildTemporalContractVariableName(outerAPLScopeInfo.getName(),
				                                                                         outerAPLScopeInfo);
				aplVarName      = VariableNamingHelper.buildDefinitiveContractMemberVariableName(aplScope.getName(),
				                                                                                 outerAPLScopeInfo);
			}
			APLVar       contractVar          = new APLVar(contractVarName, aplScopeType);
			List<String> constructorArguments = RefinementHelper.setToList(contractType.getConstArgs().keySet());
			codeBlock.addStmt(new ConstructorCallStmt(contractVar, constructorArguments));
			// Assign the temporal variable holding the data structure to the real refinement variable.
			if (!this.isContractRefiningSetOrList()) {
				/**
				 * The evaluation of the WITH clause for a contract type requires evaluating all the Z expressions of the
				 * refinements in the clause beforehand. The result of this evaluation is a list of iterable Z expressions.
				 * The number of elements in each iterable Z expressions must be the same, and the number of these iterables
				 * must be equal to the arity of the contract type's setter function.
				 * Consider a contract type with a setter of arity equal to 3 and a WITH clause such as:
				 * [ {a,b} ==> fst, {c,d} ==> snd, {e,f} ==> trd ]
				 * to refine this, first evaluate the Z expressions into a list of iterables L = [ [a,b], [c,d], [e,f] ].
				 * Then, refine the setter arguments fst, snd, and trd over the iterables:
				 * a ==> fst, c ==> snd, e ==> trd (call setter) b ==> fst, d ==> snd, f ==> trd (call setter).
				 */

				Map<String, ATCALType> setterArgs = contractType.getSetterArgs();
				// Check if setter arity matches
				if (ctx.lawRefinement().size() != (setterArgs.size()))
					throw new WrongNumberOfRefinementsInWithClauseException(modifiedAPLScope.getName());

				// Evaluate Z expressions of refinements into a list of ZExpr iterators (one for each argument of the setter).
				List<Iterator<ZExpr>> iteratorList   = Lists.newArrayList();
				ZExprEvaluator        zExprEvaluator = new ZExprEvaluator(zScope, scopeNameToCurrentPositionMap,
				    processedElements);
				for (LawRefinementContext lawRefinementContext : ctx.lawRefinement()) {
					ZExpr zExpr = zExprEvaluator.visit(lawRefinementContext.zExprs());
					if (zExpr instanceof Iterable)
						iteratorList.add(((Iterable<ZExpr>) zExpr).iterator());
					else
						// pack non-iterable Z expressions into an iterable collection to simplify the rest of the code.
						iteratorList.add(ImmutableList.of(zExpr).iterator());
				}

				List<String> setterArguments        = RefinementHelper.setToList(setterArgs.keySet());
				List<String> indexedSetterArguments = RefinementHelper.generateIndexedMemberNames(setterArguments,
				                                                                                  outerAPLScopeInfo);

				// Evaluate the refinement clauses. The evaluation must produce a block of code that defines one variable
				// for each argument of the setter function in the contract type.
				while (iteratorList.get(0).hasNext()) {
					for (Iterator<ZExpr> it : iteratorList) {
						// Create a new Z scope corresponding to the setters argument.
						ZExprSchema newScope = ZExprSchema.add(zScope, new ZVar(RefinementHelper.zScopeVarName, it.next()));

						// Recursively evaluate the refinements of the law with the new Z scope and the current APL scope
						// The evaluation of each refinement produces a block of intermediate code that is collected to produce
						// the output.
						RefinementLawEvaluator lawEvaluator = new RefinementLawEvaluator(newScope, contractVar, types,
						    new LValueFactory(), zVarConstantMaps, zVarToImpVarName, elementsInfo, contractRefiningSetOrList,
						    processedElements);
						lawEvaluator.setScopeNameToPositionMap(scopeNameToCurrentPositionMap);
						lawEvaluator.setOuterAPLScopeInfo(outerAPLScopeInfo);
						codeBlock.join(lawEvaluator.visit(ctx.lawRefinement(iteratorList.indexOf(it)).refinement(0)));
					}

					// Insert the values of the refined clauses into the data structure using the provided setter function
					// TODO: check the type values with the types of the arguments.
					codeBlock.addStmt(new SetterCallStmt(contractVar, indexedSetterArguments));
				}
			} else {
				for (LawRefinementContext lawRefinementContext : ctx.lawRefinement()) {
					outerAPLScopeInfo.setCounter(0);
					codeBlock.join(visit(lawRefinementContext));
				}

				ATCALType uniqueMemberType = contractType.getUniqueMemberType();
				if (uniqueMemberType instanceof ContractType) {
					CodeBlock    contractStmts;
					ContractType innerContractType = (ContractType) uniqueMemberType;

					if (RefinementHelper.isSpecifyingElemOperator(ctx)) {
						String lvlueName = RefinementHelper.getLValueName(ctx);
						contractStmts = StatementsHelper.generateFinalContractAssignmentStatements(outerAPLScopeInfo, contractVar,
						                                                                           lvlueName);
					} else {
						MappingHelper.completeContractContractMemberInfoMap(outerAPLScopeInfo, elementsInfo, innerContractType);
						contractStmts = StatementsHelper.generateContractInnerContractStmts(outerAPLScopeInfo, elementsInfo,
						                                                                    contractType, innerContractType);
					}
					elementsInfo.reset();
					codeBlock.join(contractStmts);
				} else if (uniqueMemberType instanceof RecordType) {
					CodeBlock  recordStmts;
					RecordType recordType = (RecordType) uniqueMemberType;

					if (RefinementHelper.isSpecifyingElemOperator(ctx)) {
						String lvlueName = RefinementHelper.getLValueName(ctx);
						recordStmts = StatementsHelper.generateFinalContractAssignmentStatements(outerAPLScopeInfo, contractVar,
						                                                                         lvlueName);
					} else {
						MappingHelper.completeContractRecordMemberInfoMap(outerAPLScopeInfo, elementsInfo, recordType);
						recordStmts = StatementsHelper.generateContractInnerRecordStmts(outerAPLScopeInfo, elementsInfo,
						                                                                contractType, recordType);
					}
					elementsInfo.reset();
					codeBlock.join(recordStmts);
				} else {
					List<String> setterArguments = RefinementHelper.setToList(contractType.getSetterArgs().keySet());
					Integer      counter         = outerAPLScopeInfo.getCounter();
					List<String> indexedSetterArguments;
					outerAPLScopeInfo.setCounter(0);
					for (int i = 0; i < counter; i++) {
						indexedSetterArguments = RefinementHelper.generateIndexedMemberNames(setterArguments,
						                                                                     newOuterAPLScopeInfo);
						codeBlock.addStmt(new SetterCallStmt(contractVar, indexedSetterArguments));
						outerAPLScopeInfo.incrementCounter();
					}
				}

			}

			APLVar aplVar = new APLVar(aplVarName, aplScopeType);
			codeBlock.addStmt(new AssignStmt(aplVar, contractVar));

		} else if (aplScopeType instanceof ArrayType) {
			// Array types are handled as a special case because they often have native support in the target language.
			APLLValue upperAPLScope = newOuterAPLScopeInfo.getUpperAPLScope();
			if (upperAPLScope != null && upperAPLScope.getType() instanceof ArrayType) {
				setRefiningArrayInsideArray(true);
			}
			ArrayType type      = (ArrayType) aplScopeType;
			ATCALType innerType = type.getType();
			int       arraySize = type.getSize();
			outerAPLScopeInfo.setArraySize(arraySize);
			// Allocate a new array using the APL built-in function newArray
			// and assign it to the current APL variable in scope.
			codeBlock.addStmt(new ArrayDeclStmt(type, outerAPLScopeInfo.getName(), arraySize));
			CodeBlock arrayStmts = new CodeBlock();

			// Evaluate the WITH-clauses. The evaluation produces a block of code that assign values to many indices in
			// the array (or all).
			Integer counterBkp = 0;
			for (LawRefinementContext lawRefinementContext : ctx.lawRefinement()) {
				outerAPLScopeInfo.setCounter(0);
				arrayStmts.join(visit(lawRefinementContext));
				// Save counter in case of an array inner processing that doesn't iterate over
				// array elements (leading the counter to 0)
				if (outerAPLScopeInfo.getCounter() != 0) {
					counterBkp = outerAPLScopeInfo.getCounter();
				}
			}
			outerAPLScopeInfo.setCounter(counterBkp);

			// If array's inner type is a contract, a record or an array, then for each array's element:
			// - With contracts: generate constructor and setter call statements
			// - With records:
			// ---> Generate an assignment for each record member
			// ---> Generate an assignment between an array element and the record formed by the record members
			// - With arrays:
			APLArray aplArray = new APLArray(outerAPLScopeInfo.getName(), type);
			if (innerType instanceof ContractType) {
				CodeBlock    contractStmts;
				ContractType contractType = (ContractType) innerType;
				if (RefinementHelper.isSpecifyingElemOperator(ctx)) {
					String lvlueName = RefinementHelper.getLValueName(ctx);
					contractStmts = StatementsHelper.generateFinalArrayAssignmentStatements(outerAPLScopeInfo, aplArray,
					                                                                        lvlueName);
				} else {
					MappingHelper.completeContractInfoMap(outerAPLScopeInfo, elementsInfo, contractType);
					contractStmts = StatementsHelper.generateArrayInnerContractStmts(outerAPLScopeInfo, elementsInfo, aplArray,
					                                                                 contractType);
				}
				elementsInfo.reset();
				arrayStmts.join(contractStmts);
			} else if (innerType instanceof RecordType) {
				RecordType recordType = (RecordType) innerType;
				MappingHelper.completeRecordInfoMap(outerAPLScopeInfo, elementsInfo, recordType);
				CodeBlock recordStmts = StatementsHelper.generateArrayInnerRecordStmts(outerAPLScopeInfo, elementsInfo,
				                                                                       (RecordType) innerType, aplArray);
				elementsInfo.reset();
				arrayStmts.join(recordStmts);
			} else if (innerType instanceof ArrayType) {
				List<ArrayDeclStmt> arrayDeclaredStatements = StatementsHelper.getArrayDeclStmts(arrayStmts,
				                                                                                 outerAPLScopeInfo.getName());
				APLLValue           aplArrayIndex;
				APLVar              arrayVar;
				APLArray            innerArrayVar;
				for (ArrayDeclStmt arrayDeclStmt : arrayDeclaredStatements) {
					aplArrayIndex = aplArray.getNextIndex();
					arrayVar      = new APLVar(aplArrayIndex.getName(), innerType);
					innerArrayVar = new APLArray(arrayDeclStmt.getName(), (ArrayType) innerType, true);
					arrayStmts.addStmt(new AssignStmt(arrayVar, innerArrayVar));
				}
			}
			codeBlock.join(arrayStmts);
		} else if (aplScopeType instanceof RecordType) {
			// Record types are handled separately because they have native support in the target language.
			String     recordVarName;
			RecordType recordType = (RecordType) aplScopeType;

			if (existOuterAPLScope) {
				recordVarName = VariableNamingHelper
				    .buildDefinitiveRecordMemberVariableNameWithUpperScope(outerAPLScopeInfo.getName(), outerAPLScopeInfo);
			} else {
				recordVarName = VariableNamingHelper.buildDefinitiveRecordMemberVariableName(outerAPLScopeInfo.getName(),
				                                                                             outerAPLScopeInfo);
			}

			APLVar       modifiedRecordVar    = new APLVar(recordVarName, recordType);
			List<String> indexedRecordMembers = RefinementHelper.generateIndexedMemberNames(recordType.getMembers(),
			                                                                                outerAPLScopeInfo);
			// Evaluate the WITH-clauses. The evaluation must produce a block
			// of code that defines one variable for each field of the record type.
			for (LawRefinementContext lawRefinementContext : ctx.lawRefinement()) {
				codeBlock.join(visit(lawRefinementContext));
			}
			codeBlock.addStmt(new RecordStmt(modifiedRecordVar, indexedRecordMembers));
		}
		return codeBlock;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * When <strong>aplScope</strong> is array of I, I must be of enum type <br>
	 * <br>
	 * Cases:<br>
	 * <ul>
	 * <li>Constant to enum: Get the constant's corresponding enum</li>
	 * <li>Set or list to enum: For each set or list element get the constant's corresponding enum
	 * </li>
	 * <li>Set or list to array i: For each set or list element get the constant's corresponding enum y store it into an array
	 * index</li>
	 * <li>Product to array i: For each product's members get the constant's corresponding enum y store it into an array
	 * index</li>
	 * </ul>
	 */
	@Override
	public CodeBlock visitConstMapping(ConstMappingContext ctx) {
		CodeBlock codeBlock    = new CodeBlock();
		ZExpr     zExpr        = this.getZScopeVarible();
		APLLValue aplScope     = this.aplScope;
		ATCALType aplScopeType = aplScope.getType();

		if (zExpr instanceof ZExprConst) {
			ZExprConst zExprConst     = (ZExprConst) zExpr;
			EnumType   enumType       = (EnumType) aplScopeType;
			AssignStmt enumAssignment = StatementsHelper.generateEnumAssigment(this, aplScope, zExprConst, enumType,
			                                                                   ctx.constMap());
			codeBlock.addStmt(enumAssignment);
		} else if (zExpr instanceof ZExprSet || zExpr instanceof ZExprList) {
			if (aplScopeType instanceof EnumType) {
				ZExpr           element;
				Iterator<ZExpr> iterator = RefinementHelper.getIterator(zExpr, outerAPLScopeInfo.getArraySize());
				while (iterator.hasNext()) {
					element = iterator.next();
					if (element instanceof ZExprConst) {
						ZExprConst zConstMember   = (ZExprConst) element;
						EnumType   enumType       = (EnumType) aplScopeType;
						AssignStmt enumAssignment = StatementsHelper.generateEnumAssigment(this, aplScope, zConstMember, enumType,
						                                                                   ctx.constMap());
						codeBlock.addStmt(enumAssignment);
					} else {
						throw new ZExprMustBeConstantException(element.toString());
					}
				}
			} else if (aplScopeType instanceof ArrayType) {
				ArrayType arrayType      = (ArrayType) aplScopeType;
				ATCALType arrayInnerType = arrayType.getType();
				if (arrayInnerType instanceof EnumType) {
					ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(arrayType, outerAPLScopeInfo.getName(),
					    arrayType.getSize());
					CodeBlock     arrayIndexStmts = RefinementHelper
					    .generateEnumArrayIndexStmts(this, zExpr, aplScope, (EnumType) arrayInnerType, ctx.constMap());

					codeBlock.addStmt(arrayDeclStmt);
					codeBlock.join(arrayIndexStmts);
				} else {
					throw new InnerArrayTypeMustBeEnumException();
				}
			} else { // aplScopeType is ContractType
				ContractType contractType      = (ContractType) aplScopeType;
				ATCALType    contractInnerType = contractType.getUniqueMemberType();
				if (contractInnerType instanceof EnumType) {
					CodeBlock contractStmts = RefinementHelper
					    .generateEnumContractStmts(this, zExpr, aplScope, (EnumType) contractInnerType, ctx.constMap());
					codeBlock.join(contractStmts);
				} else {
					throw new InnerContractTypeMusBeEnumException();
				}
			}

		} else {
			// zExpr is a ZExprProd
			ArrayType arrayType      = (ArrayType) aplScopeType;
			ATCALType arrayInnerType = arrayType.getType();
			if (arrayInnerType instanceof EnumType) {
				ArrayDeclStmt arrayDeclStmt   = new ArrayDeclStmt(arrayType, outerAPLScopeInfo.getName(), arrayType.getSize());
				CodeBlock     arrayIndexStmts = RefinementHelper
				    .generateEnumArrayIndexStmts(this, zExpr, aplScope, (EnumType) arrayInnerType, ctx.constMap());

				codeBlock.addStmt(arrayDeclStmt);
				codeBlock.join(arrayIndexStmts);
			} else {
				throw new InnerArrayTypeMustBeEnumException();
			}
		}
		return codeBlock;
	}

	@Override
	public CodeBlock visitUUTNoRetVal(UUTNoRetValContext ctx) {
		CodeBlock          codeBlock   = new CodeBlock();
		List<APLLValue>    uutCallArgs = new ArrayList<>();
		List<TerminalNode> argumentIds = ctx.args().ID();
		for (TerminalNode argumentId : argumentIds) {
			String    argString = argumentId.getText();
			APLLValue aplLvalue = lValueFactory.getLValue(argString);
			if (aplLvalue == null) {
				throw new UUTArgumentNotRefinedPreviously(argString);
			}
			uutCallArgs.add(aplLvalue);
		}
		CallExpr uutCall = new CallExpr(ctx.ID().getText(), uutCallArgs);
		codeBlock.addStmt(uutCall);
		return codeBlock;
	}

	@Override
	public CodeBlock visitUUTRetVal(UUTRetValContext ctx) {
		CodeBlock       codeBlock      = new CodeBlock();
		List<APLLValue> uutCallArgs    = Lists.transform(ctx.args().ID(), tn -> lValueFactory.getLValue(tn.getText()));
		String          resultVariable = ctx.ID(0).getText();
		String          methodName     = ctx.ID(1).getText();
		CallExpr        uutCall        = new CallExpr(methodName, uutCallArgs);
		ATCALType       type           = this.types.get(ctx.type().getText());
		/**
		 * Call newArray or newVar without saving the returned var so it's
		 * created the corresponding variable in the LValueFactory
		 */
		if (type instanceof ArrayType) {
			this.lValueFactory.newArray(resultVariable, type, false);
		} else {
			this.lValueFactory.newVar(resultVariable, type, false, zScopeType);
		}
		// Create an APLVar to assign the result of the method execution
		APLLValue  retVal    = new APLVar(resultVariable, type);
		AssignStmt uutRetVal = new AssignStmt(retVal, uutCall);
		codeBlock.addStmt(uutRetVal);
		return codeBlock;
	}

	/**
	 * ###################################################################
	 * ########################### Getter methods ########################
	 * ###################################################################
	 */

	public ZExprSchema getzScope() {
		return zScope;
	}

	public APLLValue getAPLScope() {
		return aplScope;
	}

	public Map<String, ATCALType> getTypes() {
		return types;
	}

	public LValueFactory getlValueFactory() {
		return lValueFactory;
	}

	public ElementsInfo getElementsInfo() {
		return elementsInfo;
	}

	public RefinementHelper.ContextType getzScopeType() {
		return zScopeType;
	}

	public Map<String, ConstantMapper> getZVarConstantMaps() {
		return zVarConstantMaps;
	}

	public BiMap<ZVar, String> getzVarToImpVarName() {
		return zVarToImpVarName;
	}

	public Map<String, Integer> getScopeNameToPositionMap() {
		return scopeNameToCurrentPositionMap;
	}

	public OuterAPLScopeInfo getOuterAPLScopeInfo() {
		return outerAPLScopeInfo;
	}

	public boolean isContractRefiningSetOrList() {
		return contractRefiningSetOrList;
	}

	public boolean isRefiningArrayInsideArray() {
		return refiningArrayInsideArray;
	}

	public Map<String, ZExpr> getProcessedElements() {
		return processedElements;
	}

	/**
	 * ##################################################################
	 * ###################### Setter and add methods ####################
	 * ##################################################################
	 */

	public void setZScopeType(RefinementHelper.ContextType zScopeType) {
		this.zScopeType = zScopeType;
	}

	public void setContractRefiningSetOrList(boolean contractRefiningSetOrList) {
		this.contractRefiningSetOrList = contractRefiningSetOrList;
	}

	public void setRefiningArrayInsideArray(boolean refiningArrayInsideArray) {
		this.refiningArrayInsideArray = refiningArrayInsideArray;
	}

	public void setScopeNameToPositionMap(Map<String, Integer> scopeNameToCurrentPositionMap) {
		this.scopeNameToCurrentPositionMap = scopeNameToCurrentPositionMap;
	}

	public void setOuterAPLScopeInfo(OuterAPLScopeInfo outerAPLScopeInfo) {
		this.outerAPLScopeInfo = outerAPLScopeInfo;
	}

	public void addProcessedVariable(String variable) {
		elementsInfo.addProcessedVariable(variable);
	}

	/**
	 * ##############################################################################
	 */

	public ZExpr getZScopeVarible() {
		return this.zScope.getVar(RefinementHelper.zScopeVarName).get().getValue();
	}

	public void addScopeIndexToMap(String scopeName, int index) {
		this.scopeNameToCurrentPositionMap.put(scopeName, index);
	}

	private void resetState() {
		this.elementsInfo.reset();
		this.scopeNameToCurrentPositionMap = new HashMap<>();
		this.outerAPLScopeInfo             = null;
		this.contractRefiningSetOrList     = false;
		this.refiningArrayInsideArray      = false;
	}

	// Private nested class to evaluate lvalues and their APL types.
	public class LValueEvaluator extends AtcalBaseVisitor<APLLValue> {

		private final ATCALType type;

		public LValueEvaluator(TypeContext typeCtx) {
			// Parse and resolve the type of the lvalue
			this.type = resolveType(typeCtx);
		}

		private ATCALType resolveType(TypeContext typeCtx) {
			// Lookup the type id in the types table. If not found it may be an in-place declaration, thus parse it.
			ATCALType asType = types.get(typeCtx.getText());
			if (asType == null) {
				asType = new TypesEvaluator.TypeEvaluator(types).visit(typeCtx);
			}
			return asType;
		}

		@Override
		public APLLValue visitVarLValue(@NotNull VarLValueContext ctx) {
			boolean insideAnAPLScope = aplScope == null ? false : true;
			String  varIdentifier    = ctx.ID().getText();
			if (type instanceof ArrayType)
				return lValueFactory.newArray(varIdentifier, type, insideAnAPLScope);
			else
				return lValueFactory.newVar(varIdentifier, type, insideAnAPLScope, zScopeType);
		}

		@Override
		public APLLValue visitArrayLValue(@NotNull ArrayLValueContext ctx) {
			if (ctx.NUMBER() != null) {
				return ((APLArray) aplScope).getIndex(Integer.valueOf(ctx.NUMBER().getText()));
			} else {
				return ((APLArray) aplScope).getNextIndex();
			}
		}

		@Override
		public APLLValue visitFieldLValue(@NotNull FieldLValueContext ctx) {
			boolean insideAnAPLScope = aplScope == null ? false : true;
			String  varIdentifier    = ctx.ID().getText();
			if (type instanceof ArrayType)
				return lValueFactory.newArray(varIdentifier, type, insideAnAPLScope);
			else
				return lValueFactory.newVar(varIdentifier, type, insideAnAPLScope, zScopeType);
		}
	}
}