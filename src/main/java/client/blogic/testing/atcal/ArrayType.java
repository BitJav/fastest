package client.blogic.testing.atcal;

import java.util.Iterator;

import client.blogic.testing.atcal.apl.expressions.APLExpr;
import client.blogic.testing.atcal.apl.expressions.APLExprList;
import client.blogic.testing.atcal.helpers.RefinementHelper;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprList;
import client.blogic.testing.atcal.z.ast.ZExprProd;
import client.blogic.testing.atcal.z.ast.ZExprSet;

/**
 * Created by Cristian on 29/04/15.
 */
public class ArrayType extends ATCALType {

	private final ATCALType type;
	private final int       size;

	public ArrayType(ATCALType type, int size) {
		this.type = type;
		this.size = size;
	}

	public ATCALType getType() {
		return type;
	}

	public int getSize() {
		return size;
	}

	@Override
	public APLExpr fromZExpr(ZExpr expr) {
		if (expr instanceof ZExprSet) {
			return translateZExprs(((ZExprSet) expr).iterator());
		} else if (expr instanceof ZExprList) {
			return translateZExprs(((ZExprList) expr).iterator());
		} else {
			throw new RuntimeException("");
		}
	}

	private APLExprList translateZExprs(Iterator<ZExpr> iterator) {
		APLExprList aplExprList = new APLExprList(type);
		while (iterator.hasNext()) {
			ZExpr   element = iterator.next();
			APLExpr aplExpr = type.fromZExpr(element);
			aplExprList.add(aplExpr);
		}
		return aplExprList;
	}

	@Override
	public boolean canImplementDirectly(ZExpr expr) {
		boolean result = true;
		if (expr instanceof ZExprSet || expr instanceof ZExprList) {
			Iterator<ZExpr> iterator = RefinementHelper.getIterator(expr);
			// Note that when the expression doesn't have elements inside, by default, it's considered that
			// its elements can be implemented directly from type
			if (iterator.hasNext()) {
				ZExpr element = iterator.next();
				result = type.canImplementDirectly(element);
			}
		} // A product can be implemented as an array when product elements are all of the same type and the array's inner type
		  // can implement directly elements of that type. Anyway, this should be a very special case
		else if (expr instanceof ZExprProd) {
			ZExprProd       prod     = (ZExprProd) expr;
			Iterator<ZExpr> iterator = prod.getValues().iterator();
			while (iterator.hasNext()) {
				ZExpr element = iterator.next();
				result = result && type.canImplementDirectly(element);
			}
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public String typeCategory() {
		return "Array";
	}

	@Override
	public boolean contains(String member) {
		boolean result = false;
		if (type instanceof RecordType) {
			RecordType recordType = (RecordType) type;
			result = recordType.contains(member);
		} else if (type instanceof ContractType) {
			ContractType contractType = (ContractType) type;
			result = contractType.contains(member);
		}
		return result;
	}

	@Override
	public String toString() {
		return "Array{" + "type='" + type + '\'' + ", size=" + size + '}';
	}

	@Override
	public int hashCode() {
		final int prime  = 31;
		int       result = 1;
		result = prime * result + size;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArrayType other = (ArrayType) obj;
		if (size != other.size)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
