package client.blogic.testing.atcal.z.ast;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Cristian on 3/31/15.
 */
public class ZExprSet implements ZExprSetOrList {

	private final List<ZExpr> set;
	private String            contextName = "";

	public ZExprSet(List<ZExpr> set) {
		this.set = set;
	}

	public ZExprSet(List<ZExpr> set, String contextName) {
		this.set         = set;
		this.contextName = contextName;
	}

	public ZExprSet(Set<ZExpr> set) {
		this.set = List.copyOf(set);
	}

	public ZExprSet(Iterator<ZExpr> iterator) {
		Set<ZExpr> set = new HashSet<>();
		while (iterator.hasNext()) {
			set.add(iterator.next());
		}
		this.set = List.copyOf(set);
	}

	public static ZExprSet of(ZExpr e1) {
		return new ZExprSet(Arrays.asList(e1));
	}

	public static ZExprSet of(ZExpr e1, ZExpr e2) {
		return new ZExprSet(ImmutableSet.of(e1, e2).asList());
	}

	public static ZExprSet of(ZExpr e1, ZExpr e2, ZExpr e3) {
		return new ZExprSet(ImmutableSet.of(e1, e2, e3).asList());
	}

	public static ZExprSet of(ZExpr e1, ZExpr e2, ZExpr e3, ZExpr e4) {
		return new ZExprSet(ImmutableSet.of(e1, e2, e3, e4).asList());
	}

	public static ZExprSet of(ZExpr e1, ZExpr e2, ZExpr e3, ZExpr e4, ZExpr e5) {
		return new ZExprSet(ImmutableSet.of(e1, e2, e3, e4, e5).asList());
	}

	private static final class EmptySetHolder {
		static final ZExprSet emptySet = new ZExprSet(new ArrayList<>());
	}

	public static ZExprSet getEmptySet() {
		return EmptySetHolder.emptySet;
	}

	public boolean contains(ZExpr expr) {
		if (expr == null)
			return false;
		else
			return set.contains(expr);
	}

	public int getCard() {
		return set.size();
	}

	public ZExpr get(int n) {
		return set.get(n);
	}

	public ZExprSet intersection(ZExprSet s) {
		ImmutableSet<ZExpr> currentSet   = ImmutableSet.copyOf(this.set);
		ImmutableSet<ZExpr> argumentSet  = ImmutableSet.copyOf(s.set);
		Set<ZExpr>          intersection = Sets.intersection(currentSet, argumentSet);
		return new ZExprSet(intersection);
	}

	public ZExprSet difference(ZExprSet s) {
		ImmutableSet<ZExpr> currentSet  = ImmutableSet.copyOf(this.set);
		ImmutableSet<ZExpr> argumentSet = ImmutableSet.copyOf(s.set);
		Set<ZExpr>          difference  = Sets.difference(currentSet, argumentSet);
		return new ZExprSet(difference);
	}

	public ZExprSet union(ZExprSet s) {
		ImmutableSet<ZExpr> currentSet  = ImmutableSet.copyOf(this.set);
		ImmutableSet<ZExpr> argumentSet = ImmutableSet.copyOf(s.set);
		Set<ZExpr>          union       = Sets.union(currentSet, argumentSet);
		return new ZExprSet(union);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ZExprSet zExprSet = (ZExprSet) o;

		if (!set.equals(zExprSet.set))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return set.hashCode();
	}

	@Override
	public String toString() {
		return "{" + Joiner.on(",").join(set) + '}';
	}

	@Override
	public Iterator<ZExpr> iterator() {
		return set.iterator();
	}

	@Override
	public String getContextName() {
		return contextName;
	}
}
