package client.blogic.testing.atcal.z.ast;

/**
 * Just a logic interface to join the classes {@link ZExprSet} and {@link ZExprList}
 * 
 * @author javier
 *
 */
public interface ZExprSetOrList extends ZExpr, Iterable<ZExpr> {
}
