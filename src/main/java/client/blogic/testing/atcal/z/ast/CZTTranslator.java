package client.blogic.testing.atcal.z.ast;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import net.sourceforge.czt.z.ast.ApplExpr;
import net.sourceforge.czt.z.ast.Expr;
import net.sourceforge.czt.z.ast.NumExpr;
import net.sourceforge.czt.z.ast.RefExpr;
import net.sourceforge.czt.z.ast.SetExpr;
import net.sourceforge.czt.z.ast.TupleExpr;
import net.sourceforge.czt.z.ast.ZExprList;
import net.sourceforge.czt.z.ast.ZNumeral;
import net.sourceforge.czt.z.impl.ZExprListImpl;
import net.sourceforge.czt.z.visitor.ExprVisitor;

/**
 * Created by Cristian on 03/08/15.
 */

/**
 * This class implements a visitor that translates CZT expressions into ATCAL Z expressions.
 * ATCAL has its own AST to represent the abstract test cases that simplifies the refiner implementation.
 */
public final class CZTTranslator implements ExprVisitor<ZExpr> {

    private final String zVarName;

    public CZTTranslator(String zVarName) {
        this.zVarName = zVarName;
    }

    public String getzVarName() {
        return zVarName;
    }

    @Override
    public ZExpr visitExpr(Expr expr) {

        if (expr instanceof SetExpr) {
            ZExprListImpl zExprList = (ZExprListImpl) ((SetExpr) expr).getExprList();
            Set<ZExpr> zExprSet = zExprList.stream().map(new Function<Expr, ZExpr>() {
                @Override
                public ZExpr apply(Expr expr) {
                    return expr.accept(CZTTranslator.this);
                }
            }).collect(Collectors.toSet());
            return new ZExprSet(zExprSet);

        } else if (expr instanceof NumExpr) {
            //Todo: What if the number is bigger than a long?
            return new ZExprNum(((NumExpr) expr).getValue().longValue());

        } else if (expr instanceof RefExpr) {
            // The empty set expression has an instance of a different class than the rest of the sets.
            RefExpr refExpr = (RefExpr)expr;
            if (refExpr.getZName().getWord().equals("∅"))
                return ZExprSet.getEmptySet();
            else
                return new ZExprConst(refExpr.getZName().getWord(), this.getzVarName(), ZExprConst.ConstantType.BASIC);

        } else if (expr instanceof TupleExpr) {
            ZExpr[] array = {};
            TupleExpr tupleExpr = (TupleExpr)expr;
            List<ZExpr> exprList = tupleExpr.getZExprList().stream().map(zExpr -> zExpr.accept(this)).collect(Collectors.toList());
            return new ZExprProd(exprList.toArray(array));

        } else if (expr instanceof ApplExpr) {
            ApplExpr applExpr = (ApplExpr)expr;
            if(applExpr.getMixfix()) {
            	String refWord = ((RefExpr) applExpr.getLeftExpr()).getZName().getWord();
            	if (refWord.equals("⟨ ,, ⟩")) {
            		ZExprList zExprList = ((SetExpr)applExpr.getRightExpr()).getZExprList();
            		return new client.blogic.testing.atcal.z.ast.ZExprList((zExprList.stream().
            			map( t -> ((TupleExpr)t).getZExprList().get(1).accept(this)).collect(Collectors.toList())));
            	} else if (refWord.equals(" _ ↦ _ ")) {
					ZExprList tupleZExprList      = ((TupleExpr) applExpr.getRightExpr()).getZExprList();
					ZExpr     assignmentLeftExpr  = tupleZExprList.get(0).accept(this);
					ZExpr     assignmentRightExpr = tupleZExprList.get(1).accept(this);
					return ZExprProd.of(assignmentLeftExpr, assignmentRightExpr);
            	} else if (refWord.equals("- _ ")) {
            		ZNumeral numZExpr = ((NumExpr) applExpr.getRightExpr()).getZNumeral();
            		BigInteger value = numZExpr.getValue();
            		return new ZExprNum(-1 * value.longValue());
            	}
            }
        }
        throw new RuntimeException("Unsupported CZT expr translation.");
    }
}
