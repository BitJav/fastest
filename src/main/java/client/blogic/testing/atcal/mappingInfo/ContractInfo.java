package client.blogic.testing.atcal.mappingInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.apl.expressions.APLExpr;

/**
 * Represents the necessary information to generate the statements, to be included in the code block,
 * of constructors and setters call when refining a specification variable into an array of contracts
 * 
 * @author Javier Bonet on 21/10/2019
 */
public class ContractInfo implements ElementInfo {

	/* Used only when a contract type is inside an array.
	 * Holds the relation between the contract's members
	 * and the list of values assigned in the WITH clause */
	private Map<String, List<Tuple<String, APLExpr>>> contractMembersMap;

	/* Holds the mapping between contract's members and its types */
	private Map<String, ATCALType> contractMembersTypeMap;

	public ContractInfo(Map<String, List<Tuple<String, APLExpr>>> contractMembersMap,
	                    Map<String, ATCALType> contractMembersTypeMap) {
		this.contractMembersMap     = contractMembersMap;
		this.contractMembersTypeMap = contractMembersTypeMap;
	}

	@Override
	public Map<String, List<Tuple<String, APLExpr>>> getElementMembersMap() {
		return this.contractMembersMap;
	}

	@Override
	public Map<String, ATCALType> getElementMembersTypeMap() {
		return this.contractMembersTypeMap;
	}

	@Override
	public void addValueToElementMembersMap(String member, String modifiedMember, APLExpr value) {
		List<Tuple<String, APLExpr>> modifiedMembersList = contractMembersMap.get(member);
		if (modifiedMembersList == null) {
			modifiedMembersList = new ArrayList<>();
			contractMembersMap.put(member, modifiedMembersList);
		}
		modifiedMembersList.add(new Tuple<>(modifiedMember, value));
	}

	@Override
	public void addTypeToElementMembersTypeMap(String member, ATCALType type) {
		if (!contractMembersTypeMap.containsKey(member)) {
			contractMembersTypeMap.put(member, type);
		}
	}

	@Override
	public boolean areElementsInElementsMap(List<String> elements) {
		boolean elementsPresentInMap = true;
		for (String element : elements) {
			if (!contractMembersMap.containsKey(element)) {
				return false;
			}
		}
		return elementsPresentInMap;
	}

	@Override
	public String toString() {
		return contractMembersMap + " --- " + contractMembersTypeMap;
	}

}
