package client.blogic.testing.atcal.mappingInfo;

/**
 * Relates an element of type K with its corresponding value of type V
 * 
 * @author     javier
 * @param  <K> Keys
 * @param  <V> Values
 */
public class Tuple<K, V> {
	private K key;
	private V value;

	public Tuple(K key, V value) {
		super();
		this.key   = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "(" + key + ", " + value + ")";
	}
}
