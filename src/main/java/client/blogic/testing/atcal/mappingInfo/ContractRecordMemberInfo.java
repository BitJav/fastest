package client.blogic.testing.atcal.mappingInfo;

import java.util.List;
import java.util.Map;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.apl.expressions.APLExpr;

/**
 * Represents the information necessary to generate the statements, to be included in the code block,
 * of constructors and setters call when refining a specification variable into a contract of records
 * 
 * @author javier on 15/03/2020
 *
 */
public class ContractRecordMemberInfo extends RecordInfo {
	public ContractRecordMemberInfo(Map<String, List<Tuple<String, APLExpr>>> contractMembersMap,
	                                Map<String, ATCALType> contractMembersTypeMap) {
		super(contractMembersMap, contractMembersTypeMap);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
}
