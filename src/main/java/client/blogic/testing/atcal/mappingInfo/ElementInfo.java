package client.blogic.testing.atcal.mappingInfo;

import java.util.List;
import java.util.Map;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.apl.expressions.APLExpr;

public interface ElementInfo {

	/**
	 * Return the mapping between element members' and its values
	 * @return The mapping between element members' and its values
	 */
	Map<String, List<Tuple<String, APLExpr>>> getElementMembersMap();

	/** Returns the mapping between element's members and its types
	 * @return The mapping between element's members and its types
	 */
	Map<String, ATCALType> getElementMembersTypeMap();

	/**
	 * Adds {@code value} to the corresponding element's member {@code member} in element mapping
	 * 
	 * @param member
	 * @param value
	 */
	void addValueToElementMembersMap(String member, String modifiedMember, APLExpr value);

	/**
	 * Adds {@code type} as the {@code member}'s type if it hasn't been added before
	 * 
	 * @param member
	 * @param type
	 */
	void addTypeToElementMembersTypeMap(String member, ATCALType type);

	/**
	 * Checks if all the {@code elements} are included in the element members' map
	 * @param elements
	 * @return
	 */
	boolean areElementsInElementsMap(List<String> elements);
}
