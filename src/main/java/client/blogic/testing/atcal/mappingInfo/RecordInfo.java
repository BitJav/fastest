package client.blogic.testing.atcal.mappingInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.apl.expressions.APLExpr;

/**
 * Represents the necessary information to generate the statements, to be included in the code block,
 * for each record member and the a statement for the assignment of the record itself when refining
 * a specification variable into an array of records
 * 
 * @author Javier Bonet on 21/10/2019
 */
public class RecordInfo implements ElementInfo {

	/**
	 * Used only when a record type is inside an array.
	 * Holds the relation between the record's members
	 * and the list of values assigned in the WITH clause
	 */
	private Map<String, List<Tuple<String, APLExpr>>> recordMembersMap;

	/** Holds the mapping between record's members and its types */
	private Map<String, ATCALType> recordMembersTypeMap;

	public RecordInfo(Map<String, List<Tuple<String, APLExpr>>> contractMembersMap,
	                  Map<String, ATCALType> contractMembersTypeMap) {
		this.recordMembersMap     = contractMembersMap;
		this.recordMembersTypeMap = contractMembersTypeMap;
	}

	@Override
	public Map<String, List<Tuple<String, APLExpr>>> getElementMembersMap() {
		return this.recordMembersMap;
	}

	@Override
	public Map<String, ATCALType> getElementMembersTypeMap() {
		return this.recordMembersTypeMap;
	}

	@Override
	public void addValueToElementMembersMap(String member, String modifiedMember, APLExpr value) {
		List<Tuple<String, APLExpr>> modifiedMembersList = recordMembersMap.get(member);
		if (modifiedMembersList == null) {
			modifiedMembersList = new ArrayList<>();
			recordMembersMap.put(member, modifiedMembersList);
		}
		modifiedMembersList.add(new Tuple<>(modifiedMember, value));
	}

	@Override
	public void addTypeToElementMembersTypeMap(String member, ATCALType type) {
		if (!recordMembersTypeMap.containsKey(member)) {
			recordMembersTypeMap.put(member, type);
		}
	}

	@Override
	public boolean areElementsInElementsMap(List<String> elements) {
		boolean elementsPresentInMap = true;
		for (String element : elements) {
			if (!recordMembersMap.containsKey(element)) {
				return false;
			}
		}
		return elementsPresentInMap;
	}

	@Override
	public String toString() {
		return recordMembersMap + " --- " + recordMembersTypeMap;
	}

}
