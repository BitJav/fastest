package client.blogic.testing.atcal.mappingInfo;

import java.util.List;
import java.util.Map;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.apl.expressions.APLExpr;

/**
 * Represents the information necessary to generate the statements, to be included in the code block,
 * of constructors and setters call when refining a specification variable into a contract of contracts
 * 
 * @author javier on 11/03/2020
 *
 */
public class ContractContractMemberInfo extends ContractInfo {
	public ContractContractMemberInfo(Map<String, List<Tuple<String, APLExpr>>> contractMembersMap,
	                                  Map<String, ATCALType> contractMembersTypeMap) {
		super(contractMembersMap, contractMembersTypeMap);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
}
