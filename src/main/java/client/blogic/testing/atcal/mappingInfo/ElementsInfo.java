package client.blogic.testing.atcal.mappingInfo;

import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.Maps;

import client.blogic.testing.atcal.ATCALType;
import client.blogic.testing.atcal.apl.expressions.APLExpr;

public class ElementsInfo {
	/**
	 * Info needed to generate contract's statements when refining to an array of contracts
	 */
	private ElementInfo contractInfo;
	/**
	 * Info needed to generate record's statements when refining to an array of records
	 */
	private ElementInfo recordInfo;
	/**
	 * This variable should only be used when refining a set or list with a contract type.
	 * The contract's inner type is another contract.
	 * It holds info needed to generate a statement and setter method call for each element
	 * in the set/list being refined
	 */
	private ElementInfo contractContractMemberInfo;
	/**
	 * This variable should only be used when refining a set or list with a contract type.
	 * The contract's inner type is a record.
	 * It holds info needed to generate a statement and setter method call for each element
	 * in the set/list being refined
	 */
	private ElementInfo contractRecordMemberInfo;

	/**
	 * Used to store processed elements in a specific scope
	 */
	private Set<String> processedVariables;

	public ElementsInfo() {
		initializeElements();
	}

	public ElementInfo getContractInfo() {
		return contractInfo;
	}

	public ElementInfo getRecordInfo() {
		return recordInfo;
	}

	public ElementInfo getContractContractMemberInfo() {
		return contractContractMemberInfo;
	}

	public ElementInfo getContractRecordMemberInfo() {
		return contractRecordMemberInfo;
	}

	public Set<String> getProcessedvariables() {
		return processedVariables;
	}

	public void reset() {
		initializeElements();
	}

	private void initializeElements() {
		contractInfo               = new ContractInfo(Maps.newHashMap(), Maps.newHashMap());
		recordInfo                 = new RecordInfo(Maps.newHashMap(), Maps.newHashMap());
		contractContractMemberInfo = new ContractContractMemberInfo(Maps.newHashMap(), Maps.newHashMap());
		contractRecordMemberInfo   = new ContractRecordMemberInfo(Maps.newHashMap(), Maps.newHashMap());
		processedVariables         = new HashSet<>();
	}

	/**
	 * Adds the relation <strong>{@code contractMember}</strong> -> <strong>({@code modifiedContractMember},
	 * {@code aplExpr})</strong> to the {@code contractInfo}
	 * values's map. <br>
	 * Also adds the relation <strong>{@code contractMember}</strong> -> <strong>{@code type}</strong> to the {@code contractInfo}
	 * type's map.
	 * 
	 * @param contractMember
	 * @param aplExpr
	 * @param type
	 */
	public void addContractMemberInfoToMaps(String contractMember,
	                                        String modifiedContractMember,
	                                        APLExpr aplExpr,
	                                        ATCALType type) {
		contractInfo.addValueToElementMembersMap(contractMember, modifiedContractMember, aplExpr);
		contractInfo.addTypeToElementMembersTypeMap(contractMember, type);
	}

	/**
	 * Adds the relation <strong>{@code recordMember}</strong> -> <strong>({@code modifiedRecordMember}, {@code aplExpr})</strong>
	 * to the {@code recordInfo}
	 * values's map. <br>
	 * Also adds the relation <strong>{@code recordMember}</strong> -> <strong>{@code type}</strong> to the {@code recordInfo}
	 * type's map.
	 * 
	 * @param contractMember
	 * @param aplExpr
	 * @param type
	 */
	public void addRecordMemberInfoToMaps(String recordMember, String modifiedRecordMember, APLExpr aplExpr, ATCALType type) {
		recordInfo.addValueToElementMembersMap(recordMember, modifiedRecordMember, aplExpr);
		recordInfo.addTypeToElementMembersTypeMap(recordMember, type);
	}

	/**
	 * Adds the relation <strong>{@code contractMember}</strong> -> <strong>({@code modifiedContractMember},
	 * {@code aplExpr})</strong> to the {@code contractContractMemberInfo} values's map. <br>
	 * Also adds the relation <strong>{@code contractMember}</strong> -> <strong>{@code type}</strong> to the
	 * {@code contractContractMemberInfo} type's map.
	 * 
	 * @param contractMember
	 * @param aplExpr
	 * @param type
	 */
	public void addContractContractMemberInfoToMaps(String contractMember,
	                                                String modifiedContractMember,
	                                                APLExpr aplExpr,
	                                                ATCALType type) {
		contractContractMemberInfo.addValueToElementMembersMap(contractMember, modifiedContractMember, aplExpr);
		contractContractMemberInfo.addTypeToElementMembersTypeMap(contractMember, type);
	}

	/**
	 * Adds the relation <strong>{@code recordMember}</strong> -> <strong>({@code modifiedRecordMember}, {@code aplExpr})</strong>
	 * to the {@code contractRecordMemberInfo} values's map. <br>
	 * Also adds the relation <strong>{@code recordMember}</strong> -> <strong>{@code type}</strong> to the
	 * {@code contractRecordMemberInfo} type's map.
	 * 
	 * @param recordMember
	 * @param aplExpr
	 * @param type
	 */
	public void addContractRecordMemberInfoToMaps(String recordMember,
	                                              String modifiedRecordMember,
	                                              APLExpr aplExpr,
	                                              ATCALType type) {
		contractRecordMemberInfo.addValueToElementMembersMap(recordMember, modifiedRecordMember, aplExpr);
		contractRecordMemberInfo.addTypeToElementMembersTypeMap(recordMember, type);
	}

	/**
	 * Adds {@code processedElement} to the {@link #processedVariables}
	 * 
	 * @param processedVariable
	 */
	public void addProcessedVariable(String processedVariable) {
		processedVariables.add(processedVariable);
	}

	@Override
	public String toString() {
		return contractInfo.toString() + "\n" +
		       recordInfo.toString() +
		       "\n" +
		       contractContractMemberInfo.toString() +
		       "\n" +
		       contractRecordMemberInfo.toString() +
		       "\n" +
		       processedVariables.toString();
	}
}
