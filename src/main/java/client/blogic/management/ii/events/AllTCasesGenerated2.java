package client.blogic.management.ii.events;

import java.util.List;

public class AllTCasesGenerated2 extends Event_{

	public List<TCaseGenerated> gettCaseGeneratedList() {
		return tCasesGeneratedList;
	}

	public void settCaseGeneratedList(List<TCaseGenerated> tCaseGeneratedList) {
		this.tCasesGeneratedList = tCaseGeneratedList;
	}

	private List<TCaseGenerated> tCasesGeneratedList;

	public AllTCasesGenerated2(){
		super.setEventName("allTCasesGenerated2");
	}


}
 
