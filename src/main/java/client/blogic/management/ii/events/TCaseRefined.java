package client.blogic.management.ii.events;

import client.blogic.testing.atcal.ConcreteTCase;
import common.z.AbstractTCase;

/**
 * @author Hache
 * @author Cristian
 */

public class TCaseRefined extends Event_ {
    private final String opName;
    private final AbstractTCase absTCase;
    private final ConcreteTCase concreteTCase;
    private final String refinementErrors;

	public TCaseRefined(String opName, AbstractTCase absTCase, ConcreteTCase concreteTCase, String refinementErrors) {
        this.opName = opName;
        this.absTCase = absTCase;
        this.concreteTCase = concreteTCase;
        this.refinementErrors = refinementErrors;
        super.setEventName("tCaseRefined");
    }

    public String getOpName() {
        return opName;
    }

    public AbstractTCase getAbstractTCase() {
        return absTCase;
    }

    public ConcreteTCase getConcreteTCase() {
        return concreteTCase;
    }
    
    public String getRefinementErrors() {
		return refinementErrors;
	}
}
