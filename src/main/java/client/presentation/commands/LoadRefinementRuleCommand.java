package client.presentation.commands;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import client.blogic.testing.atcal.RefinementRule;
import client.blogic.testing.atcal.RuleManager;
import client.blogic.testing.atcal.parser.AtcalLexer;
import client.blogic.testing.atcal.parser.AtcalParser;
import client.blogic.testing.atcal.parser.AtcalParser.LawContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawRefinementContext;
import client.blogic.testing.atcal.parser.AtcalParser.LawsContext;
import client.presentation.ClientTextUI;

/**
 * An instance of this class represents a command to load a refinement rule written in ATCALv2.0
 */
public class LoadRefinementRuleCommand implements Command {

    /**
     * Runs this command.
     *
     * @param clientTextUI  a reference to the text user interface issuing the command
     * @param args
     */
    @Override
    public void run(ClientTextUI clientTextUI, String args) {

        PrintWriter output = clientTextUI.getOutput();

        if (args == null || args.isEmpty() || args.split("\\s+").length != 1) {
            output.println("Invalid parameters.  Try 'help'.");
            return;
        }
        try {
            // Tokenize ATCAL refinement rule.
            ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(args.trim()));
            AtcalLexer lexer = new AtcalLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            AtcalParser parser = new AtcalParser(tokens);
            AtcalParser.RefinementRuleContext r = parser.refinementRule();
            Map<String, LawContext> lawsById = getLawsById(r.laws());
            List<LawContext> lawsWithoutId = getLawsWithoutId(r.laws());
            RuleManager.getInstance().addRule(r.ID().getText(), new RefinementRule(r, lawsById, lawsWithoutId));
        } catch (IOException e) {
            output.println("File '" + args + "' not found.");
        }
    }

	/**
	 * Builds a mapping between laws with its associated identification.
	 * It only consider {@link LawRefinementContext} when filling this map
	 * 
	 * @param  laws
	 * @return      A mapping between laws with its associated identification.
	 */
	private Map<String, LawContext> getLawsById(LawsContext laws) {
		Map<String, LawContext> lawsById = new HashMap<>();
		for (LawContext law : laws.law()) {
			if (law.ID() != null) {
				lawsById.put(law.ID().getText(), law);
			}
		}

		return lawsById;
	}
	
	/**
	 * Builds a list with the laws that doesn't have an identification.
	 * 
	 * @param  laws
	 * @return      A list with the laws that doesn't have an identification.
	 */
	private List<LawContext> getLawsWithoutId(LawsContext laws) {
		ArrayList<LawContext> lawsWithoutId = new ArrayList<>();
		for (LawContext law : laws.law()) {
			if (law.ID() == null) {
				lawsWithoutId.add(law);
			}
		}
		return lawsWithoutId;
	}
}

