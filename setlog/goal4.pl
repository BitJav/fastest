consult('/home/jmesuro/fastest/setlog/setlog495-16b.pl').
set_prolog_flag(toplevel_print_options, [quoted(true), portray(true)]).
use_module(library(dialect/sicstus/timeout)).
set_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)]).
time_out(setlog( 
NAT = int(0, 2147483649) & 
NAT1 = int(1, 2147483647) & 
INT = int(-2147483648, 2147483647) & 
BIN = {cc750no,cc753yes} & 
CMODE = {cc760COF,cc751CON} & 
CTYPE = {cc761RM,cc755SC,cc754IDA,cc757SDA,cc756TD,cc758RC,cc759MD,cc762ML,cc752LP} & 
pfun(Memp) & 
dom(Memp,AUX368) & 
AUX368 = int(1,AUX369) & 
AUX369 in NAT & 
pfun(Memd) & 
dom(Memd,AUX370) & 
AUX370 = int(1,AUX371) & 
AUX371 in NAT & 
Mdp in NAT & 
Mep in NAT & 
Ped in NAT & 
Ctime in NAT & 
Acquiring in BIN & 
Waiting in BIN & 
Sending in BIN & 
Dumping in BIN & 
Waitsignal in BIN & 
Mode in CMODE & 
Ccmd in CTYPE & 
Waiting = cc750no & 
Ccmd = cc756TD & 
Sending = cc753yes & 
Dumping = cc750no & 
Mep < 43 & 
Ped = 43 & 
Memp neq {} & 
AUX372 in INT & 
AUX372 is Mdp + 1 & 
AUX373 in INT & 
AUX373 is Mdp + 43 & 
AUX374 = int(AUX372,AUX373) & 
AUX374 neq {} & 
ssubset(AUX374,AUX368)
,_CONSTR),1000,_RET).