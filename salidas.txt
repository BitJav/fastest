
--------------------------------
showsch AddBike_SP_2_TCASE -u 2
--------------------------------

\begin{schema}{AddBike\_ SP\_ 2\_ TCASE}\\
 AddBike\_ VIS 
\where
 bikeid? \notin \dom bicycles \\
 bicycles = \{ \} \\
 \{ bikeid? \mapsto b? \} \neq \{ \} \\
 bicycles =~\emptyset \\
 bikeid? = BikeIdBikeidInput \\
 b? = ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) )
\end{schema}


--------------------------------
showsch AddBike_SP_4_TCASE -u 2
--------------------------------

\begin{schema}{AddBike\_ SP\_ 4\_ TCASE}\\
 AddBike\_ VIS 
\where
 bikeid? \notin \dom bicycles \\
 bicycles \neq \{ \} \\
 \{ bikeid? \mapsto b? \} \neq \{ \} \\
 bicycles \cap \{ bikeid? \mapsto b? \} = \{ \} \\
 bicycles = \{ ( BikeId1980 \mapsto ( cc224s18 , cc223yellow , ( Name2064 , Phone2064 , Address2064 ) ) ) \} \\
 bikeid? = BikeIdBikeidInput \\
 b? = ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) )
\end{schema}

--------------------------------
showsch AddBike_SP_12_TCASE -u 2
--------------------------------

\begin{schema}{AddBike\_ SP\_ 12\_ TCASE}\\
 AddBike\_ VIS 
\where
 bikeid? \in \dom bicycles \\
 bicycles \neq \{ \} \\
 \{ bikeid? \mapsto b? \} \neq \{ \} \\
 bicycles \cap \{ bikeid? \mapsto b? \} = \{ \} \\
 bicycles = \{ ( BikeIdBikeidInput \mapsto ( cc152s18 , cc151yellow , ( Name1916 , Phone1916 , Address1916 ) ) ) \} \\
 bikeid? = BikeIdBikeidInput \\
 b? = ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) )
\end{schema}

--------------------------------
showsch AddBike_SP_14_TCASE -u 2
--------------------------------

\begin{schema}{AddBike\_ SP\_ 14\_ TCASE}\\
 AddBike\_ VIS 
\where
 bikeid? \in \dom bicycles \\
 bicycles \neq \{ \} \\
 \{ bikeid? \mapsto b? \} \neq \{ \} \\
 \{ bikeid? \mapsto b? \} \subset bicycles \\
 bicycles = \{ ( BikeIdBikeidInput \mapsto ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) ) ) , ( BikeId1856 \mapsto ( cc134s18 , cc133yellow , ( Name1862 , Phone1862 , Address1862 ) ) ) \} \\
 bikeid? = BikeIdBikeidInput \\
 b? = ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) )
\end{schema}

--------------------------------
showsch AddBike_SP_15_TCASE -u 2
--------------------------------

\begin{schema}{AddBike\_ SP\_ 15\_ TCASE}\\
 AddBike\_ VIS 
\where
 bikeid? \in \dom bicycles \\
 bicycles \neq \{ \} \\
 \{ bikeid? \mapsto b? \} \neq \{ \} \\
 \{ bikeid? \mapsto b? \} = bicycles \\
 bicycles = \{ ( BikeIdBikeidInput \mapsto ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) ) ) \} \\
 bikeid? = BikeIdBikeidInput \\
 b? = ( s18 , yellow , ( NameBInput_{3}_{1}~, PhoneBInput_{3}_{2}~, AddressBInput_{3}_{3} ) )
\end{schema}
