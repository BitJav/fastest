package Person;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $name, $age, $address) = @_;
	$self->{"name"} = $name;
	$self->{"age"} = $age;
	$self->{"address"} = $address;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package main;

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = (
	'userAlreadyRegistered' => 'La persona ya fue registrada',
	'nonRegisteredUser' => 'La persona aún no fue registrada'
);

@blog = ();

sub isRegistered {
	$nickname = $_[0];
	$blogSize = getBlogSize();
	for $i (0..$blogSize-1) {
		if($blog[$i]->{"nickname"} eq $nickname) {
			return 1;
		}
	}
	return 0;
}

sub getUserIndex {
	$nickname = $_[0];
	$blogSize = getBlogSize();
	for $i (0..$blogSize-1) {
		if($blog[$i]->{"nickname"} eq $nickname) {
			return $i;
		}
	}
	return -1;
}

sub getBlogSize {
	$size = @blog;
	return $size;
}

sub insert {
	my ($nickname, $person) = @_;
	if(isRegistered($nickname)) {
		say($mensajes{'userAlreadyRegistered'})
	} else {
		push(@blog, {"nickname" => $nickname, "person" => $person});
	}
}

sub update {
	my ($nickname, $person) = @_;
	if(isRegistered($nickname)) {
		$index = getUserIndex($nickname);
		$blog[$index]->{"person"} = $person;
	} else {
		say($mensajes{'nonRegisteredUser'})
	}
}

sub deleteUser {
	$nickname = $_[0];
	if(isRegistered($nickname)) {
		$index = getUserIndex($nickname);
		delete $blog[$index];
	} else {
		say($mensajes{'nonRegisteredUser'})
	}
}