# Defino el enumerado para rangos de edad
use constant {
    young	=> 'young',
    adult	=> 'adult',
    old	=> 'old'
};

package Pair;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $dni, $ageRange) = @_;
	$self->{"dni"} = $dni;
	$self->{"ageRange"} = $ageRange;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package Ranges;

sub new {
    my ($class,$args) = @_;
    @emptyArray = ();
    my $self = bless {"elem" => \@emptyArray}, $class;
}

sub push {
	my ($self, $pair) = @_;
	@elem = @{$self->{"elem"}};
	$elemArraySize = @elem;
	$self->{"elem"}[$elemArraySize] = $pair;
}

sub pop {
	my $self = shift;
	my $arg = shift;
	@elem = @{$self->{"elem"}};
	$elemArraySize = @elem;
	if($elemArraySize == 0) {
		die;
	}
	$index = $elemArraySize-1;
	$result = $self->{"elem"}[$index];
	delete $self->{"elem"}[$index];
	return $result;
}

package main;

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = (
	'personAlreadyRegistered' => 'La persona ya fue registrada',
	'nonRegisteredPerson' => 'La persona aún no fue registrada'
);

$ranges = Ranges->new();

sub isRegistered {
	$dni = $_[0];
	$rangesCount = getRangesSize();
	for $i (0..$rangesCount-1) {
		if($ranges->{"elem"}[$i]->{"dni"} eq $dni) {
			return 1;
		}
	}
	return 0;
}

sub getPersonIndex {
	$dni = $_[0];
	$rangesSize = getRangesSize();
	for $i (0..$rangesSize-1) {
		if($ranges->{"elem"}[$i]->{"dni"} eq $dni) {
			return $i;
		}
	}
	return -1;
}

sub getRangesSize {
	$size = @{$ranges->{"elem"}};
	return $size;
}

sub insert {
	$dni = $_[0];
	$range = $_[1];
	if(isRegistered($dni)) {
		say($mensajes{'personAlreadyRegistered'})
	} else {
		push(@{$ranges->{"elem"}}, {"dni" => $dni, "ageRange" => $range});
	}
}

sub update {
	$dni = $_[0];
	$range = $_[1];
	if(isRegistered($dni)) {
		$index = getPersonIndex($dni);
		$ranges->{"elem"}[$index]->{"ageRange"} = $range;
	} else {
		say($mensajes{'nonRegisteredPerson'})
	}
}

sub getPeopleInRange {
	$range = $_[0];
	$rangesSize = getRangesSize();
	@result = ();
	for $i (0..$rangesSize-1) {
		if($ranges->{"elem"}[$i]->{"ageRange"} eq $range) {
			push(@result, $ranges->{"elem"}[$i]->{"dni"});
		}
	}
	return $result;
}

sub deletePerson {
	$dni = $_[0];
	if(isRegistered($dni)) {
		$index = getPersonIndex($dni);
		delete $ranges->{"elem"}[$index];
	} else {
		say($mensajes{'nonRegisteredPerson'})
	}
}