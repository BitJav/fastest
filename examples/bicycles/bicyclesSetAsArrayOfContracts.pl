# Defino el enumerado para tamaños de bicicletas
use constant {
    s18I	=> 's18I',
    s20I	=> 's20I',
    s24I	=> 's24I',
    s26I	=> 's26I',
    s28I	=> 's28I',
    s29I	=> 's29I'
};

# Defino el enumerado para colores
use constant {
    yellowI	=> 'yellowI',
    redI	=> 'redI',
    blueI	=> 'blueI'
};

package Pair;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $fst, $snd) = @_;
	$self->{"fst"} = $fst;
	$self->{"snd"} = $snd;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package Bike;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $size, $color, $manufacturer) = @_;
	$self->{"size"} = $size;
	$self->{"color"} = $color;
	$self->{"manufacturer"} = $manufacturer;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package Manufacturer;

sub new {
    my ($class,$args) = @_;
    my $self = bless { }, $class;
}

sub set {
	my ($self, $name, $phone, $address) = @_;
	$self->{"name"} = $name;
	$self->{"phone"} = $phone;
	$self->{"address"} = $address;
}

sub get {
	my $self = shift;
	my $arg = shift;
	return $self->{$arg};
}

package main;

# Hash donde se almacenan los mensajes a mostrar como salida
%mensajes = (
	'bikeAlreadyAdded' => 'La bicicleta ya fue agregada previamente',
	'bikeNotFound' => 'La bicicleta no existe'
);

# Array de contratos donde se guardarán las bicicletas
@bikes = ();

# Entero utilizado para saber cual es el índice donde
# debe guardarse el siguiente elemento de bikes
$lastIndex = 0;

# Chequea si ya fue agregado un ID de bicilceta
# Argumento: ID de bicilceta
sub bikeExists {
	$bikesSize = @bikes;
	for $i (0..$bikesSize-1) {
		if($bikes[$i]->get("fst") eq $_[0]) {
			return 1;
		}
	}
	return 0;
}

# Retorna  el índice del array donde se encuentra guardado
# el hash que tiene el número de cuenta que se pasa como
# argumento como clave. Devuelve -1 si no existe
# Argumento: número de cuenta
sub getAccountArrayIndex {
	$bikesSize = @bikes;
	for $i (0..$bikesSize-1) {
		if($bikes[$i]->get("fst") == $_[0]) {
			return $i;
		}
	}
	return -1;
}

# Agrega una bicicleta
# Primer argumento: ID de bicicleta
# Segundo argumento: Objeto que contiene la información de la bicilceta
sub addBike {
	$bikeId = $_[0];
	$bike = $_[1];
	if(bikeExists($bikeId)) {
		say($mensajes{'bikeAlreadyAdded'})
	} else {
		$lastIndex = @bikes;

		$bikeAux = Bike->new();
		$size = $bike->get("size");
		$color = $bike->get("color");
		$manufacturerAux = $bike->get("manufacturer");
		$name = $manufacturerAux->get("name");
		$phone = $manufacturerAux->get("phone");
		$address = $manufacturerAux->get("address");
		$manufacturer = Manufacturer->new();
		$manufacturer->set($name,$phone,$address);
		$bikeAux->set($size,$color,$manufacturer);

		$bikes[$lastIndex] = Pair->new();
		$bikes[$lastIndex]->set($bikeId, $bikeAux);
		$lastIndex++;
	}
}

sub removeBike {
	$bikeId = $_[0];

	if(bikeExists($bikeId)) {
		$arrayIndex = getAccountArrayIndex($bikeId);
		delete $bikes[$arrayIndex];
	} else {
		say($mensajes{'bikeNotFound'})
	}
}

sub getBikeManufacturer {
	$bikeId = $_[0];

	if(bikeExists($bikeId)) {
		$arrayIndex = getAccountArrayIndex($bikeId);
		return $bikes[$arrayIndex]->get("manufacturer");
	} else {
		say($mensajes{'bikeNotFound'})
	}
}
