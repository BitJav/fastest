sub plus {
	$in1 = $_[0];
	$in2 = $_[1];
	
	return $in1 + $in2;
}

sub minus {
	$in1 = $_[0];
	$in2 = $_[1];
	
	return $in1 - $in2;
}

sub multiplication {
	$in1 = $_[0];
	$in2 = $_[1];
	
	return $in1 * $in2;
}

sub division {
	$in1 = $_[0];
	$in2 = $_[1];

	if ($in2 == 0) {
		say('Error de división por 0')
	} else {
		return $in1 / $in2;
	}
}